
create or ALTER    PROCEDURE [dbo].[USP_PAGO_I]
(@PI_IDCONSULTA  bigint,
 @PI_TXTRANSACCION  varchar(50),
 @PI_FETRANSACCION  varchar(30),
 @PI_DEPAGO  decimal,
 @PI_IDPARAMTIPOTARJETA  bigint,
 @PI_TXTARJETA  varchar(20),
 @PI_FGUSOCREDITO  char(1),
 @PI_DECREDITOINI  decimal,
 @PI_DECREDITOFIN  decimal,
 @PI_IDPAGOPADRE  bigint,
 @PI_IDPARAMTIPOTRANSACCION  bigint,
 @PI_IDUSUARIO BIGINT,
 @PO_RESULTADO BIGINT OUTPUT,
 @PO_RESULTADOMSG VARCHAR(500) OUTPUT
 )
AS  

BEGIN TRY   
	INSERT INTO TBL_PAGO (
				IDCONSULTA,
				TXTRANSACCION,
				FETRANSACCION,
				DEPAGO,
				IDPARAMTIPOTARJETA,
				TXTARJETA,
				FGUSOCREDITO,
				DECREDITOINI,
				DECREDITOFIN,
				IDPAGOPADRE,
				IDPARAMTIPOTRANSACCION,
				FGESTADO,
				IDUSUARIOREGISTRA,
				FEREGISTRO)
			VALUES (
				@PI_IDCONSULTA,
				@PI_TXTRANSACCION,
				CONVERT(DATETIME,@PI_FETRANSACCION,103),
				@PI_DEPAGO,
				@PI_IDPARAMTIPOTARJETA,
				@PI_TXTARJETA,
				@PI_FGUSOCREDITO,
				@PI_DECREDITOINI,
				@PI_DECREDITOFIN,
				@PI_IDPAGOPADRE,
				@PI_IDPARAMTIPOTRANSACCION,
				'1',
				@PI_IDUSUARIO,
				GETDATE()
				);
	UPDATE TBL_CONSULTA
		SET IDUSUARIOMODIFICA = @PI_IDUSUARIO,
			FEMODIFICA = GETDATE(),
			IDPARAMESTADOCONSULTA = 15--ESTADO PROGRAMADO
		WHERE IDCONSULTA = @PI_IDCONSULTA;
	IF(@PI_DECREDITOINI<>@PI_DECREDITOFIN)
		UPDATE TBL_PERSONA 
		SET DECREDITO = DECREDITO - (@PI_DECREDITOINI-@PI_DECREDITOFIN) 
		WHERE IDPERSONA = (SELECT IDPERSONA FROM TBL_USUARIO WHERE IDUSUARIO=@PI_IDUSUARIO);
	
	SET @PO_RESULTADO=1;
END TRY
BEGIN CATCH
	SELECT @PO_RESULTADO=ERROR_NUMBER();
	SELECT @PO_RESULTADOMSG=ERROR_MESSAGE()+' LINE :'+CONVERT(VARCHAR,ERROR_LINE());
END CATCH

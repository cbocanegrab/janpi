ALTER TABLE TBL_PERSONA_MEDIDA ALTER COLUMN DEVALOR VARCHAR(20);
EXEC sp_RENAME 'TBL_PERSONA_MEDIDA.DEVALOR' , 'TXVALOR', 'COLUMN';
GO
CREATE TABLE dbo.TBL_MEDICO_PERSONA(
	IDMEDICOPERSONA  bigint IDENTITY(1,1) NOT NULL ,
	IDMEDICO bigint  NOT NULL,
	IDPERSONA bigint NOT NULL,
	FGDESTACADO char(1) NULL,
	FGESTADO char(1) NOT NULL,
	IDUSUARIOREGISTRA bigint NOT NULL,
	FEREGISTRO datetime NOT NULL,
	IDUSUARIOMODIFICA bigint NULL,
	FEMODIFICA datetime NULL,
 CONSTRAINT PK_MEDICOPERSONA PRIMARY KEY (IDMEDICOPERSONA))
 go
CREATE OR ALTER     PROCEDURE [dbo].[USP_PACIENTE_BY_MEDICO]
@PI_IDPERSONA bigint
AS
BEGIN
SET NOCOUNT ON; 
SELECT P.IDPERSONA,P.TXNOMBRE,P.TXPRIMERAPELLIDO,P.TXSEGUNDOAPELLIDO,P.TXCELULARPRINCIPAL,P.TXCORREOPRINCIPAL,
	COUNT(1) NUCANTIDADCONSULTAS,MAX(C.FECONSULTA) FEULTIMACONSULTA,ISNULL(NUCANTIDADTRATAMIENTOS,0) NUCANTIDADTRATAMIENTOS,
	ISNULL(MP.FGDESTACADO,'0') FGDESTACADO
from TBL_CONSULTA c 
join TBL_PERSONA P ON C.IDPERSONA=P.IDPERSONA
JOIN TBL_MEDICO_ESPECIALIDAD ME ON C.IDMEDICOESPECIALIDAD=ME.IDMEDICOESPECIALIDAD
JOIN TBL_MEDICO M ON ME.IDMEDICO=M.IDMEDICO
LEFT JOIN (SELECT R.IDCONSULTA, COUNT(1) NUCANTIDADTRATAMIENTOS FROM  
			TBL_RECETA R 
			JOIN TBL_RECETA_MEDICAMENTO RM ON R.IDRECETA=RM.IDRECETA
			WHERE R.FGESTADO<>'0'
			AND RM.FGESTADO<>'0'
			AND convert(date,GETDATE(),103) between RM.FEINICIO AND DATEADD(DAY,RM.NUDIAS,RM.FEINICIO)
			GROUP BY R.IDCONSULTA) T ON C.IDCONSULTA=T.IDCONSULTA
LEFT JOIN TBL_MEDICO_PERSONA MP ON M.IDMEDICO=MP.IDMEDICO
WHERE 1=1
--AND M.IDPERSONA=@PI_IDPERSONA
GROUP BY P.IDPERSONA,P.TXNOMBRE,P.TXPRIMERAPELLIDO,P.TXSEGUNDOAPELLIDO,P.TXCELULARPRINCIPAL,P.TXCORREOPRINCIPAL,
NUCANTIDADTRATAMIENTOS,MP.FGDESTACADO
ORDER BY FEULTIMACONSULTA DESC;
END
GO
CREATE OR ALTER   PROCEDURE [dbo].[USP_MEDICO_PERSONA_G]
	@PI_IDPERSONAMEDICO  bigint,
	@PI_IDPERSONAPACIENTE  bigint,
	@PI_FGDESTACADO char(1),
	@PI_IDUSUARIO  bigint,
	@PO_RESULTADO BIGINT OUTPUT,
	@PO_RESULTADOMSG VARCHAR(500) OUTPUT
AS   
BEGIN
DECLARE @IDMEDICO bigint;
    SET NOCOUNT ON;  
	BEGIN TRY
	SET @PO_RESULTADO =1;
	SELECT @IDMEDICO=IDMEDICO  FROM TBL_MEDICO M WHERE M.IDPERSONA=@PI_IDPERSONAMEDICO;
	IF EXISTS (SELECT 1 FROM  TBL_MEDICO_PERSONA  WHERE IDMEDICO = @IDMEDICO)
		UPDATE TBL_MEDICO_PERSONA
			SET FGDESTACADO = @PI_FGDESTACADO,
				IDUSUARIOMODIFICA = @PI_IDUSUARIO,
				FEMODIFICA = GETDATE()
			WHERE IDMEDICO= @IDMEDICO
			AND IDPERSONA = @PI_IDPERSONAPACIENTE;
	ELSE
		BEGIN
			INSERT INTO TBL_MEDICO_PERSONA (IDMEDICO,IDPERSONA,FGDESTACADO,IDUSUARIOREGISTRA,FEREGISTRO)
		VALUES (@IDMEDICO,@PI_IDPERSONAPACIENTE,@PI_FGDESTACADO, @PI_IDUSUARIO,GETDATE());
		END
	END TRY
	BEGIN CATCH
		SELECT @PO_RESULTADO=ERROR_NUMBER();
		SELECT @PO_RESULTADOMSG=ERROR_MESSAGE()+' LINE :'+CONVERT(VARCHAR,ERROR_LINE());
	END CATCH	
END
GO


CREATE OR ALTER     PROCEDURE [dbo].[USP_MEDICO_BYUSUARIO_LIST]
(@PI_IDPERSONA BIGINT)
AS  
BEGIN
    SET NOCOUNT ON; 
	SELECT DISTINCT M.IDMEDICO,ME.IDMEDICOESPECIALIDAD ,P.TXNOMBRE, P.TXPRIMERAPELLIDO , P.TXSEGUNDOAPELLIDO,
		E.IDESPECIALIDAD,E.TXESPECIALIDAD
	FROM TBL_CONSULTA C JOIN TBL_MEDICO_ESPECIALIDAD ME ON C.IDMEDICOESPECIALIDAD=ME.IDMEDICOESPECIALIDAD 
		JOIN TBL_MEDICO M ON ME.IDMEDICO=M.IDMEDICO
		JOIN TBL_PERSONA P  ON M.IDPERSONA=P.IDPERSONA
		JOIN TRF_ESPECIALIDAD E ON ME.IDESPECIALIDAD=E.IDESPECIALIDAD
	WHERE C.FGESTADO<>'0'
		AND C.IDPERSONA = @PI_IDPERSONA;	
END

GO
CREATE OR ALTER   PROCEDURE [dbo].[USP_MEDICO_COMENTARIOS]
@PI_IDMEDICOESPECIALIDAD bigint
AS
BEGIN
SET NOCOUNT ON; 
	SELECT C.IDCONSULTA, C.TXCOMENTARIO, C.DEVALOR, P.TXNOMBRE,P.TXPRIMERAPELLIDO,P.TXSEGUNDOAPELLIDO,
	dbo.GET_DIAS(CONVERT(VARCHAR,C.FEVALORACION,103),CONVERT(VARCHAR,GETDATE(),103)) TXTIEMPOVALORACION
	FROM TBL_CONSULTA C 
	JOIN TBL_PERSONA P ON C.IDPERSONA=P.IDPERSONA
	WHERE C.FGESTADO<>'0'
	AND C.IDMEDICOESPECIALIDAD = @PI_IDMEDICOESPECIALIDAD
   AND C.TXCOMENTARIO IS NOT NULL;
END;
GO
CREATE OR ALTER     PROCEDURE [dbo].[USP_MEDICO_EXPERIENCIA_L]
@PI_IDMEDICO bigint,
@PI_IDMEDICOEXPERIENCIA bigint
AS
BEGIN
	SET NOCOUNT ON; 
	SET LANGUAGE 'SPANISH';

	SELECT IDMEDICOEXPERIENCIA, IDMEDICO, TXPUESTO, TXDESCRIPCION, TXINSTITUCIONEXPERIENCIA,
		   TXANIOINICIO, TXANIOFIN,
		   TXMESINICIO,UPPER(DATENAME(month, '01/' + TXMESINICIO+ '/' + TXANIOINICIO)) TXNOMBREMESINICIO,
			TXMESFIN,UPPER(DATENAME(month, '01/' + TXMESFIN+ '/' + TXANIOINICIO)) TXNOMBREMESFIN,
						CASE WHEN TXANIOFIN IS NOT NULL THEN 
			dbo.GET_DIAS(CONVERT(VARCHAR,CONVERT(DATE,'01/'+TXMESINICIO+'/'+TXANIOINICIO,103),103),CONVERT(VARCHAR,CONVERT(DATE,'01/'+TXMESFIN+'/'+TXANIOFIN,103),103))
			ELSE dbo.GET_DIAS(CONVERT(VARCHAR,CONVERT(DATE,'01/'+TXMESINICIO+'/'+TXANIOINICIO,103),103),CONVERT(VARCHAR,GETDATE(),103)) + 'ACTUALIDAD' END TXTIEMPOEXPERIENCIA
		   FROM dbo.TBL_MEDICO_EXPERIENCIA
	WHERE FGESTADO = '1' 
		AND IDMEDICO = @PI_IDMEDICO 
		AND (IDMEDICOEXPERIENCIA = @PI_IDMEDICOEXPERIENCIA OR @PI_IDMEDICOEXPERIENCIA = 0);
END;
GO
CREATE OR ALTER     PROCEDURE [dbo].[USP_PERSONA_MEDIDA_I](
@PI_IDPERSONA BIGINT,
@PI_IDPARAMTIPOMEDIDA BIGINT,
@PI_TXVALOR VARCHAR(20),
@PI_IDUSUARIO BIGINT
)
AS
INSERT INTO TBL_PERSONA_MEDIDA(IDPARAMTIPOMEDIDA,TXVALOR, IDPERSONA,IDUSUARIOREGISTRA,FGESTADO)
VALUES(@PI_IDPARAMTIPOMEDIDA,@PI_TXVALOR,@PI_IDPERSONA,@PI_IDUSUARIO,'1');

GO
ALTER   PROCEDURE [dbo].[USP_PERSONA_MEDIDA_L](
@PI_IDPERSONA BIGINT,
@PI_IDPARAMTIPOMEDIDA BIGINT
)
AS
SELECT TOP(10) IDPERSONAMEDIDA, TXVALOR, FEREGISTRO
FROM TBL_PERSONA_MEDIDA
WHERE IDPERSONA = @PI_IDPERSONA AND IDPARAMTIPOMEDIDA = @PI_IDPARAMTIPOMEDIDA
AND FGESTADO = '1' 
ORDER BY FEREGISTRO DESC
GO
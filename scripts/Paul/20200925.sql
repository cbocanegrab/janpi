ALTER     PROCEDURE [dbo].[USP_CONSULTA_CANCEL_CITA_P](
@PI_IDCONSULTA BIGINT,
@PI_IDPARAMMOTIVOCANCELACION BIGINT,
@PI_IDUSUARIO BIGINT,
@PO_RESULTADO BIGINT OUTPUT,
@PO_RESULTADOMSG VARCHAR(500) OUTPUT
)
AS
DECLARE @CREDITO DECIMAL
DECLARE @PAGO DECIMAL
BEGIN TRY
	SELECT @CREDITO = DECREDITO
	FROM TBL_PERSONA
	WHERE IDPERSONA = (SELECT IDPERSONA FROM TBL_CONSULTA WHERE IDCONSULTA =@PI_IDCONSULTA)
	SELECT @PAGO = DEPAGO
	FROM TBL_PAGO
	WHERE IDCONSULTA = @PI_IDCONSULTA
	AND FGESTADO<>'0'
	AND IDPARAMTIPOTRANSACCION = 55;
	--ACTUALIZA CONSULTA
	UPDATE TBL_CONSULTA 
	SET IDPARAMMOTIVOCANCELACION = @PI_IDPARAMMOTIVOCANCELACION,
	IDPARAMESTADOCONSULTA = 17,--CANCELADO
	IDUSUARIOMODIFICA = @PI_IDUSUARIO,
	FEMODIFICA = GETDATE()
	WHERE IDCONSULTA = @PI_IDCONSULTA;
	--ACTUALIZA CREDITOS
	UPDATE TBL_PERSONA 
	SET DECREDITO = DECREDITO + @PAGO 
	WHERE IDPERSONA = (SELECT IDPERSONA FROM TBL_CONSULTA WHERE IDCONSULTA =@PI_IDCONSULTA);
	--INSERTA MOVIMIENTO DE LA TRANSACCIÓN
	INSERT INTO TBL_PAGO (IDCONSULTA,TXTRANSACCION,FETRANSACCION,DEPAGO,IDPARAMTIPOTARJETA,TXTARJETA,FGUSOCREDITO,
	DECREDITOINI,DECREDITOFIN,IDPAGOPADRE,IDPARAMTIPOTRANSACCION,FGESTADO,IDUSUARIOREGISTRA,FEREGISTRO,IDUSUARIOMODIFICA,FEMODIFICA)
	SELECT IDCONSULTA,TXTRANSACCION,FETRANSACCION,DEPAGO,IDPARAMTIPOTARJETA,TXTARJETA,FGUSOCREDITO,@CREDITO,
		@CREDITO + DEPAGO,NULL,56,'1',@PI_IDUSUARIO,GETDATE(),NULL,NULL
	FROM TBL_PAGO P 
	WHERE IDCONSULTA=@PI_IDCONSULTA
		AND FGESTADO<>'0'
	AND IDPARAMTIPOTRANSACCION = 55;
	SET @PO_RESULTADO=1
END TRY
BEGIN CATCH
	SELECT @PO_RESULTADO=ERROR_NUMBER();
	SELECT @PO_RESULTADOMSG=ERROR_MESSAGE()+' LINE :'+CONVERT(VARCHAR,ERROR_LINE());
END CATCH

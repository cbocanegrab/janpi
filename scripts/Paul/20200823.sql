ALTER TABLE TBL_SEGUIMIENTO   DROP CONSTRAINT FK_SEGUIMIENTO_PARAMETRO; 
ALTER TABLE TBL_USUARIO_SOCIAL_LOGIN   DROP CONSTRAINT FK__TBL_USUAR__IDPAR__40058253; 
ALTER TABLE TBL_USUARIO   DROP CONSTRAINT FK_USUARIO_PERSONA; 
ALTER TABLE TBL_CONSULTA   DROP CONSTRAINT FK_CONSULTA_MEDICO; 
EXEC sp_RENAME 'TBL_CONSULTA.IDMEDICO' , 'IDMEDICOESPECIALIDAD', 'COLUMN'
EXEC sp_RENAME 'TBL_CONSULTA.IDPACIENTE' , 'IDPERSONA', 'COLUMN'

GO

go
drop table tbl_parametro;
create table TBL_PARAMETRO( IDPARAMETRO bigint NOT NULL,
 IDTIPOPARAMETRO bigint NOT NULL,
 TXDESCRIPCION varchar(100)  NOT NULL,
 FGESTADO char(1)  NOT NULL,
 IDUSUARIOREGISTRA bigint NOT NULL,
 FEREGISTRO datetime NOT NULL,
 IDUSUARIOMODIFICA bigint,
 FEMODIFICA datetime,
  CONSTRAINT PK_PARAMETRO PRIMARY KEY (IDPARAMETRO))
  GO

create table TRF_TIPO_EMERGENCIA
(IDTIPOEMERGENCIA bigint not null,
 TXTIPOEMERGENCIA varchar(100),
 FGESTADO char(1)  NOT NULL,
 IDUSUARIOREGISTRA bigint NOT NULL,
 FEREGISTRO datetime NOT NULL,
 IDUSUARIOMODIFICA bigint,
 FEMODIFICA datetime,
  CONSTRAINT PK_TIPOEMERGENCIA PRIMARY KEY (IDTIPOEMERGENCIA))
  GO

ALTER TABLE TBL_SEGUIMIENTO ADD CONSTRAINT FK_SEGUIMIENTO_PARAMETRO FOREIGN KEY (IDPARAMSEGUIMIENTO) REFERENCES TBL_PARAMETRO(IDPARAMETRO);
ALTER TABLE TBL_USUARIO_SOCIAL_LOGIN ADD CONSTRAINT FK_USUSOCIALLOGIN_PARAMETRO FOREIGN KEY (IDPARAMTIPORED) REFERENCES TBL_PARAMETRO(IDPARAMETRO);


INSERT INTO TBL_PARAMETRO VALUES (1,0,'TIPO DOCUMENTO','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (2,1,'DNI','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (3,1,'CE','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (4,0,'GRADO INSTRUCCION','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (5,4,'PRIMARIA','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (6,4,'SECUNDARIA','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (7,4,'SUPERIOR NO UNIVERSITARIA','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (8,4,'SUPERIOR UNIVERSITARIA','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (9,0,'TIPO CONSULTA','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (10,9,'PERSONAL','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (11,9,'CORPORATIVO','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (12,9,'ESSALUD','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (13,0,'ESTADO CONSULTA MEDICA','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);  
INSERT INTO TBL_PARAMETRO VALUES (14,13,'REGISTRADO','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (15,13,'PROGRAMADO','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (16,13,'REPROGRAMADO','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (17,13,'CANCELADO','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (18,13,'ATENDIDO','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (19,0,'TIPO MEDICO','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (20,19,'PRIVADO','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (21,19,'ESSALUD','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (22,19,'PRIVADO/ESSALUD','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (23,0,'TIPO CENTRO ESTUDIOS','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (24,23,'UNIVERSIDAD','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (25,23,'INSTITUTO','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (26,23,'OTROS','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (27,0,'TIPO ESTUDIO','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (28,27,'LICENCIATURA','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (29,27,'DIPLOMADO','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (30,27,'MAESTRIA','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (31,27,'DOCTORADO','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (32,0,'LOGIN EXTERNOS','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (33,32,'FACEBOOK','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (34,32,'GOOGLE','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES (35,32,'TWITTER','1',SUSER_ID(),SYSDATETIME(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES(36,0,'TIPOS DE NOTIFICACION','1',1,GETDATE(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES( 37,36,'GENERAL','1',1,GETDATE(),NULL,NULL);
INSERT INTO TBL_PARAMETRO VALUES( 38,36,'POR PERSONA','1',1,GETDATE(),NULL,NULL);
ALTER TABLE TBL_CONSULTA ADD FETRANSACCION DATETIME;

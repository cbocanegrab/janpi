﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Domain.Commons
{
    public class TokenResponse
    {
        // Response Model from Google Recaptcha V3 Verify API
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("score")]
        public decimal Score { get; set; }

        [JsonProperty("action")]
        public string Action { get; set; }

        [JsonProperty("error-codes")]
        public List<string> ErrorCodes { get; set; }
    }
}

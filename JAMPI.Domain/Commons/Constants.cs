﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace JAMPI.Domain.Commons
{
    public class Constants
    {
        #region "[Generico]"
        public static string _GENERAL_GET_OK = "SE RECUPERÓ LA INFORMACIÓN CORRECTAMENTE";
        public static string _GENERAL_GET_NULL = "NO SE ENCONTRÓ NINGÚN REGISTRO";
        public static string _GENERAL_LIST_OK = "SE LISTÓ LA INFORMACIÓN CORRECTAMENTE";
        public static string _GENERAL_INSERT_OK = "SE GUARDÓ EL REGISTRO CORRECTAMENTE";
        public static string _GENERAL_UPDATE_OK = "SE ACTUALIZÓ EL REGISTRO CORRECTAMENTE";
        public static string _GENERAL_DELETE_OK = "SE BORRÓ EL REGISTRO CORRECTAMENTE";
        public static string _GENERAL_GET_ERROR = "NO SE PUDO OBTENER LA INFORMACIÓN";
        public static string _GENERAL_LIST_ERROR = "NO SE PUDO LISTAR LA INFORMACIÓN";
        public static string _GENERAL_INSERT_ERROR = "NO SE PUDO PROCESAR EL PEDIDO";
        public static string _GENERAL_UPDATE_ERROR = "ERROR: {0}";
        public static string _GENERAL_DELETE_ERROR = "ERROR: {0}";
        public static string _GENERAL_ERROR = "NO SE PUDO PROCESAR LA SOLICITUD";
        public static string _GENERAL_ERROR_NULL = "NO SE OBTUVO DATOS DE LA CONSULTA";
        public static string _GENERAL_ERROR_DEV = "ERROR GENERAL: {0}";
        public static string _GENERAL_ERROR_DEV_NULL = "CONSULTA RETORNA NULL";
        public static string _GENERAL_ERROR_SQL = "ERROR SQL {0}: {1}";
        public static string _GENERAL_CORREO_SEND_OK = "SE ENVIO CORRECTAMENTE EL CORREO";
        public static string _GENERAL_CORREO_SEND_ERROR = "OCURRIO UN ERROR AL ENVIAR CORREO";
        #endregion

        #region "[Persona]"
        public static string _PERSONA_EMAILEXISTE = "YA EXISTE UNA CUENTA CON EL CORREO ELECTRÓNICO INGRESADO";
        public static string _PERSONA_DNIEXISTE = "YA EXISTE UNA CUENTA CON EL NÚMERO DE DOCUMENTO INGRESADO";
        #endregion

        #region "[CAPTCHA]"
        public static string _CAPTCHA_VALIDATION_ERROR = "NO SE PUDO VALIDAR EL CAPTCHA";
        public static string _CAPTCHA_VALIDATION_ERROR_DEV = "ERROR: {0}";
        #endregion

        public static class Jwt
        {
            public static string _NO_VALID_ALGORITHM = "EL TOKEN ENVIADO NO CUMPLE CON EL ALGORITMO DE ENCRIPTACIÓN";
            public static string _TOKEN_NOT_EXPIRED = "EL TOKEN NO HA EXPIRADO";
            public static string _TOKEN_REFRESH_NOT_EXISTS = "EL TOKEN DE REFRESCO NO ES VÁLIDO";
        }

        public static class Token
        {
            public static string _AUTENTICACION_ERROR_GENERA_TOKEN = "NO SE PUDO OBTENER SESIÓN DEL USUARIO";
        }


        #region "[Login]"
        public static class Login
        {
            public const string _NO_EXISTE = "EL USUARIO O LA CONTRASEÑA NO SON CORRECTAS.";
            public const string _NO_EXISTE_DEV = "RETORNO USUARIO NULL.";
            public static class External
            {
                public const long _FACEBOOK_ID = 32;
                public const long _GOOGLE_ID = 33;
                public static class Facebook
                {
                    public const string _NO_TOKEN_VALID = "EL USUARIO NO ESTA REGISTRADO EN EL SISTEMA.";
                    public const string _NO_USER_EXISTS = "EL USUARIO NO TIENE CUENTA REGISTRADA";
                }
            }

            public static class Recovery {
                public const string _SUCCESS_GENERATE_TOKEN = "SE GENERÓ EL TOKEN SATISFACTORIAMENTE";
                public const string _NOT_USER_EXISTS = "NO EXISTE EL USUARIO";
                public const string _TOKEN_VALID_SUCCESS = "EL TOKEN SE GENERÓ CORRECTAMENTE";
                public const string _TOKEN_NOT_EXISTS = "EL TOKEN INGRESADO NO EXISTE";
            }
        }

        public static class PersonaMedidas{
               public const string _LIST_SUCCESS = "SE LISTARON LAS MEDIDAS CORRECTAMENTE";
        }

        #endregion
        #region "Empresa"
        public static string _EMPRESA_RUCEXISTE = "YA EXISTE UNA EMPRESA CON EL RUC INGRESADO";
        #endregion

        #region "Archivos"
        public static string _FILE_ERROR_CREATE_FOLDER = "NO SE PUDO INICIAR LA CARGA DE ARCHIVOS";
        public static string _FILE_ERROR_ADD_TEMP_FILE = "NO SE PUDO CONTINUAR CON LA CARGA DEL ARCHIVO";
        public static string _FILE_ERROR_DIRECTORY_NOT_FOUND = "NO SE ENCONTRÓ LA CARPETA DEL TOKEN RECIBIDO";
        public static string _FILE_ERROR_FILE_NOT_BASE64 = "LA TRAMA NO LLEGÓ EN FORMATO BASE64";

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Domain.Commons
{
    public class Session
    {
        public string jwtId { get; set; }
        public long idUsuario { get; set; }
        public long idPersona { get; set; }
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        public string strSegundoApellido { get; set; }
        public string strNumeroDocumento { get; set; }
        public string strCorreoPrincipal { get; set; } 
        public decimal decCredito { get; set; }
        public long idPerfil { get; set; }
        public string strPerfil { get; set; }
        public long idMedico { get; set; }
        public string strRutaFoto { get; set; }
        public string strRutaFirma { get; set; }
    }
}

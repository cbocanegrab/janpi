﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Domain.Commons
{
    public class Response<T>
    {
        public string message { get; set; }
        public bool success { get; set; }
        public T data { get; set; }
        public string developer { get; set; }
        public int type { get; set; }

        public void ObtenerResponseSuccess(T data)
        {
            this.success = true;
            this.data = data;
        }
        public void ObtenerResponseCatchError(Exception ex)
        {
            this.success = false;
            this.developer = ex.Message;
            this.message = Constants._GENERAL_ERROR;
        }

        public void ObtenerResponseCatchError(string developer)
        {
            this.success = false;
            this.developer = developer;
            this.message = Constants._GENERAL_ERROR;
        }
        public void ObtenerResponseError(string message)
        {
            this.success = false;
            this.developer = message;
            this.message = message;
        }
        public void ObtenerResponseError(string message, string developer)
        {
            this.success = false;
            this.developer = developer;
            this.message = message;
        }
    }

    


}

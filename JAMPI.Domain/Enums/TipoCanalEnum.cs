﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Domain.Enums
{
    public enum TipoCanalEnum
    {
        Correo = 1,
        Telefono = 2
    }
}

﻿using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Domain.Entities
{
    public class EmpresaEmpleado : Auditoria
    {
        public long idEmpresaEmpleado { get; set; }
        public long idEmpresa { get; set; }
        public long idUsuario { get; set; }
        public string strFechaIngreso { get; set; }
        public string strFechaSalida { get; set; }
        public long idEmpleadoPadre { get; set; }
        public Persona oPersona { get; set; }
    }
}

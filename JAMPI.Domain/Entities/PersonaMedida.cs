﻿using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Domain.Entities
{
    public class PersonaMedida : Auditoria
    {
        public long idPersonaMedida { get; set; }
        public long idParamTipoMedida { get; set; }
        public long idPersona { get; set; }
        public string strValor { get; set; }
        public string strFechaRegistro { get; set; }
    }
}

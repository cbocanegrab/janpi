﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Domain.Entities
{
    public class MedicoExperiencia
    {
        public long idMedicoExperiencia { get; set; }
        public long idMedico { get; set; }
        public string strPuesto { get; set; }
        public string strDescripcion { get; set; }
        public string strInstitucionExperiencia { get; set; }
        public string strAnioInicio { get; set; }
        public string strAnioFin { get; set; }
        public string strMesInicio { get; set; }
        public string strMesFin { get; set; }
        public string strNombreMesInicio { get; set; }
        public string strNombreMesFin { get; set; }
        public string strTiempoExperiencia { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Domain.Entities
{
    public class Usuario : Persona
    {
        public long idUsuario { get; set; }
        public string strNombreUsuario { get; set; }
        public string strClave { get; set; }
        public string strFechaCaducidad { get; set; }
        public long idUsuarioPadre { get; set; }
        public string strToken { get; set; }
        public string strActivo { get; set; }
        public string strUserId { get; set; }
        public long idParamTipoRed { get; set; }
    }
}

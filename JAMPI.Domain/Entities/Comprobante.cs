﻿using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Domain.Entities
{
    public class Comprobante : Auditoria
    {
        public long idComprobante { get; set; }
        public long idPaciente { get;set; }
        public long idEmpresa { get; set; }
        public long strFechaComprobante { get; set; }
        public long deMonto { get; set; }
        public long strPagado { get; set; }
        public long strCodigoPago { get; set; }
        
    }
}

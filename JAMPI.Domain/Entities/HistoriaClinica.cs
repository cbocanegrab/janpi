﻿using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Domain.Entities
{
    public class HistoriaClinica : Auditoria
    {
        public long idHistoriaClinica { get; set; }
        public long idPersona { get; set; }
        public string strAntecedenteFamiliar { get; set; }
        public string strAntecedenteMedico { get; set; }
        public string strAntecedenteQuirurgico { get; set; }
        public string strAntecedenteTraumatico { get; set; }
        public string strAntecedenteAlergico { get; set; }
        public string strAntecedenteViciomania{ get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Domain.Entities
{
    public class ExamenConsulta
    {
        public long idConsultaExamen { get; set; }
        public long idConsulta { get; set; }
        public long idExamen { get; set; }
        public string strExamen { get; set; }
        public string strEstado { get; set; }
    }
}

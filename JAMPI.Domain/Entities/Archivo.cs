﻿using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Domain.Entities
{
    public class Archivo : Auditoria
    {
        public long idArchivo { get; set; }
        public string strToken { get; set; }
        public string strRuta { get; set; }
        public string strNombre { get; set; }
        public string strExtension { get; set; }
        public decimal decPeso { get; set; }
        public string strTipoMime { get; set; }
        public string strHash { get; set; }
        public bool blExito { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Domain.Entities
{
    public class MedicamentoHorario
    {
        public long idMedicamentoFrecuencia { get; set; }
        public string strHora { get; set; }
    }
}

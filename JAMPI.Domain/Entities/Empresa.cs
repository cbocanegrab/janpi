﻿using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Domain.Entities
{  
    public class Empresa: Auditoria
    {
        public long idEmpresa { get; set; }
        public string strRUC { get; set; }
        public string strRazonSocial { get; set; }
        public string strDireccionComercial { get; set; }
        public string strTelefono { get; set; }
        public string strUbigeo { get; set; }
    }
}

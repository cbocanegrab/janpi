﻿using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Domain.Entities
{
    public class EspecialidadTip:Auditoria
    {
        public long idEspecialidadTip { get; set; }
        public long idEspecialidad { get; set; }
        public string strTip { get; set; }
    }
}

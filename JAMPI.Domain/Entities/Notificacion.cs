﻿using JAMPI.Domain.Commons;
using System;

namespace JAMPI.Domain.Entities
{
    public class Notificacion : Auditoria
    {
        public long idNotificacion { get; set; }
        public string strNotificacion { get; set; }
	    public long idParamTipoNotificacion { get; set; }
		public long idPersona { get; set; }
		public string strFechaNotificacion { get; set; }
		public string strLeido { get; set; }
		public string strFechaLeido { get; set; }
	}
}

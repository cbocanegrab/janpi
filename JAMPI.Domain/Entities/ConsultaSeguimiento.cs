﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Domain.Entities
{
    public class ConsultaSeguimiento
    {
        public long idSeguimiento { get; set; }
        public long idParamTipoSeguimiento { get; set; }
        public string strTipoSeguimiento { get; set; }
        public string strDescripcion { get; set; }
        public int intValoracion { get; set; } 
        public string strFechaSeguimientoTexto { get; set; }
        public int intDia { get; set; }
    }
}

﻿using JAMPI.Domain.Commons;

namespace JAMPI.Domain.Entities
{
    public class MensajePersona : Auditoria
    {
        public long idMensajePersona { get; set; }
        public long idPersona { get; set; }
        public long idMedico { get; set; }
        public string strMedico { get; set; }
        public int intNuevos { get; set; }
        public string strFechaUltMensaje { get; set; }
        public string strPaciente { get; set; }
        public string strFechaUltimaCita { get; set; }
    }
}

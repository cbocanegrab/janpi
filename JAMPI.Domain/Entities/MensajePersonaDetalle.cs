﻿using JAMPI.Domain.Commons;

namespace JAMPI.Domain.Entities
{
    public class MensajePersonaDetalle : Auditoria
    {
        public long idMensajePersonaDetalle { get; set; }
        public long idMensajePersona { get; set; }
        public long idPersonaEnvia { get; set; }
        public string strRutaFoto { get; set; }
        public long idMedico { get; set; }
        public string strNombre { get; set; }
        public string strMensaje { get; set; }
        public string strFechaEnvia { get; set; }
        public string strFechaLee { get; set; }
        public string strLeido { get; set; }
    }
}

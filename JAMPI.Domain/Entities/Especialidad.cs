﻿using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Domain.Entities
{
    public class Especialidad :Auditoria
    {
        public long idEspecialidad { get; set; }
        public string strEspecialidad { get; set; }
    }
}

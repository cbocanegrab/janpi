﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Domain.Entities
{
    public class Parametro
    {
        public long idParametro { get; set; }
        public string strParametro { get; set; }
    }
}

﻿using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Domain.Entities
{
    public class ConfigMedida : Auditoria
    {
        public long idPersonaConfigMedida { get; set; }
        public long idPersona { get; set; }
        public long idParamTipoMedida { get; set; }
        public string strTipoMedida { get; set; }
    }
}

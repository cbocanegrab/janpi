﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Domain.Entities
{
    public class Ubigeo
    {
        public string strUbigeo { get; set; }
        public string strUbiDepartamento { get; set; }
        public string strUbiProvincia { get; set; }
        public string strUbiDistrito { get; set; }
        public string strDepartamento { get; set; }
        public string strProvincia { get; set; }
        public string strDistrito { get; set; }
    }
}

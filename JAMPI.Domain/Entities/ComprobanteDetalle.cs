﻿using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Domain.Entities
{
    public class ComprobanteDetalle : Auditoria
    {
        public long idComprobanteDetalle { get; set; }
        public long idComprobante { get; set; }
        public string strDescripcion { get; set; }
        public decimal deMonto { get; set; }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Domain.Entities
{
    public class HistoriaClinicaDetalle
    {
        public long idHistoriaClinicaDet { get; set; }
        public long idHistoriaClinica { get; set; }
        public long idConsulta { get; set; }
        public string strMotivoConsulta{ get; set; }
        public string strDetalleEnfermedad{ get; set; }
    }
}

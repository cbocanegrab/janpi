﻿using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Domain.Entities
{
    public class Menu : Auditoria
    {
        public long idMenu { get; set; }
        public long idModulo { get; set; }
        public string strModulo { get; set; }
        public string strMenu { get; set; }
        public string strDescripcion { get; set; }
        public string strURL { get; set; }
        public string strColorA { get; set; }
        public string strColorB { get; set; }
        public int intColumnas { get; set; }
        public int intFilas { get; set; }
    }
}

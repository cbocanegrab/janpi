﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Domain.Entities
{
    public class MedicoHorario
    {
        public long idMedicoHorario { get; set; }
        public long idMedico { get; set; }
        public long idEspecialidad { get; set; }
        public string strFechaInicio { get; set; }
        public string strFechaFin { get; set; }
        public string strHoraInicio { get; set; }
        public string strHoraFin { get; set; }
        public long idUsuario { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Domain.Entities
{
    public class Examen
    {
        public long idExamen { get; set; }
        public string strExamen { get; set; }
        public long idExamenPadre { get; set; }
    }
}

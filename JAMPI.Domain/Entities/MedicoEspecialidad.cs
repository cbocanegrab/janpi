﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Domain.Entities
{
    public class MedicoEspecialidad
    {
        public long idMedico { get; set; }
        public long idMedicoEspecialidad { get; set; }
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        public string strSegundoApellido { get; set; }
        public long idEspecialidad { get; set; }
        public string strEspecialidad { get; set; }
        public decimal decRating { get; set; }
        public int intCitasAtendidas { get; set; }
        public string strTiempoHAMPIK { get; set; }

    }
}

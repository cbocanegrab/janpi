﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Domain.Entities
{
    public class Paciente
    {
        public long idPersona { get; set; }
        public string strNumeroDocumento { get; set; }
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        public string strSegundoApellido { get; set; }
        public string strSexo { get; set; }
        public string strFechaNacimiento { get; set; }
        public string strUbigeoNacimiento { get; set; }
        public string strUbigeo { get; set; }
        public string strDireccion { get; set; }
        public string strCelularPrincipal { get; set; }
        public string strCorreoPrincipal { get; set; }
        public int intCantidadConsultas { get; set; }
        public string strFechaUltimaConsulta { get; set; }
        public int intCantidadTratamientos { get; set; }
        public string strDestacado { get; set; }

    }
}

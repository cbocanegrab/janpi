﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Domain.Entities
{
    public class TipoEmergencia
    {
        public long idTipoEmergencia { get; set; }
        public string strTipoEmergencia { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Domain.Entities
{
    public class MedicoEstudio
    {
        public long idMedicoEstudio { get; set; }
        public long idMedico { get; set; }
        public string strCentroEstudio { get; set; }
        public string strCarrera { get; set; }
    }
}

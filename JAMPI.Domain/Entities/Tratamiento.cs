﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Domain.Entities
{
    public class Tratamiento
    {
        public long idConsulta { get; set; }
        public string strFechaConsulta { get; set; }
        public string strFechaConsultaTexto { get; set; }
        public string strMotivo { get; set; }
        public string strEstadoConsulta { get; set; }
        public long idMedico { get; set; }
        public long idMedicoEspecialidad { get; set; }
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        public string strSegundoApellido { get; set; }
        public long idEspecialidad { get; set; }
        public string strEspecialidad { get; set; }
        public int intDiasTranscurridos { get; set; }
        public int intDiasTratamiento { get; set; }
        public string strFechaFinTratamientoTexto { get; set; }
        public string strFechaInicioTratamiento { get; set; }
    }
}

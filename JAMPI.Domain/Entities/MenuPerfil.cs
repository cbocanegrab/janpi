﻿using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Domain.Entities
{
    public class MenuPerfil : Auditoria
    {
        public long  idMenuPerfil { get; set; }
        public long  idMenu { get; set; }
        public long idPerfil { get; set; }
    }
}

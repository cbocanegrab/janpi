﻿using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Domain.Entities
{
    public class Persona : Auditoria
    {
        public long idPersona { get; set; }
        public long idParamTipoDocumento { get; set; }
        public string strNumeroDocumento { get; set; }
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        public string strSegundoApellido { get; set; }
        public string strSexo { get; set; }
        public string strFechaNacimiento { get; set; }
        //public string strUbigeoNacimiento { get; set; }
        //public string strUbigeo { get; set; }
        public string strDireccion { get; set; }
        public string strCelularPrincipal { get; set; }
        public string strCelularSecundario { get; set; }
        public string strCorreoPrincipal { get; set; }
        public string strCorreoSecundario { get; set; }
        public long idParamGradoInstr { get; set; }
        public string strOcupacion { get; set; }
        public decimal decCredito { get; set; }
        public long idParamEstadoCivil { get; set; }
        public Usuario usuario { get; set; }
        public long idPersonaSup { get; set; }
        public string strRutaFoto { get; set; }
        public string strRutaFirma { get; set; }
    }
}

﻿using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Domain.Entities
{
    public class Consulta : Auditoria
    {
        public long idConsulta { get; set; }
        public long idPaciente { get; set; }
        public long idMedico { get; set; }
        public string strNombreMedico { get; set; }
        public string strFechaConsulta { get; set; }
        public string strHora { get; set; }
        public long idParamTipoConsulta { get; set; }
        public decimal dePeso { get; set; }
        public decimal deTalla { get; set; }
        public decimal deTemperatura { get; set; }
        public decimal dePresion { get; set; }
        public decimal deOxigenacion { get; set; }
        public long idConsultaPadre { get; set; }
        public decimal deDescuento { get; set; }
        public string strAlta { get; set; }
        public long idEspecialidad { get; set; }
        public string strEspecialidad { get; set; }
        public string strURL { get; set; }
        public long idParamPlataforma { get; set; }
        public string strNombrePaciente { get; set; }
        public string strFechaConsultaTexto { get; set; }
        public string strMotivo { get; set; }
        public string strEstadoTratamiento { get; set; }

    }
}

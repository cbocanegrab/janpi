﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Domain.Entities
{
    public class Pago
    {
        public long idPago { get; set; }
        public long idConsulta { get; set; }
        public string strTransaccion { get; set; }
        public string strFechaTransaccion { get; set; }
        public decimal decPago { get; set; }
        public int idParamTipoTarjeta { get; set; }
        public string strTarjeta { get; set; }
        public string strUsoCredito { get; set; }
        public decimal decCreditoIni { get; set; }
        public decimal decCreditoFin { get; set; }
        public long idPagoPadre { get; set; }
        public long idParamTipoTransaccion { get; set; }


    }
}

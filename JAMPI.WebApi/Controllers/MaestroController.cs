﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JAMPI.WebApi.Controllers
{
    ///
    [Route("api/[controller]")]
    [ApiController]
    public class MaestroController : ControllerBase
    {
        private readonly IMaestroService _maestroService;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="maestroService"></param>
        public MaestroController(IMaestroService maestroService)
        {
            _maestroService = maestroService;
        }
        /// <summary>
        /// Lista los tipos de emergencias
        /// </summary>
        /// <returns></returns>
        [HttpPost("ListarTipoEmergencia/")]
        public async Task<IActionResult> ListarTipoEmergencia()
        {
            Response<List<TipoEmergenciaSelectorResponse>> response = new Response<List<TipoEmergenciaSelectorResponse>>();
            response = await _maestroService.ListarTipoEmergencia();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista los convenios con las clínicas
        /// </summary>
        /// <returns></returns>
        [HttpPost("ListarConvenio/")]
        public async Task<IActionResult> ListarConvenio()
        {
            Response<List<ConveniosResponse>> response = new Response<List<ConveniosResponse>>();
            response = await _maestroService.ListarConvenio();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista los convenios con las clínicas
        /// </summary>
        /// <returns></returns>
        [HttpPost("ListarMedicamento/")]
        public async Task<IActionResult> ListarMedicamento()
        {
            var response = await _maestroService.ListarMedicamento();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost("ListarExamen/")]
        public async Task<IActionResult> ListarExamen()
        {
            var response = await _maestroService.ListarExamen();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idTipoEmergencia"></param>
        /// <returns></returns>
        [HttpPost("TipoEmergenciaTipListar/{idTipoEmergencia}")]
        public async Task<IActionResult> TipoEmergenciaTipListar(long idTipoEmergencia)
        {
            var response = await _maestroService.TipoEmergenciaTipListar(idTipoEmergencia);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
    }
}

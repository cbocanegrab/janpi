using System.Collections.Generic;
using System.Threading.Tasks;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace JAMPI.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class PersonaController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IPersonaService _personaService;
        /// 
        public PersonaController(IConfiguration configuration, IPersonaService personaService)
        {
            this._configuration = configuration;
            this._personaService = personaService;
        }
        /// <summary>
        /// Registra los datos del paciente
        /// </summary>
        /// <param name="persona">Objeto PersonaIRequest</param>
        /// <returns>Response.success true en caso se grabe correctamente</returns>
        [AllowAnonymous]
        [HttpPost("Insertar/")]
        public async Task<IActionResult> Insertar(PersonaIRequest persona)
        {
            Response<long> response = new Response<long>();
            if (ModelState.IsValid)
            {
                response = await _personaService.InsertarAsync(persona);
                if (response.success) return Ok(response);
                else return BadRequest(response);
            }
            else
            {
                response.success = false;
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Actualiza los datos del paciente
        /// </summary>
        /// <param name="persona">Objeto PersonaURequest</param>
        /// <returns>Response.success true en caso se grabe correctamente</returns>
        [AllowAnonymous]
        [HttpPut("Actualizar/")]
        public async Task<IActionResult> Actualizar(PersonaURequest persona)
        {
            Response<bool> response = new Response<bool>();
            if (ModelState.IsValid)
            {
                response = await _personaService.ActualizarAsync(persona);
                if (response.success) return Ok(response);
                else return BadRequest(response);
            }
            else
            {
                response.success = false;
                return BadRequest(response);
            }
        }

        /// <summary>
        /// Obtener los datos del usuario
        /// </summary>
        /// <returns>Response.success true en caso se grabe correctamente</returns>
        [HttpPost]
        public async Task<IActionResult> GetInfo()
        {
            return Ok(await _personaService.GetInfoAsync());
        }
        /// <summary>
        /// Lista las personas dependientes del usuario
        /// </summary>
        /// <returns></returns>
        [HttpPost("PersonasDependientes/")]
        public async Task<IActionResult> PersonasDependientes()
        {
            Response<List<PersonaDependienteResponse>> response = new Response<List<PersonaDependienteResponse>>();
            response = await _personaService.ListarDependientes();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Obtiene el credito disponible del paciente
        /// </summary>
        /// <returns></returns>
        [HttpPost("Credito/")]
        public async Task<IActionResult> Credito()
        {
            var response = await _personaService.CreditoAsync();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="oContactoRequest"></param>
        /// <returns></returns>
        [HttpPost("InsertContacto/")]
        public async Task<IActionResult> InsertContacto(ContactoRequest oContactoRequest)
        {
            var response = await _personaService.InsertarContacto(oContactoRequest.strDescripcion);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Obtiene datos basicos de la persona
        /// </summary>
        /// <param name="idPersona"></param>
        /// <returns></returns>
        [HttpPost("PersonaMin/{idPersona}")]
        public async Task<IActionResult> PersonaMin(long idPersona)
        {
            var response = await _personaService.PersonaMin(idPersona);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Guarda la persona dependiente, enviar idPersona = 0, para insertar, otro valor para actualizar.
        /// </summary>
        /// <param name="persona"></param>
        /// <returns></returns>
        [HttpPost("DependienteGuardar/")]
        public async Task<IActionResult> PersonaDepInsertar(PersonaDepRequest persona)
        {
            var response = await _personaService.PersonaDepInsertar(persona);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Actualiza el estado del vinculo.
        /// </summary>
        /// <param name="idPersona"></param>
        /// <param name="strEstado"></param>
        /// <returns></returns>
        [HttpPut("VinculoActualizar/{idPersona}/{strEstado}")]
        public async Task<IActionResult> VinculoActualizar(long idPersona, string strEstado)
        {
            var response = await _personaService.VinculoActualizar(idPersona, strEstado);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idPersona"></param>
        /// <returns></returns>
        [HttpPost("PersonaDepObtener/{idPersona}")]
        public async Task<IActionResult> PersonaDepObtener(long idPersona)
        {
            var response = await _personaService.PersonaObtener(idPersona);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }

    }
}

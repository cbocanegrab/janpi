﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace JAMPI.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IUsuarioService usuarioService;
        ///
        public LoginController(IConfiguration configuration, IUsuarioService usuarioService)//, ISignInManager signInManager)
        {
            _configuration = configuration;
            this.usuarioService = usuarioService;
        }
        /// <summary>
        /// Valida las credenciales pars el Login
        /// </summary>
        /// <param name="oLogin">Objeto LoginRequest </param>
        /// <returns>response.success true si las credenciales son correctas</returns>
        [HttpPost("Validar/")]
        public async Task<IActionResult> Validar(LoginRequest oLogin)
        {
            return Ok(await usuarioService.AccederAsync(oLogin));
        }
        /// <summary>
        /// Cambiar clave
        /// </summary>
        /// <param name="passwordRequest">Objeto PasswordRequest</param>
        /// <returns>Response.success true si se guarda la nueva clave</returns>
        [HttpPost("CambiarClave/")]
        public async Task<IActionResult> CambiarClave(PasswordRequest passwordRequest)
        {
            Response<long> response = new Response<long>();
            if (ModelState.IsValid)
            {
                response = await usuarioService.CambiarClaveAsync(passwordRequest);
                if (response.success) return Ok(response);
                else return BadRequest(response);
            }
            else
            {
                response.success = false;
                return BadRequest(response);
            }
        }
        /// <summary>
        /// Recuperar contraseña, envía correo con la contraseña
        /// </summary>
        /// <param name="recuperarClave">Email del usuario</param>
        /// <returns></returns>
        [HttpPost("RecuperarClave/")]
        public async Task<IActionResult> RecuperarClave(RecuperarClaveRequest recuperarClave)
        {
            Response<int> response = new Response<int>();
            if (ModelState.IsValid)
            {
                response = await usuarioService.RecuperarClaveAsync(recuperarClave.strUsuario);
                if (response.success) return Ok(response);
                else return BadRequest(response);
            }
            else
            {
                response.success = false;
                return BadRequest(response);
            }
        }
        /// <summary>
        /// Recuperar contraseña, envía correo con la contraseña
        /// </summary>
        /// <param name="validaToken">token de verificacion</param>
        /// <returns></returns>
        [HttpPost("CheckToken/")]
        public async Task<IActionResult> VerificarToken(ValidaTokenRequest validaToken)
        {
            Response<int> response = new Response<int>();
            if (ModelState.IsValid)
            {
                response = await usuarioService.ValidarTokenAsync(validaToken.strUsuario, validaToken.strToken);
                if (response.success) return Ok(response);
                else return BadRequest(response);
            }
            else
            {
                response.success = false;
                return BadRequest(response);
            }
        }


        /// <summary>
        /// Acceder con red social FB
        /// </summary>
        /// <param name="externalLogin">Token de Acceso de Facebook</param>
        /// <returns></returns>
        [HttpPost("facebook-login/")]
        public async Task<IActionResult> FacebookLogin(ExternalLoginRequest externalLogin)
        {
            return Ok(await usuarioService.FacebookLoginAsync(externalLogin.strAccessToken));
        }
        /// <summary>
        /// Restablece clave
        /// </summary>
        /// <param name="oRestableceClave"></param>
        /// <returns></returns>
        [HttpPost("RestableceClave/")]
        public async Task<IActionResult> RestableceClave(RestableceClaveRequest oRestableceClave)
        {
            Response<bool> response = new Response<bool>();
            if (ModelState.IsValid)
            {
                response = await usuarioService.RestableceClaveAsync(oRestableceClave);
                if (response.success) return Ok(response);
                else return BadRequest(response);
            }
            else
            {
                response.success = false;
                return BadRequest(response);
            }
        }
    }
}

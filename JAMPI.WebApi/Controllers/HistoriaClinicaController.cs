﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JAMPI.WebApi.Controllers
{
    ///
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class HistoriaClinicaController : ControllerBase
    {
        private readonly IHistoriaClinicaService _historiaClinicaService;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="historiaClinicaService"></param>
        public HistoriaClinicaController(IHistoriaClinicaService historiaClinicaService)
        {
            _historiaClinicaService = historiaClinicaService;
        }

        /// <summary>
        /// Insertar una historia clinica
        /// </summary>
        /// <param name="oHistoriaClinicaIRequest"> objeto para inserción HistoriaClinicaIRequest</param>
        /// <returns>Response.success true en caso se grabe correctamente</returns>
        [HttpPost("HistoriaClinicaInsertar/")]
        public async Task<IActionResult> HistoriaClinicaInsertar(HistoriaClinicaIRequest oHistoriaClinicaIRequest)
        {
            Response<long> response = new Response<long>();
            response = await _historiaClinicaService.HistoriaClinicaInsertar(oHistoriaClinicaIRequest);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }

        /// <summary>
        /// Actualiza un registro de historia clinica
        /// </summary>
        /// <param name="oHistoriaClinicaURequest">objeto para la actualización de historia clinica</param>
        /// <returns></returns>
        [HttpPut("HistoriaClinicaActualizar/")]
        public async Task<IActionResult> HistoriaClinicaActualizar(HistoriaClinicaURequest oHistoriaClinicaURequest)
        {
            Response<bool> response = new Response<bool>();
            response = await _historiaClinicaService.HistoriaClinicaActualizar(oHistoriaClinicaURequest);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        
        /// <summary>
        /// Retorna las historias clinicas
        /// </summary>
        /// /// <param name="idPersona">Identificador de persona</param>
        /// <returns></returns>
        [HttpPost("HistoriaClinicaListar/{idPersona}")]
        public async Task<IActionResult> HistoriaClinicaListar(long idPersona)
        {
            var response = await _historiaClinicaService.HistoriaClinicaListar(idPersona);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }

        /// <summary>
        /// Insertar detalle de historia clinica
        /// </summary>
        /// <param name="oHistoriaClinicaDetalleIRequest"> objeto para inserción HistoriaClinicaDetalleIRequest</param>
        /// <returns>Response.success true en caso se grabe correctamente</returns>
        [HttpPost("HistoriaClinicaDetalleInsertar/")]
        public async Task<IActionResult> HistoriaClinicaDetalleInsertar(HistoriaClinicaDetalleIRequest oHistoriaClinicaDetalleIRequest)
        {
            Response<long> response = new Response<long>();
            response = await _historiaClinicaService.HistoriaClinicaDetalleInsertar(oHistoriaClinicaDetalleIRequest);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }

        /// <summary>
        /// Actualiza un registro de detalle de historia clinica
        /// </summary>
        /// <param name="oHistoriaClinicaDetalleURequest">objeto para la actualización de detalle de historia clinica</param>
        /// <returns></returns>
        [HttpPut("HistoriaClinicaDetalleActualizar/")]
        public async Task<IActionResult> HistoriaClinicaDetalleActualizar(HistoriaClinicaDetalleURequest oHistoriaClinicaDetalleURequest)
        {
            Response<bool> response = new Response<bool>();
            response = await _historiaClinicaService.HistoriaClinicaDetalleActualizar(oHistoriaClinicaDetalleURequest);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }

        /// <summary>
        /// Retorna el detalle de las historias clinicas
        /// </summary>
        /// /// <param name="idHistoriaClinica">Identificador de historia clinica</param>
        /// <returns></returns>
        [HttpPost("HistoriaClinicaDetalleListar/{idHistoriaClinica}")]
        public async Task<IActionResult> HistoriaClinicaDetalleListar(long idHistoriaClinica)
        {
            var response = await _historiaClinicaService.HistoriaClinicaDetalleListar(idHistoriaClinica);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
    }
}

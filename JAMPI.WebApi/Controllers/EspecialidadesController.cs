﻿using System.Threading.Tasks;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;

namespace JAMPI.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class EspecialidadesController : ControllerBase
    {
        private readonly IEspecialidadService _especialidadService;
        private readonly IEspecialidadTipService _especialidadTipService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="especialidadService"></param>
        ///  /// <param name="tipSaludService"></param>
        /// 
        public EspecialidadesController(IEspecialidadService especialidadService, IEspecialidadTipService especialidadTipService)
        {
            _especialidadService = especialidadService;
            _especialidadTipService = especialidadTipService;
        }
        /// <summary>
        /// Listar Todas las especialidades médicas
        /// </summary>
        /// <returns>Lista de Especialidades</returns>
        [HttpPost]
        public async Task<IActionResult> Listar()
        {
            return Ok(await _especialidadService.Listar());
        }
        /// <summary>
        /// Obtiene una especialidad
        /// </summary>
        /// <param name="id">Id de Especialidad</param>
        /// <returns>Objeto Especialidad Medica</returns>
        [HttpPost("{id}")]
        public async Task<IActionResult> Obtener(long id)
        {

            return Ok(await _especialidadService.Obtener(id));
        }
        /// <summary>
        /// Agrega una nueva especialidad
        /// </summary>
        /// <param name="especialidadMedica">Objeto Especialidad medica</param>
        /// <returns>Response.success true en caso se grabe correctamente</returns>
        [HttpPost("Agregar")]
        public async Task<IActionResult> Agregar(EspecialidadRequest especialidadMedica)
        {
            return Ok(await _especialidadService.Agregar(especialidadMedica));
        }
        /// <summary>
        /// Actualiza la especialidad medica
        /// </summary>
        /// <param name="especialidadMedica">Objeto Especialidad medica</param>
        /// <returns>Response.success true en caso se grabe correctamente</returns>
        [HttpPut]
        public async Task<IActionResult> Actualizar(EspecialidadRequest especialidadMedica)
        {
            return Ok(await _especialidadService.Actualizar(especialidadMedica));
        }
        /// <summary>
        /// Elimina la especialidad medica
        /// </summary>
        /// <param name="id">Id de la especialidad médica</param>
        /// <returns>Response.success true en caso se grabe correctamente</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Eliminar(long id)
        {
            return Ok(await _especialidadService.Borrar(id));
        }

        /// <summary>
        /// Lista los tips por idEspecialidad
        /// </summary>
        /// <param name="id">Id de la especialidad médica</param>
        /// <returns>Listado de Tips (3)</returns>
        [HttpPost("ListarTips/{id}")]
        public async Task<IActionResult> ListarTips(long id)
        {
            var response = await _especialidadTipService.ListarPorEspecialidad(id);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
    }
}

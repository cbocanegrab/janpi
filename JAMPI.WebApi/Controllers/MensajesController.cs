﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JAMPI.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MensajesController : ControllerBase
    {
        private readonly IMensajePersonaService _mensajePersonaService;
        private readonly IMensajePersonaDetalleService _mensajePersonaDetalleService;


        public MensajesController(IMensajePersonaService mensajePersonaService, IMensajePersonaDetalleService mensajePersonaDetalleService)
        {
            _mensajePersonaService = mensajePersonaService;
            _mensajePersonaDetalleService = mensajePersonaDetalleService;
        }

        /// <summary>
        /// Listar los grupos de mensajes disponibles del usuario
        /// </summary>
        /// <returns></returns>
        [HttpPost("Mensajes/")]
        public async Task<IActionResult> ListarMensajes()
        {
            return Ok(await _mensajePersonaService.Listar());
        }

        /// <summary>
        /// Listar el chat de un grupo de mensaje
        /// </summary>
        /// <param name="idMensaje">recibe el id del grupo de mensaje. Si no existe se manda 0</param>
        /// <returns></returns>
        [HttpPost("Mensajes/{idMensaje}")]
        public async Task<IActionResult> ListarMensajesDetalle(long idMensaje)
        {
            return Ok(await _mensajePersonaDetalleService.Listar(idMensaje));
        }

        /// <summary>
        /// Agregar un mensaje al grupo de chat
        /// </summary>
        /// <param name="detalleRequest">Objeto que tiene:
        ///  idMensajePersona: El ID del grupo de chat
        ///  idMedico: Si es un nuevo chat (no se tiene idMensajePersona)
        ///  strMensaje: Mensaje de Chat
        /// </param>
        /// <returns></returns>
        [HttpPost("Mensajes/Agregar")]
        public async Task<IActionResult> AgregarDetalleMensaje(MensajePersonaDetalleRequest detalleRequest)
        {
            return Ok(await _mensajePersonaDetalleService.Insert(detalleRequest));
        }
        /// <summary>
        /// Lista remitentes del médico
        /// </summary>
        /// <returns></returns>
        [HttpPost("MensajesMedico/")]
        public async Task<IActionResult> MensajesMedico()
        {
            var response = await _mensajePersonaService.ListarMensajesMedico();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Actualiza el estado de los mensajes leidos
        /// </summary>
        /// <param name="id">identidicador de la conversacion medico-paciente</param>
        /// <returns></returns>
        [HttpPut("UpdateLeido/")]
        public async Task<IActionResult> UpdateLeido(long id)
        {
            var response = await _mensajePersonaDetalleService.UpdateLeido(id);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Registra los mensajes de medicos a pacientes
        /// </summary>
        /// <param name="mensajeMedicoDetalle"></param>
        /// <returns></returns>
        [HttpPost("MensajeMedicoInsert/")]
        public async Task<IActionResult> MensajeMedicoInsert(MensajeMedicoDetalleRequest mensajeMedicoDetalle)
        {
            var response = await _mensajePersonaDetalleService.MensajeMedicoInsert(mensajeMedicoDetalle);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
    }
}

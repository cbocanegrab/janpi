﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JAMPI.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ConsultaController : ControllerBase
    {
        private readonly IConsultaService _consultaService;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="consultaService"></param>
        public ConsultaController(IConsultaService consultaService)
        {
            _consultaService = consultaService;
        }
        /// <summary>
        /// Agrega una nueva consulta (previo al pago)
        /// </summary>
        /// <param name="consulta">Objeto Consulta medica</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Agregar(ConsultaBasicRequest consulta)
        {
            return Ok(await _consultaService.Agregar(consulta));
        }
        /// <summary>
        /// Se confirma la cita con el pago, Se manda el ID de la consulta y el codigo de transaccion
        /// </summary>
        /// <param name="consulta">Objeto Consulta medica</param>
        /// <returns></returns>
        [HttpPost("Programar/")]
        public async Task<IActionResult> Programar(ConsultaProgramaRequest consulta)
        {
            return Ok(await _consultaService.Programar(consulta));
        }
        /// <summary>
        /// Actualiza el triaje médico previa a la cita
        /// </summary>
        /// <param name="triajeRequest">Objeto triaje</param>
        /// <returns>Response.success true cuando se graba correctamente</returns>
        [HttpPut]
        public async Task<IActionResult> ActualizarTriaje(ConsultaTriajeRequest triajeRequest)
        {
            return Ok(await _consultaService.ActualizarTriaje(triajeRequest));
        }
        /// <summary>
        /// Cancela Consulta por Paciente
        /// </summary>
        /// <param name="cancelaRequest">Objeto cancelarequest</param>
        /// <returns>Response.success true en caso se grabe correctamente</returns>
        [HttpPut("CancelarPorPaciente")]
        public async Task<IActionResult> CancelarPorPaciente(ConsultaCancelaRequest cancelaRequest)
        {
            return Ok(await _consultaService.CancelarPorPaciente(cancelaRequest));
        }
        /// <summary>
        /// Cancela Consulta por Medico
        /// </summary>
        /// <param name="cancelaRequest">Objeto cancelarequest</param>
        /// <returns>Response.success true en caso se grabe correctamente</returns>
        [HttpPut("CancelarPorMedico")]
        public async Task<IActionResult> CancelarPorMedico(ConsultaCancelaRequest cancelaRequest)
        {
            return Ok(await _consultaService.CancelarPorMedico(cancelaRequest));
        }
        /// <summary>
        /// Cancela Consulta por Paciente
        /// </summary>
        /// <param name="urlRequest">Objeto de la URL</param>
        /// <returns>Response.success true en caso se grabe correctamente</returns>
        [HttpPatch("ActualizarURL")]
        public async Task<IActionResult> ActualizarURL(ConsultaURLRequest urlRequest)
        {
            return Ok(await _consultaService.ActualizarURL(urlRequest));
        }
        /// <summary>
        /// Elimina Cita
        /// </summary>
        /// <param name="id">ID de Cita</param>
        /// <returns>Response.success true en caso se grabe correctamente</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Borrar(long id)
        {
            return Ok(await _consultaService.Borrar(id));
        }
        /// <summary>
        /// Obtiene los datos de la proxima consulta
        /// </summary>
        /// <returns></returns>
        [HttpPost("ProximaConsulta/")]
        public async Task<IActionResult> ProximaConsulta()
        {
            var response = await _consultaService.ProximaConsulta();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Obtiene los datos de la última consulta
        /// </summary>
        /// <returns></returns>
        [HttpPost("UltimaConsulta/")]
        public async Task<IActionResult> UltimaConsulta()
        {
            var response = await _consultaService.UltimaConsulta();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Registra datos de la transaccion del pago
        /// </summary>
        /// <param name="pagoRequest"></param>
        /// <returns></returns>
        [HttpPut("ProcesarPago/")]
        public async Task<IActionResult> ProcesarPago(ConsultaPagoRequest pagoRequest)
        {
            var response = await _consultaService.ProcesarPago(pagoRequest);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idConsulta"></param>
        /// <returns></returns>
        [HttpPost("LinkConsulta/")]
        public async Task<IActionResult> LinkConsulta(long idConsulta)
        {
            var response = await _consultaService.LinkConsulta(idConsulta);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Registra la valoracion de la consulta
        /// </summary>
        /// <param name="ValoracionRequest"></param>
        /// <returns></returns>
        [HttpPut("ValorarAtencion/")]
        public async Task<IActionResult> ValorarConsulta(ConsultaValoracionRequest ValoracionRequest)
        {
            var response = await _consultaService.ValorarConsulta(ValoracionRequest);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista las ordenes realizadas
        /// </summary>
        /// <returns></returns>
        [HttpPost("ListarOrdenes/")]
        public async Task<IActionResult> Ordenes()
        {
            var response = await _consultaService.Ordenes();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Devuelve información básica de la consulta (medico, especialidad,fecha,hora,motivo consulta)
        /// </summary>
        /// <param name="idConsulta"></param>
        /// <returns></returns>        
        [HttpPost("ConsultaDetalle/")]
        public async Task<IActionResult> ConsultaDetalle(long idConsulta)
        {
            var response = await _consultaService.ConsultaDetalle(idConsulta);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="oSeguimiento"></param>
        /// <returns></returns>
        [HttpPost("InsertarSeguimiento/")]
        public async Task<IActionResult> InsertarSeguimiento(SeguimientoRequest oSeguimiento)
        {
            var response = await _consultaService.InsertarSeguimientoAsync(oSeguimiento);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Obtiene la próxima cita del médico
        /// </summary>
        /// <returns></returns>
        [HttpPost("ProximaConsultaMedico/")]
        public async Task<IActionResult> ProximaConsultaMedico()
        {
            var response = await _consultaService.ProximaConsultaMedico();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista las consultas programadas del meédico
        /// </summary>
        /// <returns></returns>
        [HttpPost("ConsultaProgramadaMedico/")]
        public async Task<IActionResult> ConsultaProgramadaMedico()
        {
            var response = await _consultaService.ConsultaProgramadaMedico();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista todas las consultas atendidas
        /// </summary>
        /// <param name="idEspecialidad">enviar 0 para listar todos</param>
        /// <param name="idEstadoTratamiento">enviar 0 para listar todos</param>
        /// <returns></returns>
        [HttpPost("ConsultaHistoricoMedico/{idEspecialidad}/{idEstadoTratamiento}")]
        public async Task<IActionResult> ConsultaHistoricoMedico(long idEspecialidad, long idEstadoTratamiento)
        {
            var response = await _consultaService.ConsultaHistoricoMedico(idEspecialidad,  idEstadoTratamiento);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Guardar los examenes de una consulta
        /// </summary>
        /// <param name="lConsultaExamen">listados de examenes a guardar, enviar strEstado :'0' para eliminar examen guardado</param>
        /// <returns></returns>
        [HttpPost("ConsultaExamenGuardar/")]
        public async Task<IActionResult> ConsultaExamenGuardar(List<ConsultaExamenGRequest> lConsultaExamen)
        {
            var response = await _consultaService.ConsultaExamenGuardarAsync(lConsultaExamen);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista los examenes por consulta
        /// </summary>
        /// <param name="idConsulta"></param>
        /// <returns></returns>
        [HttpPost("ConsultaExamenListar/")]
        public async Task<IActionResult> ConsultaExamenListar(long idConsulta)
        {
            var response = await _consultaService.ConsultaExamenListar(idConsulta);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Registra la reconsulta
        /// </summary>
        /// <param name="oConsulta"></param>
        /// <returns></returns>
        [HttpPost("ReconsultaInsertar/")]
        public async Task<IActionResult> ReconsultaInsertar(ReconsultaRequest oConsulta)
        {
            var response = await _consultaService.ReconsultaInsertarAsync(oConsulta);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista las consultas por paciente
        /// </summary>
        /// <param name="idPersonaPaciente"></param>
        /// <returns></returns>
        [HttpPost("ConsultaHistoricoPaciente/{idPersonaPaciente}")]
        public async Task<IActionResult> ConsultaHistoricoPaciente(long idPersonaPaciente)
        {
            var response = await _consultaService.ConsultaHistoricoPaciente(idPersonaPaciente);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Inserta la consulta derivada
        /// </summary>
        /// <param name="oConsulta"></param>
        /// <returns></returns>
        [HttpPost("ConsultaDerivadaInsertar/")]
        public async Task<IActionResult> ConsultaDerivadaInsertar(ConsultaDerivadaRequest oConsulta)
        {
            var response = await _consultaService.ConsultaDerivadaInsertarAsync(oConsulta);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Elimina la consulta derivada
        /// </summary>
        /// <param name="idConsultaDerivada"></param>
        /// <returns></returns>
        [HttpDelete("ConsultaDerivadaEliminar/")]
        public async Task<IActionResult> ConsultaDerivadaEliminar(long idConsultaDerivada)
        {
            var response = await _consultaService.ConsultaDerivadaEliminarAsync(idConsultaDerivada);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Obtiene la especialidad y el motivo de consulta apara programar una cita
        /// </summary>
        /// <returns></returns>
        [HttpPost("ConsultaDerivadaObtener/")]
        public async Task<IActionResult> ConsultaDerivadaObtener( )
        {
            var response = await _consultaService.ConsultaDerivadaObtener();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista el avance el seguimiento del tratamiento
        /// </summary>
        /// <param name="idConsulta"></param>
        /// <returns></returns>
        [HttpPost("ConsultaSeguimiento/")]
        public async Task<IActionResult> ConsultaSeguimiento(long idConsulta)
        {
            var response = await _consultaService.ConsultaSeguimiento(idConsulta);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }

        /// <summary>
        /// Lista todas las consultas realizadas por el medico
        /// </summary>
        /// <returns></returns>
        [HttpPost("ConsultaHistoricoMedicoAll/")]
        public async Task<IActionResult> ConsultaHistoricoMedicoAll()
        {
            var response = await _consultaService.ConsultaHistoricoMedicoAll();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }   
        /// <summary>
        /// Retorna el resumen de las ordenes de los examenes por consulta
        /// </summary>
        /// <param name="idConsulta"></param>
        /// <returns></returns>
        [HttpPost("ConsultaExamenResumen/")]
        public async Task<IActionResult> ConsultaExamenResumen(long idConsulta)
        {
            var response = await _consultaService.ConsultaExamenResumen(idConsulta);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// retorna un objeto con los datos para el dashboard de citas para el médico
        /// </summary>
        /// <returns></returns>
        [HttpPost("ConsultaProgramacionMedico/")]
        public async Task<IActionResult> ConsultaProgramacionMedico()
        {
            var response = await _consultaService.ConsultaProgramacion();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista las consultas por fecha
        /// </summary>
        /// <param name="oFechaRequest"></param>
        /// <returns></returns>
        [HttpPost("ConsultaPorFecha/")]
        public async Task<IActionResult> ConsultaPorFecha(FechaRequest oFechaRequest)
        {
            var response = await _consultaService.ConsultaPorFecha(oFechaRequest.strFecha);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
    }
}

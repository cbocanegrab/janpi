﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace JAMPI.WebApi.Controllers
{
    ///
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IMedidaService _medidaService;
        private readonly IConfigMedidaService _configMedidaService;
        private readonly IUsuarioService _usuarioService;
        private readonly INotificacionService _notificacionService;
        ///
        public UsuarioController(IConfiguration configuration, IMedidaService medidaService, INotificacionService notificacionService, 
            IConfigMedidaService configMedidaService, IUsuarioService usuarioService)
        {
            this._configuration = configuration;
            _medidaService = medidaService;
            _notificacionService = notificacionService;
            _configMedidaService = configMedidaService;
            this._usuarioService = usuarioService;
        }

        /// <summary>
        /// Listar los registros de Peso en el Sistema
        /// </summary>
        /// <returns>Lista de Pesos</returns>
        [AllowAnonymous]
        [HttpPost("TipoMedidas/")]
        public async Task<IActionResult> ListarTipoMedidas()
        {
            var response = await _configMedidaService.ListarTiposMedidas();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }

        /// <summary>
        /// Ingresa el Tipo de Medida
        /// </summary>
        /// <returns>Success de confirmación</returns>
        [AllowAnonymous]
        [HttpPost("InsertTipoMedida/")]
        public async Task<IActionResult> InsertTipoMedida(TipoMedidaRequest request)
        {
            var response = await _configMedidaService.InsertTipoMedidas(request);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }

        /// <summary>
        /// Elimina el Tipo de Medida a Mostrar
        /// </summary>
        /// <returns>Retorna Success </returns>
        [AllowAnonymous]
        [HttpPost("DeleteTipoMedida/{idTipoMedida}")]
        public async Task<IActionResult> DeleteTipoMedida(long idTipoMedida)
        {
            var response = await _configMedidaService.EliminarTipoMedidas(idTipoMedida);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }



        /// <summary>
        /// Listar los registros de Peso en el Sistema
        /// </summary>
        /// <returns>Lista de Pesos</returns>
        [AllowAnonymous]
        [HttpPost("Medidas/{idParamTipoMedida}")]
        public async Task<IActionResult> ListarMedidas(long idParamTipoMedida)
        {
            var response = await _medidaService.ListarMedidas(idParamTipoMedida);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }

      
        /// <summary>
        /// Permite registrar los valores de Peso, Glucosa y Presión
        /// </summary>
        /// <param name="medidaRequest">
        /// {
        ///     id: No se usa para insertar
        ///     idParamTipoMedida: 1, Peso; 2: Presión, 3: Glucosa
        ///     valor: Valor del registro
        /// }
        /// </param>
        /// <returns>Success del grabado</returns>
        [AllowAnonymous]
        [HttpPost("MedidaInsert/")]
        public async Task<IActionResult> MedidaInsert(MedidaRequest medidaRequest)
        {
            var response = await _medidaService.InsertMedidas(medidaRequest);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }

        /// <summary>
        /// Permite eliminar los valores de Peso, Glucosa y Presión
        /// </summary>
        /// <param name="medidaRequest">
        /// {
        ///     id: id del registro
        ///     idParamTipoMedida: 1, Peso; 2: Presión, 3: Glucosa
        ///     valor: Valor del registro
        /// }
        /// </param>
        /// <returns>Success del grabado</returns>
        [AllowAnonymous]
        [HttpPost("MedidaDelete/")]
        public async Task<IActionResult> MedidaDelete(MedidaRequest medidaRequest)
        {
            var response = await _medidaService.EliminarMedidas(medidaRequest);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }


        /// <summary>
        /// Listar las notificaciones del usuario
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("Notificaciones/")]
        public async Task<IActionResult> ListarNotificaciones()
        {
            var response = await _notificacionService.Listar();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }

        /// <summary>
        /// Marcar Notificación leída
        /// </summary>
        /// <param name="id">El Id de la Notificación</param>
        /// <returns>Success si se grabó correctamente</returns>
        [AllowAnonymous]
        [HttpPost("Notificaciones/Leido/{id}")]
        public async Task<IActionResult> MarcarLeido(int id)
        {
            var response = await _notificacionService.MarcarLeido(id);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }

    }
}

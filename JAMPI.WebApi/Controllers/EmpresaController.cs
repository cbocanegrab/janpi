﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace JAMPI.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class EmpresaController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IEmpresaService _empresaService;
        private readonly IEmpresaEmpleadoService _empresaEmpleadoService;
        /// 
        public EmpresaController(IConfiguration configuration, IEmpresaService empresaService, IEmpresaEmpleadoService empresaEmpleadoService)
        {
            this._configuration = configuration;
            this._empresaService = empresaService;
            this._empresaEmpleadoService = empresaEmpleadoService;
        }
        /// <summary>
        /// Insertar empresa
        /// </summary>
        /// <param name="empresa"> objeto EmpresaRequest</param>
        /// <returns>Response.success true en caso se grabe correctamente</returns>
        [HttpPost("EmpresaInsertar/")]
        public async Task<IActionResult> EmpresaInsertar(EmpresaRequest empresa)
        {
            //if (ModelState.IsValid)
            //{
                var response = await _empresaService.InsertarAsync(empresa);
                if (response.success) return Ok(response);
                else return BadRequest(response);
            //}
            //else
            //{
            //    response.success = false;
            //    return BadRequest(response);
            //}
        }
        /// <summary>
        /// Actualiza datos de la empresa
        /// </summary>
        /// <param name="empresa"> objeto EmpresaRequest</param>
        /// <returns>Response.success true en caso se grabe correctamente</returns>
        [HttpPut("EmpresaActualizar/")]
        public async Task<IActionResult> EmpresaActualizar(EmpresaRequest empresa)
        {
            //if (ModelState.IsValid)
            //{
            var response = await _empresaService.ActualizarAsync(empresa);
            if (response.success) return Ok(response);
            else return BadRequest(response);
            //}
            //else
            //{
            //    response.success = false;
            //    return BadRequest(response);
            //}
        }
        /// <summary>
        /// Elimina empresa
        /// </summary>
        /// <param name="id">Id de la empresa</param>
        /// <returns></returns>
        [HttpDelete("EmpresaEliminar/")]
        public async Task<IActionResult> EmpresaEliminar(long id)
        {
            var response = await _empresaService.EliminarAsync(id);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Obtiene los datos de la empresa
        /// </summary>
        /// <param name="id">Id empresa</param>
        /// <returns></returns>
        [HttpPost("EmpresaObtener/")]
        public async Task<IActionResult> EmpresaObtener(long id)
        {
            var response = await _empresaService.Obtener(id);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista las empresas
        /// </summary>
        /// <returns></returns>
        [HttpPost("EmpresaListar/")]
        public async Task<IActionResult> EmpresaListar()
        {
            var response = await _empresaService.Listar();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="empresaempleado"></param>
        /// <returns></returns>
        [HttpPost("EmpresaEmpleadoInsertar/")]
        public async Task<IActionResult> EmpresaEmpleadoInsertar(EmpresaEmpleadoIRequest empresaempleado)
        {
            //if (ModelState.IsValid)
            //{
            var response = await _empresaEmpleadoService.InsertarAsync(empresaempleado);
            if (response.success) return Ok(response);
            else return BadRequest(response);
            //}
            //else
            //{
            //    response.success = false;
            //    return BadRequest(response);
            //}
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="empresaempleado"></param>
        /// <returns></returns>
        [HttpPut("EmpresaEmpleadoActualizar/")]
        public async Task<IActionResult> EmpresaEmpleadoActualizar(EmpresaEmpleadoURequest empresaempleado)
        {
            //if (ModelState.IsValid)
            //{
            var response = await _empresaEmpleadoService.ActualizarAsync(empresaempleado);
            if (response.success) return Ok(response);
            else return BadRequest(response);
            //}
            //else
            //{
            //    response.success = false;
            //    return BadRequest(response);
            //}
        }
        /// <summary>
        /// Elimina empresa
        /// </summary>
        /// <param name="idEmpresaEmpleado">Id del empleado</param>
        /// <returns></returns>
        [HttpDelete("EmpresaEmpleadoEliminar/")]
        public async Task<IActionResult> EmpresaEmpleadoEliminar(long idEmpresaEmpleado)
        {
            var response = await _empresaEmpleadoService.EliminarAsync(idEmpresaEmpleado);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Obtiene los datos del empleado
        /// </summary>
        /// <param name="idEmpresaEmpleado">Id del empleado</param>
        /// <returns></returns>
        [HttpPost("EmpresaEmpleadoObtener/")]
        public async Task<IActionResult> EmpresaEmpleadoObtener(long idEmpresaEmpleado)
        {
            var response = await _empresaEmpleadoService.Obtener(idEmpresaEmpleado);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista los empleados de una empresa
        /// </summary>
        /// <param name="idEmpresa"> Id de la empresa</param>
        /// <returns></returns>
        [HttpPost("EmpresaEmpleadoListar/")]
        public async Task<IActionResult> EmpresaEmpleadoListar()
        {
            var response = await _empresaEmpleadoService.Listar();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Resumen semanal de atenciones
        /// </summary>
        /// <returns></returns>
        [HttpPost("ResumenSemanal/")]
        public async Task<IActionResult> ResumenSemanal()
        {
            var response = await _empresaService.ResumenSemanal();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Resumen mensual de atenciones
        /// </summary>
        /// <returns></returns>
        [HttpPost("ResumenMensual/")]
        public async Task<IActionResult> ResumenMensual()
        {
            var response = await _empresaService.ResumenMensual();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Historial de citas de los empleados
        /// </summary>
        /// <returns></returns>
        [HttpPost("HistorialConsultas/")]
        public async Task<IActionResult> HistorialConsultas()
        {
            var response = await _empresaService.HistorialConsultas();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JAMPI.WebApi.Controllers
{
    ///
    [Route("api/[controller]")]
    [ApiController]
    public class PacienteController : ControllerBase
    {
        private readonly IPacienteService _pacienteService;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="pacienteService"></param>
        public PacienteController(IPacienteService pacienteService)
        {
            _pacienteService = pacienteService;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        //[AllowAnonymous]
        //[HttpPost("ListarPacientesDependientes/")]
        //public async Task<IActionResult> ListarPacientesDependientes()
        //{
        //    Response<List<PersonaDependienteResponse>> response = new Response<List<PersonaDependienteResponse>>();
        //    response = await _pacienteService.ListarDependientes();
        //    if (response.success) return Ok(response);
        //    else return BadRequest(response);
        //}
    }
}

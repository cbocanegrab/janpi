﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Interfaces.Common;
using JAMPI.Application.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;

namespace JAMPI.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class CommonController : ControllerBase
    {
        private readonly IParametroService _parametroService;
        private readonly IFileUploadService _fileUploadService;
        ///
        public CommonController(IParametroService parametroService, IFileUploadService fileUploadService)
        {
            _parametroService = parametroService;
            _fileUploadService = fileUploadService;
        }
        /// <summary>
        /// Agrega una nueva consulta (previo al pago)
        /// </summary>
        /// <param name="id">id del tipo Parametro</param>
        /// <returns>
        /// 1	TIPO DOCUMENTO
        /// 4	GRADO INSTRUCCION
        /// 9	TIPO CONSULTA
        /// 13	ESTADO CONSULTA MEDICA
        /// 19	TIPO MEDICO
        /// 22	TIPO CENTRO ESTUDIOS
        /// 26	TIPO ESTUDIO
        /// 31	LOGIN EXTERNOS

        /// </returns>
        [HttpPost("Parametros/{id}")]
        public async Task<IActionResult> Listar(long id)
        {
            return Ok(await _parametroService.ListarParametros(id));
        }


        #region [Upload Files]
        /// <summary>
        /// Inicia la carga de un archivo
        /// </summary>
        /// <returns>Retorna token de procesamiento</returns>
        [HttpPost("FileIniciar/")]
        public async Task<IActionResult> FileIniciar()
        {
            return Ok(await _fileUploadService.InitFile());
        }

        /// <summary>
        /// Procesa los pedidos de bloque de archivos
        /// </summary>
        /// <param name="oFile">Request de Archivo</param>
        /// <returns></returns>
        [HttpPost("FileProceso/")]
        public async Task<IActionResult> FileProceso(FileRequest oFile)
        {
            return Ok(await _fileUploadService.ProccessFile(oFile));
        }


        /// <summary>
        /// Carga la foto del usuario
        /// </summary>
        /// <param name="fileUploadRequest">Request para guardar el archivo.
        /// Se solicita el tipo:
        /// 1: Actualizar foto usuario
        /// 2: Actualizar foto firma
        /// .... futuras cargas
        /// </param>
        /// <returns>
        /// Confirmación response success
        /// </returns>
        [HttpPost("SaveFile/")]
        public async Task<IActionResult> SaveFile(FileUploadRequest fileUploadRequest)
        {
            var response = await _fileUploadService.SaveFile(fileUploadRequest);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }


        #endregion

        /// <summary>
        /// Agrega una nueva consulta (previo al pago)
        /// </summary>
        /// <param name="id">id del tipo Parametro</param>
        /// <returns>
        /// 1	TIPO DOCUMENTO
        /// 4	GRADO INSTRUCCION
        /// 9	TIPO CONSULTA
        /// 13	ESTADO CONSULTA MEDICA
        /// 19	TIPO MEDICO
        /// 22	TIPO CENTRO ESTUDIOS
        /// 26	TIPO ESTUDIO
        /// 31	LOGIN EXTERNOS

        /// </returns>
        [HttpGet("GetFotoPersona/{id}")]
        public async Task<IActionResult> GetFotoByPersona(long id)
        {
            var ruta = await _fileUploadService.GetFotoByPersona(id);
            var bytes = await System.IO.File.ReadAllBytesAsync(ruta);
            var type = "images/" + Path.GetExtension(ruta).Replace(".", "").ToLower();
            return File(bytes, type);
        }

        [HttpGet("GetFotoMedico/{id}")]
        public async Task<IActionResult> GetFotoByMedico(long id)
        {
            var ruta = await _fileUploadService.GetFotoByMedico(id);
            var bytes = await System.IO.File.ReadAllBytesAsync(ruta);
            var type = "images/" + Path.GetExtension(ruta).Replace(".", "").ToLower();
            return File(bytes, type);
        }

        [HttpGet("GetFotoUsuario/{id}")]
        public async Task<IActionResult> GetFotoByUsuario(long id)
        {
            var ruta = await _fileUploadService.GetFotoByUsuario(id);
            var bytes = await System.IO.File.ReadAllBytesAsync(ruta);
            var type = "images/" + Path.GetExtension(ruta).Replace(".", "").ToLower();
            return File(bytes, type);
        }

        [HttpGet("GetFirmaMedico/{id}")]
        public async Task<IActionResult> GetFirmaMedico(long id)
        {
            var ruta = await _fileUploadService.GetFirmaMedico(id);
            var bytes = await System.IO.File.ReadAllBytesAsync(ruta);
            var type = "images/" + Path.GetExtension(ruta).Replace(".", "").ToLower();
            return File(bytes, type);
        }

    }
}

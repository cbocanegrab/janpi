﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JAMPI.WebApi.Controllers
{
    ///
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class MedicoController : ControllerBase
    {
        private readonly IMedicoService _medicoService;
        private readonly IMedicoHorarioService _medicoHorarioService;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="medicoService"></param>
        /// <param name="medicoHorarioService"></param>
        public MedicoController(IMedicoService medicoService, IMedicoHorarioService medicoHorarioService)
        {
            _medicoService = medicoService;
            _medicoHorarioService = medicoHorarioService;
        }
        /// <summary>
        /// Lista los medicos registrados en el sistema
        /// </summary>
        /// <returns>Lista de médicos</returns>
        [HttpPost]
        public async Task<IActionResult> Listar()
        {
            return Ok(await _medicoService.Listar());
        }
        /// <summary>
        /// Lista los medicos registrados en el sistema
        /// </summary>
        /// <returns>Lista de médicos</returns>
        [HttpPost("PorEspecialidad/{id}")]
        public async Task<IActionResult> ListarPorEspecialidad(long id)
        {
            return Ok(await _medicoService.ListarPorEspecialidad(id));
        }
        /// <summary>
        /// Obtiene un Medico especifico
        /// </summary>
        /// <param name="id">Id de Medico</param>
        /// <returns>Objeto Medico</returns>
        [HttpPost("{id}")]
        public async Task<IActionResult> Obtener(long id)
        {
            return Ok(await _medicoService.Obtener(id));
        }
        /// <summary>
        /// Agrega un nuevo medico
        /// </summary>
        /// <param name="medico">Objeto medico</param>
        /// <returns>Response.success si se graba correctamente</returns>
        [HttpPost("Agregar/")]
        public async Task<IActionResult> Agregar(Medico medico)
        {
            return Ok(await _medicoService.Agregar(medico));
        }
        /// <summary>
        /// Actualiza la información del médico
        /// </summary>
        /// <param name="medico">Objeto médico</param>
        /// <returns>Response.success si se graba correctamente</returns>
        [HttpPut]
        public async Task<IActionResult> Actualizar(Medico medico)
        {
            return Ok(await _medicoService.Actualizar(medico));
        }
        /// <summary>
        /// Eliminar un médico
        /// </summary>
        /// <param name="id">Id Medico</param>
        /// <returns>Response.success si se graba correctamente</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Eliminar(long id)
        {
            return Ok(await _medicoService.Borrar(id));
        }
        /// <summary>
        /// Lista los médicos que atendieron al paciente
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("ListarMedicosbyPaciente/")]
        public async Task<IActionResult> ListarMedicosbyPaciente()
        {
            Response<List<MedicoMinResponse>> response = new Response<List<MedicoMinResponse>>();
            response = await _medicoService.ListarMedicosbyUsuario();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista los médicos disponibles para atencón de emergecia
        /// </summary>
        /// <param name="idEspecialidad">enviar 0 si se requiere todas la especialidades</param>
        /// <returns>Lista de médicos</returns>
        [HttpPost("ListarMedicoEmergencia/{idEspecialidad}")]
        public async Task<IActionResult> ListarMedicoEmergencia(long idEspecialidad)
        {
            Response<List<MedicoMinResponse>> response = new Response<List<MedicoMinResponse>>();
            response = await _medicoService.ListarMedicoEmergencia(idEspecialidad);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista los médicos disponibles con sus horas de consulta disponible
        /// </summary>
        /// <param name="oMedicoDisponibleRequest"></param>
        /// <returns></returns>
        [HttpPost("ListarMedicoDisponible/")]
        public async Task<IActionResult> ListarMedicoDisponible(MedicoDisponibleRequest oMedicoDisponibleRequest)
        {
            Response<List<MedicoDisponibleHorarioResponse>> response = new Response<List<MedicoDisponibleHorarioResponse>>();
            response = await _medicoService.ListarMedicoDisponible(oMedicoDisponibleRequest.idEspecialidad, oMedicoDisponibleRequest.strFechaConsulta);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }

        /// <summary>
        /// Obtiene la información detalla de un medico para usar como vista de CV
        /// </summary>
        /// <param name="idMedico">Id del Medico del cual se desea ver su CV</param>
        /// <returns></returns>
        [HttpPost("GetMedicoCV/{idMedico}")]
        public async Task<IActionResult> MedicoCV(long idMedico)
        {
            return Ok(await _medicoService.GetMedicoCV(idMedico));
            
        }
        /// <summary>
        /// Inserta los horarios de lo médico, segun las fechas ingresadas, se guradarán "n" registros según el intervalo de fechas
        /// </summary>
        /// <param name="MedicoHorarioIRequest"></param>
        /// <returns></returns>
        [HttpPost("MedicoHorarioInsertar/")]
        public async Task<IActionResult> InsertarMedicoHorario(MedicoHorarioIRequest MedicoHorarioIRequest)
        {
            Response<bool> response = new Response<bool>();
            response = await _medicoHorarioService.InsertarAsync(MedicoHorarioIRequest);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Actualiza los horarios de los médicos, la actualización será por día
        /// </summary>
        /// <param name="MedicoHorarioURequest"></param>
        /// <returns></returns>
        [HttpPut("MedicoHorarioActualizar/")]
        public async Task<IActionResult> ActualizarMedicoHorario(MedicoHorarioURequest MedicoHorarioURequest)
        {
            Response<bool> response = new Response<bool>();
            response = await _medicoHorarioService.ActualizarAsync(MedicoHorarioURequest);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Elimina el horario del medico por día
        /// </summary>
        /// <param name="idMedicoHorario"></param>
        /// <returns></returns>
        [HttpDelete("HorarioEliminarMedico/")]
        public async Task<IActionResult> EliminarMedicoHorario(long idMedicoHorario)
        {
            Response<bool> response = new Response<bool>();
            response = await _medicoHorarioService.EliminarAsync(idMedicoHorario);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Obtiene el horario de un día determinado
        /// </summary>
        /// <param name="idMedicoHorario"></param>
        /// <returns></returns>
        [HttpPost("MedicoHorarioObtener/")]
        public async Task<IActionResult> ObtenerMedicoHorario(long idMedicoHorario)
        {
            Response<MedicoHorarioResponse> response = new Response<MedicoHorarioResponse>();
            response = await _medicoHorarioService.Obtener(idMedicoHorario);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista todos los horarios por idMedicoEspecialidad
        /// </summary>
        /// <param name="idMedicoEspecialidad"></param>
        /// <returns></returns>
        [HttpPost("MedicoHorarioListar/")]
        public async Task<IActionResult> ListarMedicoHorario(long idMedicoEspecialidad)
        {
            Response<List<MedicoHorarioResponse>> response = new Response<List<MedicoHorarioResponse>>();
            response = await _medicoHorarioService.Listar(idMedicoEspecialidad);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista los medicos por nombre o especialidad
        /// </summary>
        /// <param name="strDescripcion">Nombre o apellido del medico</param>
        /// <param name="idEspecialidad">Id especialidad, enviar 0 para listar todos</param>
        /// <returns></returns>
        [HttpPost("MedicoBusqueda/{idEspecialidad}")]
        public async Task<IActionResult> MedicoBusqueda(string strDescripcion, long idEspecialidad)
        {
            var response = await _medicoService.MedicoBusqueda(strDescripcion, idEspecialidad);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Retorna datos del medico, estudios y experiencia.
        /// </summary>
        /// <param name="idMedicoEspecialidad"></param>
        /// <returns></returns>
        [HttpPost("MedicoCVMin/{idMedicoEspecialidad}")]
        public async Task<IActionResult> MedicoDetalle(long idMedicoEspecialidad)
        {
            var response = await _medicoService.MedicoDetalle(idMedicoEspecialidad);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Obtiene la valoracion y cantidad de citas atendidas
        /// </summary>
        /// <returns></returns>
        [HttpPost("MedicoValoracion/")]
        public async Task<IActionResult> MedicoValoracion()
        {
            var response = await _medicoService.MedicoValoracion();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista los medicos por nombre o especialidad
        /// </summary>
        /// <param name="idEspecialidad">Id especialidad, enviar 0 para listar todos</param>
        /// <returns></returns>
        [HttpPost("MedicoContacto/{idEspecialidad}")]
        public async Task<IActionResult> MedicoContacto( long idEspecialidad)
        {
            var response = await _medicoService.MedicoContacto(idEspecialidad);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Insertar una especialidad a un medico
        /// </summary>
        /// <param name="oMedicoEspecialidadIRequest"> objeto para inserción MedicoEspecialidadIRequest</param>
        /// <returns>Response.success true en caso se grabe correctamente</returns>
        [HttpPost("MedicoEspecialidadInsertar/")]
        public async Task<IActionResult> MedicoEspecialidadInsertar(MedicoEspecialidadIRequest oMedicoEspecialidadIRequest)
        {
            Response<long> response = new Response<long>();
            response = await _medicoService.MedicoEspecialidadInsertar(oMedicoEspecialidadIRequest);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }

        /// <summary>
        /// Actaliza la especialidad de un médico
        /// </summary>
        /// <param name="oMedicoEspecialidadURequest">objeto para la actualización de medico especialidad</param>
        /// <returns></returns>
        [HttpPut("MedicoEspecialidadActualizar/")]
        public async Task<IActionResult> MedicoEspecialidadActualizar(MedicoEspecialidadURequest oMedicoEspecialidadURequest)
        {
            Response<bool> response = new Response<bool>();
            response = await _medicoService.MedicoEspecialidadActualizar(oMedicoEspecialidadURequest);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Elimina especialidad de médico
        /// </summary>
        /// <param name="idMedicoEspecialidad">Id de la especualidad</param>
        /// <returns></returns>
        [HttpDelete("MedicoEspecialidadEliminar/{idMedicoEspecialidad}")]
        public async Task<IActionResult> MedicoEspecialidadEliminar(long idMedicoEspecialidad)
        {
            Response<bool> response = new Response<bool>();
            response = await _medicoService.MedicoEspecialidadEliminar(idMedicoEspecialidad);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
                /// <summary>
        /// Retorna las especialidades asociadas al medico.
        /// </summary>
        /// /// <param name="idMedico">Identificador del medico</param>
        /// <param name="idMedicoEspecialidad">Identificador de la especialidad del medico</param>
        /// <returns></returns>
        [HttpPost("MedicoEspecialidadListar/{idMedico}/{idMedicoEspecialidad}")]
        public async Task<IActionResult> MedicoEspecialidadListar(long idMedico, long idMedicoEspecialidad)
        {
            var response = await _medicoService.MedicoEspecialidadListar(idMedico, idMedicoEspecialidad);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }

        /// <summary>
        /// Insertar experiecnia a un medico
        /// </summary>
        /// <param name="oMedicoExperienciaIRequest"> objeto para inserción MedicoExperienciaIRequest</param>
        /// <returns>Response.success true en caso se grabe correctamente</returns>
        [HttpPost("MedicoExperienciaInsertar/")]
        public async Task<IActionResult> MedicoExperienciaInsertar(MedicoExperienciaIRequest oMedicoExperienciaIRequest)
        {
            Response<long> response = new Response<long>();
            response = await _medicoService.MedicoExperienciaInsertar(oMedicoExperienciaIRequest);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }

        /// <summary>
        /// Actaliza la experienciade un médico
        /// </summary>
        /// <param name="oMedicoExperienciaURequest">objeto para la actualización de medico experiencia</param>
        /// <returns></returns>
        [HttpPut("MedicoExperienciaActualizar/")]
        public async Task<IActionResult> MedicoExperienciaActualizar(MedicoExperienciaURequest oMedicoExperienciaURequest)
        {
            var response = await _medicoService.MedicoExperienciaActualizar(oMedicoExperienciaURequest);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }

        /// <summary>
        /// Lista la experiencia por médico
        /// </summary>
        /// <param name="idMedicoExperiencia"></param>
        /// <returns></returns>
        [HttpDelete("MedicoExperienciaEliminar/")]
        public async Task<IActionResult> MedicoExperienciaEliminar(long idMedicoExperiencia)
        {
            var response = await _medicoService.MedicoExperienciaEliminar(idMedicoExperiencia);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista la experiencia laboral del medico
        /// </summary>
        /// <returns></returns>
        [HttpPost("MedicoExperienciaListar/")]
        public async Task<IActionResult> MedicoExperienciaListar()
        {
            var response = await _medicoService.MedicoExperienciaListar();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }

        /// <summary>
        /// Insertar estudios a un medico
        /// </summary>
        /// <param name="oMedicoEstudioIRequest"> objeto para inserción MedicoEstudioIRequest</param>
        /// <returns>Response.success true en caso se grabe correctamente</returns>
        [HttpPost("MedicoEstudioInsertar/")]
        public async Task<IActionResult> MedicoEstudioInsertar(MedicoEstudioIRequest oMedicoEstudioIRequest)
        {
            Response<long> response = new Response<long>();
            response = await _medicoService.MedicoEstudioInsertar(oMedicoEstudioIRequest);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }

        /// <summary>
        /// Actaliza es estudio de un médico
        /// </summary>
        /// <param name="oMedicoEstudioURequest">objeto para la actualización de medico estudio</param>
        /// <returns></returns>
        [HttpPut("MedicoEstudioActualizar/")]
        public async Task<IActionResult> MedicoEstudioActualizar(MedicoEstudioURequest oMedicoEstudioURequest)
        {
            Response<bool> response = new Response<bool>();
            response = await _medicoService.MedicoEstudioActualizar(oMedicoEstudioURequest);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Elimina experiencia de médico
        /// </summary>
        /// <param name="idMedicoEstudio">Id del estudio</param>
        /// <returns></returns>
        [HttpDelete("MedicoEstudioEliminar/{idMedicoEstudio}")]
        public async Task<IActionResult> MedicoEstudioEliminar(long idMedicoEstudio)
        {
            Response<bool> response = new Response<bool>();
            response = await _medicoService.MedicoEstudioEliminar(idMedicoEstudio);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista los estudios del médico
        /// </summary>
        /// <returns></returns>
        [HttpPost("MedicoEstudioListar/")]
        public async Task<IActionResult> MedicoEstudioListar()
        {
            var response = await _medicoService.MedicoEstudioListar();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista los paciente por médico
        /// </summary>
        /// <returns></returns>
        [HttpPost("PacienteListar/")]
        public async Task<IActionResult> PacienteListar()
        {
            var response = await _medicoService.PacienteListar();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Guarda el flag de paciente destacado
        /// </summary>
        /// <param name="oMedicoPersonaRequest"></param>
        /// <returns></returns>
        [HttpPost("MedicoPersonaGuardar/")]
        public async Task<IActionResult> MedicoPersonaGuardar(MedicoPersonaRequest oMedicoPersonaRequest)
        {
            var response = await _medicoService.MedicoPersonaGuardar(oMedicoPersonaRequest);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Insertar medico
        /// </summary>
        /// <param name="oMedico"></param>
        /// <returns></returns>
        [HttpPost("MedicoInsertar/")]
        public async Task<IActionResult> MedicoInsertar(MedicoIRequest oMedico)
        {
            var response = await _medicoService.MedicoInsertarAsync(oMedico);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Actualiza los datos del medico
        /// </summary>
        /// <param name="oMedico"></param>
        /// <returns></returns>
        [HttpPut("MedicoActualizar/")]
        public async Task<IActionResult> MedicoActualizar(MedicoURequest oMedico)
        {
            var response = await _medicoService.MedicoActualizarAsync(oMedico);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
    }
}

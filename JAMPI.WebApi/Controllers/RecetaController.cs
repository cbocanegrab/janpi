﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace JAMPI.WebApi.Controllers
{/// <summary>
/// 
/// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class RecetaController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IRecetaService _recetaService;
        /// 
        public RecetaController(IConfiguration configuration, IRecetaService recetaService)
        {
            this._configuration = configuration;
            this._recetaService = recetaService;
        }
        /// <summary>
        /// Insertar la cabecera de la receta
        /// </summary>
        /// <param name="oRecetaIRequest"> objeto para inserción RecetaIRequest</param>
        /// <returns>Response.success true en caso se grabe correctamente</returns>
        [HttpPost("InsertarReceta/")]
        public async Task<IActionResult> InsertarReceta(RecetaIRequest oRecetaIRequest)
        {
            Response<long> response = new Response<long>();
            //if (ModelState.IsValid)
            //{
            response = await _recetaService.InsertarRecetaAsync(oRecetaIRequest);
            if (response.success) return Ok(response);
            else return BadRequest(response);
            //}
            //else
            //{
            //    response.success = false;
            //    return BadRequest(response);
            //}
        }

        /// <summary>
        /// Actaliza la cabecera de la receta
        /// </summary>
        /// <param name="oRecetaURequest">objeto para la actualización de la receta</param>
        /// <returns></returns>
        [HttpPut("ActualizarReceta/")]
        public async Task<IActionResult> ActualizarReceta(RecetaURequest oRecetaURequest)
        {
            Response<bool> response = new Response<bool>();
            //if (ModelState.IsValid)
            //{
            response = await _recetaService.ActualizarRecetaAsync(oRecetaURequest);
            if (response.success) return Ok(response);
            else return BadRequest(response);
            //}
            //else
            //{
            //    response.success = false;
            //    return BadRequest(response);
            //}
        }
        /// <summary>
        /// Elimina Receta
        /// </summary>
        /// <param name="idReceta">Id de la Receta</param>
        /// <returns></returns>
        [HttpDelete("EliminarReceta/")]
        public async Task<IActionResult> EliminarReceta(long idReceta)
        {
            Response<bool> response = new Response<bool>();
            response = await _recetaService.EliminarRecetaAsync(idReceta);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Obtiene los datos de la Receta
        /// </summary>
        /// <param name="idReceta">Id Receta</param>
        /// <returns></returns>
        [HttpPost("ObtenerReceta/")]
        public async Task<IActionResult> ObtenerReceta(long idReceta)
        {
            Response<RecetaResponse> response = new Response<RecetaResponse>();
            response = await _recetaService.ObtenerReceta(idReceta);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista las Recetas
        /// </summary>
        /// <returns></returns>
        [HttpPost("ListarReceta/")]
        public async Task<IActionResult> ListarReceta()
        {
            Response<List<RecetaResponse>> response = new Response<List<RecetaResponse>>();
            response = await _recetaService.ListarReceta();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Insertar un medicamento para la receta
        /// </summary>
        /// <param name="oRecetaMedicamentoIRequest">objeto para inserción de un medicamento</param>
        /// <returns></returns>
        [HttpPost("InsertarRecetaMedicamento/")]
        public async Task<IActionResult> InsertarRecetaMedicamento(RecetaMedicamentoIRequest oRecetaMedicamentoIRequest)
        {
            Response<long> response = new Response<long>();
            //if (ModelState.IsValid)
            //{
            response = await _recetaService.InsertarRecetaMedicamentoAsync(oRecetaMedicamentoIRequest);
            if (response.success) return Ok(response);
            else return BadRequest(response);
            //}
            //else
            //{
            //    response.success = false;
            //    return BadRequest(response);
            //}
        }

        /// <summary>
        /// Actualiza un medicamento de la receta
        /// </summary>
        /// <param name="oRecetaMedicamentoURequest"> objeto que actualiza un medicamento de la receta</param>
        /// <returns></returns>
        [HttpPut("ActualizarRecetaMedicamento/")]
        public async Task<IActionResult> ActualizarRecetaMedicamento(RecetaMedicamentoURequest oRecetaMedicamentoURequest)
        {
            Response<bool> response = new Response<bool>();
            //if (ModelState.IsValid)
            //{
            response = await _recetaService.ActualizarRecetaMedicamentoAsync(oRecetaMedicamentoURequest);
            if (response.success) return Ok(response);
            else return BadRequest(response);
            //}
            //else
            //{
            //    response.success = false;
            //    return BadRequest(response);
            //}
        }
        /// <summary>
        /// Elimina Receta
        /// </summary>
        /// <param name="idRecetaMedicamento">Id del medicamento de la receta </param>
        /// <returns></returns>
        [HttpDelete("EliminarRecetaMedicamento/")]
        public async Task<IActionResult> EliminarRecetaMedicamento(long idRecetaMedicamento)
        {
            Response<bool> response = new Response<bool>();
            response = await _recetaService.EliminarRecetaMedicamentoAsync(idRecetaMedicamento);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Obtiene datos y horario de un medicmento de la receta
        /// </summary>
        /// <param name="idRecetaMedicamento">Id del medicamento de la receta</param>
        /// <returns></returns>
        [HttpPost("ObtenerRecetaMedicamento/")]
        public async Task<IActionResult> ObtenerRecetaMedicamento(long idRecetaMedicamento)
        {
            Response<RecetaMedicamentoResponse> response = new Response<RecetaMedicamentoResponse>();
            response = await _recetaService.ObtenerRecetaMedicamento(idRecetaMedicamento);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista todos los datos y horarios de los medicamentos de la receta
        /// </summary>
        /// <param name="idReceta">Id de la receta</param>
        /// <returns></returns>
        [HttpPost("ListarRecetaMedicamento/")]
        public async Task<IActionResult> ListarRecetaMedicamento(long idReceta)
        {
            Response<List<RecetaMedicamentoResponse>> response = new Response<List<RecetaMedicamentoResponse>>();
            response = await _recetaService.ListarRecetaMedicamento(idReceta);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Insertar los horarios de un medicamento
        /// </summary>
        /// <param name="lMedicamentoHorarioURequest">lista para inserción de los horarios de un medicamento</param>
        /// <returns></returns>
        [HttpPost("InsertarMedicamentoHorario/")]
        public async Task<IActionResult> InsertarMedicamentoHorario(List<MedicamentoHorarioIRequest> lMedicamentoHorarioURequest)
        {
            Response<long> response = new Response<long>();
            //if (ModelState.IsValid)
            //{
            response = await _recetaService.InsertarMedicamentoHorarioAsync(lMedicamentoHorarioURequest);
            if (response.success) return Ok(response);
            else return BadRequest(response);
            //}
            //else
            //{
            //    response.success = false;
            //    return BadRequest(response);
            //}
        }

        /// <summary>
        /// Actualiza los horarios de un medicamento
        /// </summary>
        /// <param name="lMedicamentoHorarioURequest"> listado de los horarios para un medicamento</param>
        /// <returns></returns>
        [HttpPut("ActualizarMedicamentoHorario/")]
        public async Task<IActionResult> ActualizarMedicamentoHorario(List<MedicamentoHorarioURequest> lMedicamentoHorarioURequest)
        {
            Response<bool> response = new Response<bool>();
            //if (ModelState.IsValid)
            //{
            response = await _recetaService.ActualizarMedicamentoHorarioAsync(lMedicamentoHorarioURequest);
            if (response.success) return Ok(response);
            else return BadRequest(response);
            //}
            //else
            //{
            //    response.success = false;
            //    return BadRequest(response);
            //}
        }
        /// <summary>
        /// Lista los tratamientos vigentes
        /// </summary>
        /// <returns></returns>
        [HttpPost("TratamientosVigentes/")]
        public async Task<IActionResult> TratamientosVigentes()
        {
            Response<List<TratamientoResponse>> response = new Response<List<TratamientoResponse>>();
            response = await _recetaService.TratamientosVigentes();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista los tratamientos culminados
        /// </summary>
        /// <param name="idMedico">Enviar 0 en caso de no usar</param>
        /// <param name="idEspecialidad"></param>
        /// <param name="idEstado"></param>
        /// <returns></returns>
        [HttpPost("TratamientosHistoricos/")]
        public async Task<IActionResult> TratamientosHistoricos(long idMedico,long idEspecialidad,long idEstado)
        {
            var response = await _recetaService.TratamientosHistoricos(idMedico, idEspecialidad, idEstado);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista los tratamientos por G
        /// </summary>
        /// <param name="oFechaRequest"></param>
        /// <returns></returns>
        [HttpPost("TratamientosPorFecha/")]
        public async Task<IActionResult> TratamientosPorFecha(FechaRequest oFechaRequest)
        {
            var response = await _recetaService.TratamientosPorFecha(oFechaRequest.strFecha);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista todos los tratamientos incluyendo los medicamentos y horarios para el día hoy
        /// </summary>
        /// <returns></returns>
        [HttpPost("TratamientosEnCurso/")]
        public async Task<IActionResult> TratamientosEnCurso()
        {
            var response = await _recetaService.TratamientosEnCurso();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Obtiene el avance del tratamiento
        /// </summary>
        /// <param name="idConsulta"></param>
        /// <returns></returns>
        [HttpPost("TratamientosSeguimiento/{idConsulta}")]
        public async Task<IActionResult> TratamientosSeguimiento(long idConsulta)
        {
            var response = await _recetaService.TratamientosSeguimiento(idConsulta);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista los tratamientos en curso del médico
        /// </summary>
        /// <returns></returns>
        [HttpPost("TratamientosEnCursoMedico/")]
        public async Task<IActionResult> TratamientosEnCursoMedico()
        {
            var response = await _recetaService.TratamientosSeguimientoMedico();
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Lista los tratamientos vigentes por paciente
        /// </summary>
        /// <param name="idPersonaPaciente">ID del paciente</param>
        /// <returns></returns>
        [HttpPost("TratamientosEnCursoByPaciente/")]
        public async Task<IActionResult> TratamientosEnCursoByPaciente(long idPersonaPaciente)
        {
            var response = await _recetaService.TratamientosEnCursoByPaciente(idPersonaPaciente);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Obtiene los medicamentos de la receta
        /// </summary>
        /// <param name="idConsulta"></param>
        /// <returns></returns>
        [HttpPost("RecetaResumen/")]
        public async Task<IActionResult> RecetaResumen(long idConsulta)
        {
            var response = await _recetaService.RecetaResumen(idConsulta);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
        /// <summary>
        /// Obtiene el tratamiento detallado de la receta
        /// </summary>
        /// <param name="idConsulta"></param>
        /// <returns></returns>
        [HttpPost("TratamientoResumen/")]
        public async Task<IActionResult> TratamientoResumen(long idConsulta)
        {
            var response = await _recetaService.TratamientoResumen(idConsulta);
            if (response.success) return Ok(response);
            else return BadRequest(response);
        }
    }
}

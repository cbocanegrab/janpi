﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class ExternalLoginRequest
    {
        public string strAccessToken { get; set; }
    }
}

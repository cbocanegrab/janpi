﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class PasswordRequest
    {
        public string strUsuario { get; set; }
        public string strClaveActual { get; set; }
        public string strClave1 { get; set; }
        public string strClave2 { get; set; }
    }
}

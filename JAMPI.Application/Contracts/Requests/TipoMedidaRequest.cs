﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class TipoMedidaRequest
    {
        public long id { get; set; }
        public long idParamTipoMedida { get; set; }
    }
}

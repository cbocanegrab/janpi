﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class RecetaMedicamentoURequest
    {
        public long idRecetaMedicamento { get; set; }
        public string strDosis { get; set; }
        public long idMedicamento { get; set; }
        public long idParamUnidadMedida { get; set; }
        public long idParamViaAdministracion { get; set; }
        public string strPresentacion { get; set; }
        public string strFechaInicio { get; set; }
        public int intDias { get; set; }
        public string strComentarioMedicamento { get; set; }
        //public List<MedicamentoFrecuenciaURequest> lMedicamentoFrecuencia { get; set; }
    }
}

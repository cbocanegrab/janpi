﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class MedicoURequest
    {
        public long idMedico { get; set; }
        public long idParamTipoDocumento { get; set; }
        public string strNumeroDocumento { get; set; }
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        public string strSegundoApellido { get; set; }
        public string strSexo { get; set; }
        public long idParamEstadoCivil { get; set; }
        public long idParamTipoMedico { get; set; }
        public string strCelularPrincipal { get; set; }
        public string strCelularSecundario { get; set; }
        public string strDireccion { get; set; }
        public string strCMP { get; set; }
        public long idEspecialidad1 { get; set; }
        public long idEspecialidad2 { get; set; }
    }
}

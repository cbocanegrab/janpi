﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class ConsultaPagoRequest
    {
        public long idConsulta { get; set; }
        public string strTransaccion { get; set; }
        public string strFechaTransaccion { get; set; }
        public decimal decCreditoUsado { get; set; }
    }
}

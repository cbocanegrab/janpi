﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class FileUploadRequest
    {
        public long id { get; set; }
        public string strToken { get; set; }
        public string strNombre { get; set; }
        public int intTipo { get; set; }
    }
}

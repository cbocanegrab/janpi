﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class HistoriaClinicaURequest
    {
        public long idHistoriaClinica { get; set; }
        public string strAntecedenteFamiliar { get; set; }
        public string strAntecedenteMedico { get; set; }
        public string strAntecedenteQuirurgico { get; set; }
        public string strAntecedenteTraumatico { get; set; }
        public string strAntecedenteAlergico { get; set; }
        public string strAntecedenteViciomania { get; set; }
    }
}

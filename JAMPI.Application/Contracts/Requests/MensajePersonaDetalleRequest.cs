﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class MensajePersonaDetalleRequest
    {
        public long idMensajePersona { get; set; }
        public long idMedico { get; set; }
        public string strMensaje { get; set; }
    }
}

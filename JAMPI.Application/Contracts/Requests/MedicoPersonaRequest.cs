﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class MedicoPersonaRequest
    {
        public long idPersona { get; set; }
        public string strDestacado { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class MedicoEspecialidadIRequest
    {
        public long idMedico { get; set; }
        public long idEspecialidad { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class MedicoIRequest
    {

        public int idParamTipoDocumento { get; set; }
        public string strNumeroDocumento { get; set; }
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        public string strSegundoApellido { get; set; }
        public long idParamTipoMedico { get; set; }
        public string strCelularPrincipal { get; set; }
        public string strCorreoPrincipal { get; set; }
        public string strFechaInicio { get; set; }
        public string strFechaFin { get; set; }
        public long idEspecialidad1 { get; set; }
        public long idEspecialidad2 { get; set; }
        
    }
}

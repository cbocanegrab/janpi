﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class ConsultaCancelaRequest
    {
        public long idConsulta { get; set; }
        //public string strObservacion { get; set; }
        public long idParamMotivoCancelacion { get; set; }
    }
}

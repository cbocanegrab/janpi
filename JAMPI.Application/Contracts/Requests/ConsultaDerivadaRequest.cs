﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class ConsultaDerivadaRequest
    {
        public long idConsulta { get; set; }
        public long idEspecialidad { get; set; }
        public string strMotivo { get; set; }
    }
}

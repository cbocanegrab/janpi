﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class MedicoHorarioIRequest
    {
        public long idMedico { get; set; }
        public long idEspecialidad { get; set; }
        public string strFechaInicio { get; set; }
        public string strFechaFin { get; set; }
        public string strHoraInicio { get; set; }
        public string strHoraFin { get; set; }
        
    }
}

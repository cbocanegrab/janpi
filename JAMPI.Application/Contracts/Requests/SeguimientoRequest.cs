﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class SeguimientoRequest
    {
        public long idConsulta { get; set; }
        public long idParamSeguimiento { get; set; }
        public string strDescripcion { get; set; }
        public int intValoracion { get; set; }
    }
}

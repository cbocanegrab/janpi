﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class FechaRequest
    {
        public string strFecha { get; set; }
    }
}

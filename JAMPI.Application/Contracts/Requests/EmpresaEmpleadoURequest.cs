﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class EmpresaEmpleadoURequest
    {
        public long idEmpresaEmpleado { get; set; }
        public long idParamTipoDocumento { get; set; }
        public string strNumeroDocumento { get; set; }
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        public string strSegundoApellido { get; set; }
        public string strSexo { get; set; }
        public string strFechaNacimiento { get; set; }
        public long idParamEstadoCivil { get; set; }
        public string strCelularPrincipal { get; set; }
        public string strCelularSecundario { get; set; }
        public string strCorreoSecundario { get; set; }
        public string strDireccion { get; set; }
        public long idParamGradoInstruccion { get; set; }
        public string strOcupacion { get; set; }
        public string strFechaIngreso { get; set; }
        public string strFechaSalida { get; set; }
        public long idEmpleadoPadre { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class RecetaIRequest
    {
        public long idConsulta { get; set; }
        public string strFechaReceta { get; set; }
        public string strFechaCaducidad { get; set; }
        public string strComentario { get; set; }
    }
}

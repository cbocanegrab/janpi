﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class ConsultaExamenGRequest
    {
        public long idConsulta { get; set; }
        public long idExamen { get; set; }
        public string strEstado { get; set; }

    }
}

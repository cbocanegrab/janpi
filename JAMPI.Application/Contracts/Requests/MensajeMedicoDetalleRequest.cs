﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class MensajeMedicoDetalleRequest
    {
        public long idMensajePersona { get; set; }
        public long idPersona { get; set; }
        public string strMensaje { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class RecuperarClaveRequest
    {
        public string strUsuario { get; set; }
    }
}

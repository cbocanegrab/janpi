﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class ConsultaURLRequest
    {
        public long idConsulta { get; set; }
        public long idParamPlataforma { get; set; }
        public string strURL { get; set; }
    }
}

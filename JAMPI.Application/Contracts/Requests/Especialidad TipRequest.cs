﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class EspecialidadTipRequest
    {
        public long idEspecialidadTip { get; set; }
        public string strTip { get; set; }
        public long idEspecialidad { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class UserUpdateRequest
    {
        public int idPersona { get; set; }
        public int idParamTipoDocumento { get; set; }
        public string strNumeroDocumento { get; set; }
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        public string strSegundoApellido { get; set; }
        public string strSexo { get; set; }
        public string strFechaNacimiento { get; set; }
        public string strUbigeoNacimiento { get; set; }
        public string strUbigeo { get; set; }
        public string strDireccion { get; set; }
        public string strCelularPrincipal { get; set; }
        public int idParamGradoInstr { get; set; }
        public string strOcupacion { get; set; }
        public string strCelularSecundario { get; set; }
        public string strCorreoPrincipal { get; set; }
        public string strCorreoSecundario { get; set; }
    }
}

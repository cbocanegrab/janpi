﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class ConsultaValoracionRequest
    {
        public long idConsulta { get; set; }
        public decimal deValor { get; set; }
        public long idParamTipoValor { get; set; }
        public string strComentario { get; set; }
    }
}

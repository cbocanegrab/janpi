﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class RefreshTokenRequest
    {
        public string strToken { get; set; }
        public string strTokenRefresh { get; set; }
    }
}

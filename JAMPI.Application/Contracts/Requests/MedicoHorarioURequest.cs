﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class MedicoHorarioURequest
    {
        public long idMedicoHorario { get; set; }
        public string strHoraInicio { get; set; }
        public string strHoraFin { get; set; }
    }
}

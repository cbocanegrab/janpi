﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class ConsultaTriajeRequest
    {
        public long idConsulta { get; set; }
        public decimal decPeso { get; set; }
        public decimal decTalla { get; set; }
        public decimal decTemperatura { get; set; }
        public decimal decPresion { get; set; }
        public decimal decOxigenacion { get; set; }
    }
}

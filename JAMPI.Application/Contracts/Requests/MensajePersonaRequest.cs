﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class MensajePersonaRequest
    {
        public long idMensajePersona { get; set; }
        public long idMedico { get; set; }
        public string strMensaje { get; set; }
        public string strFechaEnvia { get; set; }
    }
}

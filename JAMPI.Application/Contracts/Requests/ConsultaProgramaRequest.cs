﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class ConsultaProgramaRequest
    {
        public long idConsulta { get; set; }
        public string strTransaccion { get; set; }
    }
}

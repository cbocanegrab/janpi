﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class MedicoEstudioURequest
    {
        public long idMedicoEstudio { get; set; }
        public string strCentroEstudio { get; set; }
        public string strCarrera { get; set; }
    }
}

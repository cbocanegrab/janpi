﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class PersonaIRequest
    {
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        //public string strSegundoApellido { get; set; }
        public string strCelularPrincipal { get; set; }
        public string strCorreoPrincipal { get; set; }
        public string strClave1 { get; set; }
        public string strClave2 { get; set; }
        public string strUserId { get; set; }
        public long idParamTipoRed { get; set; }
        public long idPersonaSup { get; set; }
    }
}

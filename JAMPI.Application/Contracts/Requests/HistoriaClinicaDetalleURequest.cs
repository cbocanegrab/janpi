﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class HistoriaClinicaDetalleURequest
    {
        public long idHistoriaClinicaDet { get; set; }
        public long idConsulta { get; set; }
        public string strMotivoConsulta { get; set; }
        public string strDetalleEnfermedad { get; set; }
    }
}

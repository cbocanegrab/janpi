﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class MedicoDisponibleRequest
    {
        public long idEspecialidad { get; set; }
        public string strFechaConsulta { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class UserBasicRequest
    {
        public long idParamTipoDocumento { get; set; }
        public string strNumeroDocumento { get; set; }
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        public string strSegundoApellido { get; set; }
        public string strCelularPrincipal { get; set; }
        public string strCorreoPrincipal { get; set; }
        public string strToken { get; set; }
    }
}

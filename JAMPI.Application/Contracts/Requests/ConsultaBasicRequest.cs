﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class ConsultaBasicRequest
    {
        public long idMedicoEspecialidad { get; set; }
        public string strFechaConsulta { get; set; }
        public string strHora { get; set; }
        public long idParamTipoConsulta { get; set; }
        public long idConsultaPadre { get; set; }
        public decimal deDescuento { get; set; }
        public string strMotivo { get; set; }
        public long idConsultaDerivada { get; set; }
        public long idPersona { get; set; }
        public long idTipoUrgencia { get; set; }
    }
}

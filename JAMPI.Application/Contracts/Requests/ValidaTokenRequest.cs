﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class ValidaTokenRequest
    {
        public string strUsuario { get; set; }
        public string strToken { get; set; }
    }
}

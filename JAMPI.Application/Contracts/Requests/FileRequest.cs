﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class FileRequest
    {
        /// <summary>
        /// Token de Operacion
        /// </summary>
        public string strToken { get; set; }
        /// <summary>
        /// Contenido Blob del documento
        /// </summary>
        public string strSection { get; set; }
        /// <summary>
        /// Nombre de archivo. Se manda al final de la operación
        /// </summary>
        public string strFileName { get; set; }
        /// <summary>
        /// Indica si es la parte final
        /// </summary>
        public int intFin { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class MedicamentoHorarioIRequest
    {
        public long idRecetaMedicamento { get; set; }
        public string strHora { get; set; }
    }
}

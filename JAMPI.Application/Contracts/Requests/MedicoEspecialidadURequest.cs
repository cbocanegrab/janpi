﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class MedicoEspecialidadURequest
    {
        public long idMedicoEspecialidad { get; set; }
        public long idEspecialidad { get; set; }
    }
}

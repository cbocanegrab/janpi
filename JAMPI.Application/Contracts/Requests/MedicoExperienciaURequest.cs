﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class MedicoExperienciaURequest
    {
        public long idMedicoExperiencia { get; set; }
        public string strPuesto { get; set; }
        public string strInstitucionExperiencia { get; set; }
        public string strAnioInicio { get; set; }
        public string strAnioFin { get; set; }
        public string strMesInicio { get; set; }
        public string strMesFin { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class MedicamentoHorarioURequest
    {
        public long idMedicamentoFrecuencia { get; set; }
        public string strHora { get; set; }
        public string strEstado { get; set; }
    }
}

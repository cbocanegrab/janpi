﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class EspecialidadRequest
    {
        public long idEspecialidad { get; set; }
        public string strEspecialidad { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class ReconsultaRequest
    {
        public long idConsulta { get; set; }
        public string strFechaConsulta { get; set; }
        public string strHora { get; set; }
        public string strMotivo{ get; set; }
        public string decDescuento { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class TratamientoSeguimientoResponse
    {
        public long idConsulta { get; set; }
        public long idMedico { get; set; }
        public long idMedicoEspecialidad { get; set; }
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        public string strSegundoApellido { get; set; }
        public string strEspecialidad { get; set; }
        public string strFechaReceta { get; set; }
        public int intDiasTranscurridos { get; set; }
        public int intDiasTratamiento { get; set; }
        public string strMotivo { get; set; }

        public decimal decValoracion { get; set; }
    }
}

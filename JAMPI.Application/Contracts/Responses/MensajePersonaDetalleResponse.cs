﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class MensajePersonaDetalleResponse
    {
        public long id { get; set; }
        public long idPersonaEnvia { get; set; }
        public string strRutaFoto { get; set; }
        public long idMedico { get; set; }
        public string strNombre { get; set; }
        public string strMensaje { get; set; }
        public string strFechaEnvio { get; set; }
        public string strFechaLeido { get; set; }
        public int strLeido { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class MedicoValoracionResponse
    {
        public decimal decRating { get; set; }
        public int intCitasRealizadas { get; set; }
    }
}

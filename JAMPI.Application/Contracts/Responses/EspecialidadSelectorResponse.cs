﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class EspecialidadSelectorResponse
    {
        public long idEspecialidad { get; set; }
        public string strEspecialidad { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class ComentarioResponse
    {
        public long idConsulta { get; set; }
        public string strComentario { get; set; }
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        public string strSegundoApellido { get; set; }
        public decimal decValor { get; set; }
        public string strTiempoValoracion { get; set; }
    }
}

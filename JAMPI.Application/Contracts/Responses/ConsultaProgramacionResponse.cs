﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class ConsultaProgMin
    {
        public long idConsulta { get; set; }
        public long idParamPlataforma { get; set; }
        public string strURL { get; set; }
        public string strFechaConsulta { get; set; }
        public string strHora { get; set; }
        public string strFechaConsultaTexto { get; set; }
        public string strMotivo { get; set; }
        public long idMedico { get; set; }
        public string strNombreMedico { get; set; }
        public long idEspecialidad { get; set; }
        public string strEspecialidad { get; set; }
        public string strNombrePaciente { get; set; }
        public string strTiempoFaltante { get; set; }
    }
    public class ConsultaProgramacionResponse
    {
        public ConsultaProgMin oConsultaCercana { get; set; }
        public List<ConsultaProgMin> lConsultaHoy { get; set; }
        public ConsultaProgMin oConsultaProxima { get; set; }
        public ConsultaProgMin oConsultaUltima { get; set; }

    }
}

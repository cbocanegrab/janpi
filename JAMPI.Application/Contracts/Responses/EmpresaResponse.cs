﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class EmpresaResponse
    {
        public long idEmpresa { get; set; }
        public string strRUC { get; set; }
        public string strRazonSocial { get; set; }
        public string strDireccionComercial { get; set; }
        public string strTelefono { get; set; }
        public string strUbigeo { get; set; }
    }
}

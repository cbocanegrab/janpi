﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class ExamenMin 
    {
        public string strTipo { get; set; }
        public string strExamen { get; set; }
    }
    public class ExamenResumenResponse
    {
        public PersonaMin oPersona { get; set; }
        public List<ExamenMin> lExamen { get; set; }
    }
}

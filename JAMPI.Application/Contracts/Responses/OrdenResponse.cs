﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class OrdenResponse
    {
        public long idCOnsulta { get; set; }
        public string strFechaConsulta { get; set; }
        public string strHora { get; set; }
        public string strMotivo { get; set; }
        public long idMedicoEspecialidad { get; set; }
        public string strEspecialidad { get; set; }
        public long idMedico { get; set; }
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        public string strSegundoApellido { get; set; }
        public string strFechaTransaccion { get; set; }
        public string strTarjeta { get; set; }
        public long idParamTipoTarjeta { get; set; }
        public string strParamTipoTarjeta { get; set; }
        public string strParamTipoTransaccion { get; set; }
        public decimal decCreditoUsado { get; set; }
        public decimal decPago { get; set; }
    }
}

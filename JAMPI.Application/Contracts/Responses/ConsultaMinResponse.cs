﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class ConsultaDetalleResponse
    {
        public long idConsulta { get; set; }
        public string strFechaConsulta { get; set; }
        public string strHora { get; set; }
        public long idMedico { get; set; }
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        public string strSegundoApellido { get; set; }
        public long idEspecialidad { get; set; }
        public string strEspecialidad { get; set; }
        public string strMotivo { get; set; }
        public string strPaciente { get; set; }
        public int intTolerancia { get; set; }
        public string strUrl { get; set; }
        public long idParamPlataforma { get; set; }
        public string strPlataforma { get; set; }

    }
}

﻿using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class TratamientoConsulta
    {
        public string strFechaFinTratamiento { get; set; }
        public int intDiasTratamiento { get; set; }
    }
    public class ConsultaSeguimientoResponse
    {
        public TratamientoConsulta oTratamientoConsulta { get; set; }
        public List<ConsultaSeguimiento> lSeguimiento { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class MedicamentoMinResponse
    {
        public string strMedicamento { get; set; }
        public string strHora { get; set; }
        public string strComentarioMedicamento { get; set; }
    }
}

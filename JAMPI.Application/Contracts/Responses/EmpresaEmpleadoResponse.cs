﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class EmpresaEmpleadoResponse
    {
        public long idEmpresaEmpleado { get; set; }
        public long idEmpresa { get; set; }
        public string strNumeroDocumento { get; set; }
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        public string strSegundoApellido { get; set; }
        public string strCelularPrincipal { get; set; }
        public string strCorreoPrincipal { get; set; }
        public string strFechaIngreso { get; set; }
        public string strFechaSalida { get; set; }
        public long idEmpleadoPadre { get; set; }
        public string strNombreCompletoPadre { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class TipoEmergenciaTipResponse
    {
        public long idEmergenciaTip { get; set; }
        public string strDescripcion { get; set; }

    }
}

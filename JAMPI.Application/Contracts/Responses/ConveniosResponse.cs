﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class ConveniosResponse
    {
        public long idConvenio { get; set; }
        public string strEntidad { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class MedicamentoResponse
    {
        public long idMedicamento { get; set; }
        public string strMedicamento { get; set; }
    }
}

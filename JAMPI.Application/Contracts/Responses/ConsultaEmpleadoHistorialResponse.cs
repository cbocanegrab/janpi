﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class ConsultaEmpleadoHistorialResponse
    {
        public long idEmpresaEmpleado { get; set; }
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        public string strSegundoApellido { get; set; }
        public Int64 intCitas { get; set; }
        public string strUltimaCita { get; set; }
        public string strHora { get; set; }
        public string strTiempoHampik { get; set; }
        public string strEstadoEmpleado { get; set; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class ConsultaDerivadaResponse
    {
        public long idConsultaDerivada { get; set; }
        public long idEspecialidad { get; set; }
        public string strMotivo { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class ConsultaExamenResponse
    {
        public long idConsultaExamen { get; set; }
        public long idExamen { get; set; }
        public string strExamen { get; set; }
    }
}

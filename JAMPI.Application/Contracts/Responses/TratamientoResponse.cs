﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class TratamientoResponse
    {
        public long idConsulta { get; set; }
        public string strFechaConsulta { get; set; }
        public string strFechaConsultaTexto { get; set; }
        public string strMotivo { get; set; }
        public string strEstadoConsulta { get; set; }
        public long idMedico { get; set; }
        public long idMedicoEspecialidad { get; set; }
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        public string strSegundoApellido { get; set; }
        public long idEspecialidad { get; set; }
        public string strEspecialidad { get; set; }
        
    }
}

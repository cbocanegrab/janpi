﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class PersonaResponse
    {
        public long idPersona { get; set; }
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        public string strSegundoApellido { get; set; }
        public string strSexo { get; set; }
        public string strFechaNacimiento { get; set; }
        public long idParamTipoDocumento { get; set; }
        public string strNumeroDocumento { get; set; }
        public long idParamEstadoCivil { get; set; }
        public string strCelularPrincipal { get; set; }
        public string strCelularSecundario { get; set; }
        public string strCorreoPrincipal { get; set; }
        public string strCorreoSecundario { get; set; }
        public string strDireccion { get; set; }
        public long idParamGradoInstr { get; set; }
        public string strOcupacion { get; set; }
        public string strParentesco { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class TipoMedidasResponse
    {
        public long id { get; set; }
        public decimal idParamTipoMedida{ get; set; }
        public string strTipoMedida { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{

    public class TratamientoMin
    {
        public string strHorarios { get; set; }
        public string strMedicamento { get; set; }
        public string strDosis { get; set; }
        public string strUnidadMedida { get; set; }
        public int intDias { get; set; }
        public string strComentarioMedicamento { get; set; }
        public string strPresentacion { get; set; }

    }
    public class TratamientoResumenResponse
    {
        public PersonaMin oPersona { get; set; }
        public List<TratamientoMin> lTratamiento { get; set; }
    }
}

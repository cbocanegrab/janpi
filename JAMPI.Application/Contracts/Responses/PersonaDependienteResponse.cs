﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class PersonaDependienteResponse
    {
        public long idPersona { get; set; }
        public long idUsuario { get; set; }
        public string strNumeroDocumento { get; set; }
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        public string strSegundoApellido { get; set; }
        public string strNombreCompleto { get; set; }
        public string strVinculoEstado { get; set; }
        public string strParentesco { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class MedicoCVInfo
    {
        public long idMedico { get; set; }
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        public string strSegundoApellido { get; set; }
        public decimal decRating { get; set; }
        public int intReviews { get; set; }
        public string strFechaRegistro { get; set; }
        public long idEspecialidad1 { get; set; }
        public string strEspecialidad1 { get; set; }
        public long idEspecialidad2 { get; set; }
        public string strEspecialidad2 { get; set; }
        public string strRutaFoto { get; set; }
        public string strRutaFirma { get; set; }
    }
    public class MedicoCVExperiencia
    {
        public string strPuesto { get; set; }
        public string strDescripcion { get; set; }
        public string strInstitucionExperiencia { get; set; }
        public string strFechaInicio { get; set; }
        public string strFechaFin { get; set; }
    }
    public class MedicoCVEstudios { 
        public string strUbigeo { get; set; }
        public string strDescripcion { get; set; }
        public string strTipoCentro { get; set; }
        public string strCentroEstudio { get; set; }
        public string strTipoEstudio { get; set; }
        public string strCarrera { get; set; }
        public int intAnio { get; set; }
    }

    public class MedicoCVComentarios { 
        public decimal decValor { get;set; }
        public string strDescripcion { get; set; }
        public string strComentario { get; set; }
    }

    public class MedicoCVResponse
    {
        public MedicoCVInfo MedicoInfo { get; set; }
        public List<MedicoCVExperiencia> lMedicoExpediencia { get; set; }
        public List<MedicoCVEstudios> lMedicoEstudios { get; set; }
        public List<MedicoCVComentarios> lMedicoComentarios { get; set; }
    }
}

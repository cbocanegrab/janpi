﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class TratamientoPorFechaResponse
    {
        public long idConsulta { get; set; }
        public long idRecetaMedicamento { get; set; }
        public long idMedicamento { get; set; }
        public string strMedicamento { get; set; }
        public long idParamUnidadMedida { get; set; }
        public string strUnidadMedida { get; set; }
        public long idParamViaAdministracion { get; set; }
        public string strViaAdministracion { get; set; }
        public string strFechaInicio { get; set; }
        public string strHora { get; set; }
        public string strComentarioMedicamento { get; set; }
    }
}

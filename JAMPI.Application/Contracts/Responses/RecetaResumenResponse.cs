﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class PersonaMin
    {
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        public string strSegundoApellido { get; set; }
        public string strDireccion { get; set; }
        public string strFechaCaducidad { get; set; }
        public string strFechaFinTratamiento { get; set; }
        public string strFechaInicioTratamiento { get; set; }
    }

    public class RecetaMedicamentoMin
    {
        public int intUnidades { get; set; }
        public string strMedicamento { get; set; }
        public string strDosis { get; set; }
        public string strUnidadMedida  { get; set; }
        public string strPresentacion { get; set; }

    }
    public class RecetaResumenResponse
    {
        public PersonaMin oPersona {get;set;}
        public List<RecetaMedicamentoMin> lRecetaMedicamento { get; set; }
    }
}

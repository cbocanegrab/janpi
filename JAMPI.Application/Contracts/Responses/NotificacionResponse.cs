﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class NotificacionResponse
    {
        public long idNotificacion { get; set; }
        public string strNotificacion { get; set; }
        public string strFechaNotificacion { get; set; }
        public string strLeido { get; set; }
    }
}

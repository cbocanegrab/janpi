﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class Horario
    {
        public string strHora { get; set; }
    }
    public class MedicoDisponibleHorarioResponse
    {
        public long idMedico { get; set; }
        public long idMedicoEspecialidad { get; set; }
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        public string strSegundoApellido { get; set; }
        public long idEspecialidad { get; set; }
        public string strEspecialidad { get; set; }
        public decimal decRating { get; set; }
        public int intReviewsCount { get; set; }
        public List<Horario> lHorario { get; set; }
    }
}

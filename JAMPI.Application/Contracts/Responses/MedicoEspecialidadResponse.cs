﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class MedicoEspecialidadResponse
    {
        public long idMedicoEspecialidad { get; set; }
        public long idMedico { get; set; }
        public long idEspecialidad { get; set; }
        public string strEspecialidad { get; set; }
    }
}

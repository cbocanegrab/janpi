﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class MedicoContactoResponse
    {
        public long idMedico { get; set; }
        public long idMedicoEspecialidad { get; set; }
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        public string strSegundoApellido { get; set; }
        public string strEspecialidad { get; set; }
        public string strCelularPrincipal { get; set; }

    }
}

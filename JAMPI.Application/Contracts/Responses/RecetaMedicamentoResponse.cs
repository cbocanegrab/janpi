﻿using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class RecetaMedicamentoResponse
    {
        public long idRecetaMedicamento { get; set; }
        public long idReceta { get; set; }
        public string strDosis { get; set; }
        public long idMedicamento { get; set; }
        public long idParamUnidadMedida { get; set; }
        public string strUnidadMedida { get; set; }
        public long idParamViaAdministracion { get; set; }
        public string strViaAdministracion { get; set; }
        public string strFechaInicio { get; set; }
        public int intDias { get; set; }
        public string strComentarioMedicamento { get; set; }
        public string strPresentacion { get; set; }

        public List<MedicamentoHorario> lMedicamentoHorario { get; set; }
    }
}

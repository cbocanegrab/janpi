﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class CredentialResponse
    {
        public string strToken { get; set; }
        public string strTokenRefresh { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class MensajePersonaResponse
    {
        public long id { get; set; }
        public long idMedico { get; set; }
        public long idPersona { get; set; }
        public string strMedico { get; set; }
        public int intReview { get; set; }
        public int intMensajes { get; set; }
        public string strFechaMensaje { get; set; }
        public int intNuevos { get; set; }
        public string strFechaUltMensaje { get; set; }
        public string strPaciente { get; set; }
        public string strFechaUltimaCita { get; set; }
    }
}

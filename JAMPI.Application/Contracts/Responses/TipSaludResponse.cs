﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class TipSaludResponse
    {
        public string strTip { get; set; }
    }
}

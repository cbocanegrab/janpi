﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class TipoEmergenciaSelectorResponse
    {
        public long idTipoEmergencia { get; set; }
        public string strTipoEmergencia { get; set; }
    }
}

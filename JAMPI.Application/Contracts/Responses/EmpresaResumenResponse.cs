﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class EmpresaResumenResponse
    {
        public Int64 intCitas { get; set; } 
        public Int64 intUrgencias { get; set; }
        public decimal decValoracion { get; set; }
        public string strPeriodo { get; set; }
    }
}

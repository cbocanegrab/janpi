﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class MedicoDisponibleResponse
    {
        public long idMedico { get; set; } 
        public string strNombre { get; set; }
        public string strPrimerApellido { get; set; }
        public string strSegundoApellido { get; set; }
        public string strCMP { get; set; }
        public string strSexo { get; set; }
        public string strFoto { get; set; }
        public string strHorasLibres { get; set; }
    }
}

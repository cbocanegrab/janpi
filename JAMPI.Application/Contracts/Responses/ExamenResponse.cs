﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class ExamenResponse
    {
        public long idExamen { get; set; }
        public string strExamen { get; set; }
        public List<ExamenDetResponse> lExamenDet { get; set; }
    }
    public class ExamenDetResponse
    {
        public long idExamen { get; set; }
        public string strExamen { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class MedidasResponse
    {
        public long id { get; set; }
        public string strValor { get; set; }
        public string strFechaRegistro { get; set; }
    }
}

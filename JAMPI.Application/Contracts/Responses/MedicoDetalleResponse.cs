﻿using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class MedicoDetalleResponse: MedicoEspecialidad
    {
        public List<MedicoEstudioResponse> lEstudios { get; set; }
        public List<MedicoExperienciaResponse> lExperiencia { get;set; }
        public List<ComentarioResponse> lComentario { get; set; }
    }
    
}

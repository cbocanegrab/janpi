﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Responses
{
    public class MedicoHorarioResponse
    {
        public long idMedicoHorario { get; set; }
        public long idMedicoEspecialidad { get; set; }
        public string strFechaInicio { get; set; }
        public string strHoraInicio { get; set; }
        public string strHoraFin { get; set; }
    }
}

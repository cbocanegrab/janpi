﻿using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IArchivoRepository
    {
        Task<long> NewID(SqlConnection con);
        Task<int> Add(SqlConnection con, Archivo archivo, Session session);
        Task<int> UpdateArchivoTabla(SqlConnection con, long id, long idArchivo, int tipo, Session session);
        Task<string> GetFotoByPersona(SqlConnection con, long id);
        Task<string> GetFotoByMedico(SqlConnection con, long id);
        Task<string> GetFotoByUsuario(SqlConnection con, long id);
        Task<string> GetFirmaMedico(SqlConnection con, long id);
    }
}

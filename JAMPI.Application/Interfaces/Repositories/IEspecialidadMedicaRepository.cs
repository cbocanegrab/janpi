﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IEspecialidadMedicaRepository : IGenericRepository<Especialidad>
    {
        Task<List<EspecialidadSelectorResponse>> GetAllBasic(SqlConnection con);
        Task<ResponseSQL> AddAsync(SqlConnection con, EspecialidadRequest entity);
        Task<ResponseSQL> Update(SqlConnection con, EspecialidadRequest entity);
    }
}

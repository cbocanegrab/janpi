﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IMedicoEstudioRepository
    {
        Task<ResponseSQL> MedicoEstudioInsertar(SqlConnection con, MedicoEstudioIRequest entity, long idUsuario);
        Task<ResponseSQL> MedicoEstudioActualizar(SqlConnection con, MedicoEstudioURequest entity, long idUsuario);
        Task<ResponseSQL> MedicoEstudioEliminar(SqlConnection con, long idMedicoEstudio, long idUsuario);
        Task<List<MedicoEstudio>> GetAllMedicoEstudio(SqlConnection con, long idMedico, long idPersona);
    }
}

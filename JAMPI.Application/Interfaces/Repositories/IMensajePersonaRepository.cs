﻿using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IMensajePersonaRepository
    {
        Task<long> Add(SqlConnection con, MensajePersona entity, SqlTransaction trx);
        Task<List<MensajePersona>> GetAllByPersona(SqlConnection con, long id);
        Task<List<MensajePersona>> MensajesMedico(SqlConnection con, long id);
    }
}

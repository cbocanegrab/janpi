﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
   public interface IPersonaRepository : IGenericRepository<Persona>
    {
        Task<List<PersonaDependienteResponse>> ListarDependientes(SqlConnection con, long idPersona);
        Task<Persona> Credito(SqlConnection con, long idPersona);
        Task<ResponseSQL> InsertarContacto(SqlConnection con, string strDescripcion, long idPersona, long idUsuario);
        Task<PersonaMinResponse> PersonaMin(SqlConnection con, long idPersona);
        Task<ResponseSQL> PersonaActualizar(SqlConnection con, PersonaURequest entity, long idUsuario);
        Task<ResponseSQL> PersonaDepInsertar(SqlConnection con, PersonaDepRequest entity, string strClave, long idUsuario);
        Task<ResponseSQL> PersonaDepActualizar(SqlConnection con, PersonaDepRequest entity, string strClave, long idUsuario);
        Task<ResponseSQL> VinculoActualizar(SqlConnection con, long idPersona, string strEstado, long idUsuario);
        Task<PersonaResponse> PersonaObtener(SqlConnection con, long id);
    }
}

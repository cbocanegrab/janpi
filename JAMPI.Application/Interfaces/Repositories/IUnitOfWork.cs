﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IUnitOfWork
    {
        IUsuarioRepository usuarioRepository { get; }
        IPersonaRepository personaRepository { get; }
        IMedicoRepository medicoRepository { get; }
        IEspecialidadMedicaRepository especialidadMedicaRepository { get; }
        IConsultaRepository consultaRepository { get; }
        IEmpresaRepository empresaRepository { get; }
        IEmpresaEmpleadoRepository empresaEmpleadoRepository { get; }
        IParametroRepository parametroRepository { get; }
        IPersonaMedidaRepository personaMedidaRepository { get; }
        IPersonaConfigMedidaRepository personaConfigMedidaRepository { get; }
        INotificacionRepository notificacionRepository { get; }
        IEspecialidadTipRepository especialidadTipRepository { get; }
        IMaestroRepository maestroRepository { get; }
        IMensajePersonaRepository mensajePersonaRepository { get; }
        IMensajePersonaDetalleRepository mensajePersonaDetalleRepository { get; }
        IMedicoHorarioRepository medicoHorarioRepository { get; }
        IRecetaRepository recetaRepository { get; }
        IMedicoEstudioRepository medicoEstudioRepository { get; }
        IMedicoExperienciaRepository medicoExperienciaRepository { get; }
        IMedicoEspecialidadRepository medicoEspecialidadRepository { get; }
        IConsultaExamenRepository consultaExamenRepository { get; }
		IHistoriaClinicaRepository historiaClinicaRepository { get; }
        IHistoriaClinicaDetalleRepository historiaClinicaDetalleRepository { get; }
        IArchivoRepository archivoRepository { get; }
    }
}

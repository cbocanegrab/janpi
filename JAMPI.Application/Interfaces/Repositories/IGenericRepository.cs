﻿using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IGenericRepository<T> where T : class
    {
        Task<T> Get(SqlConnection con, long id);
        Task<List<T>> GetAll(SqlConnection con);
        Task<ResponseSQL> Add(SqlConnection con, T entity);
        Task<ResponseSQL> Delete(SqlConnection con, T entity);
        Task<ResponseSQL> Update(SqlConnection con, T entity);
    }
}

﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IHistoriaClinicaRepository
    {
        Task<ResponseSQL> HistoriaClinicaInsertar(SqlConnection con, HistoriaClinicaIRequest entity, long idUsuario);
        Task<ResponseSQL> HistoriaClinicaActualizar(SqlConnection con, HistoriaClinicaURequest entity, long idUsuario);
        Task<List<HistoriaClinica>> HistoriaClinicaListar(SqlConnection con, long idPersona);
    }
}

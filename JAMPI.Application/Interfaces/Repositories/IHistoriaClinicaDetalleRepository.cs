﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IHistoriaClinicaDetalleRepository
    {
        Task<ResponseSQL> HistoriaClinicaDetalleInsertar(SqlConnection con, HistoriaClinicaDetalleIRequest entity, long idUsuario);
        Task<ResponseSQL> HistoriaClinicaDetalleActualizar(SqlConnection con, HistoriaClinicaDetalleURequest entity, long idUsuario);
        Task<List<HistoriaClinicaDetalle>> HistoriaClinicaDetalleListar(SqlConnection con, long idHistoriaClinica);
    }
}

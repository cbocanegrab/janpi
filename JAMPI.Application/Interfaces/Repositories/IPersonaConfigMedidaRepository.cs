﻿using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IPersonaConfigMedidaRepository
    {
        Task<List<ConfigMedida>> GetAllByPersona(SqlConnection con, Session session);
        Task<long> Add(SqlConnection con, ConfigMedida entity, SqlTransaction trx);
        Task<int> Delete(SqlConnection con, long id, Session session);
    }
}

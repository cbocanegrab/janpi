﻿using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IMensajePersonaDetalleRepository
    {
        Task<int> Add(SqlConnection con, MensajePersonaDetalle entity, SqlTransaction trx);
        Task<List<MensajePersonaDetalle>> GetAllByPersona(SqlConnection con, long id);
        Task<ResponseSQL> UpdateLeido(SqlConnection con, long id, long session);
    }
}

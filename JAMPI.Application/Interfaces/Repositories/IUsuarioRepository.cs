﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IUsuarioRepository : IGenericRepository<Usuario>
    {
        Task<Usuario> validarLogin(SqlConnection con, LoginRequest loginRequest);
        Task<ResponseSQL> ActivarUsuario(SqlConnection con, LoginRequest loginRequest);
        Task<ResponseSQL> CambiarClave(SqlConnection con, PasswordRequest PasswordRequest);
        Task<Usuario> RecuperarClave(SqlConnection con, string strUsuario);
        Task<Session> obtenerSesion(SqlConnection con, long idUsuario);
        Task<Session> GetLoginExternal(SqlConnection con, string strAccesoToken, long idParamTipoRed);
        Task<ResponseSQL> VerificaToken(SqlConnection con, Usuario usuario);
        Task<ResponseSQL> GuardarToken(SqlConnection con, Usuario usuario);
        Task<ResponseSQL> RestableceClave(SqlConnection con, RestableceClaveRequest obj);
    }
}

﻿using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IParametroRepository
    {
        Task<List<Parametro>> List(SqlConnection con, long idTipoParametro);
    }
}

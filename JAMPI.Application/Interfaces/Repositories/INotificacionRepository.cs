﻿using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface INotificacionRepository
    {
        Task<ResponseSQL> Add(SqlConnection con, Notificacion entity);
        Task<List<Notificacion>> GetAllByPersona(SqlConnection con, long id);
        Task<ResponseSQL> UpdateLeido(SqlConnection con, Notificacion entity);
    }
}

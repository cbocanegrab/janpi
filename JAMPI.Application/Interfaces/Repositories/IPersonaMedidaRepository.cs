﻿using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IPersonaMedidaRepository
    {
        Task<List<PersonaMedida>> GetAllByPersona(SqlConnection con, long idParamTipoMedida, long id);
        Task<int> Add(SqlConnection con, PersonaMedida entity);
        Task<int> Delete(SqlConnection con, PersonaMedida entity);
    }
}

﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IMedicoExperienciaRepository
    {
        Task<ResponseSQL> MedicoExperienciaInsertar(SqlConnection con, MedicoExperienciaIRequest entity, long idUsuario);
        Task<ResponseSQL> MedicoExperienciaActualizar(SqlConnection con, MedicoExperienciaURequest entity, long idUsuario);
        Task<ResponseSQL> MedicoExperienciaEliminar(SqlConnection con, long idMedicoExperiencia, long idUsuario);
        Task<List<MedicoExperiencia>> GetAllMedicoExperiencia(SqlConnection con, long idMedico,long idPersona);

    }
}

﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IConsultaExamenRepository
    {
        Task<ResponseSQL> ConsultaExamenGuardar(SqlConnection con, ConsultaExamenGRequest entity, long idUsuario);
        Task<List<ConsultaExamen>> ConsultaExamenListar(SqlConnection con, long idConsulta);
        Task<ExamenResumenResponse> ConsultaExamenResumen(SqlConnection con, long idConsulta);
    }
}

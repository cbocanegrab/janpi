﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IRecetaRepository
    {
        Task<ResponseSQL> InsertReceta(SqlConnection con, RecetaIRequest entity, long idUsuario);
        Task<ResponseSQL> UpdateReceta(SqlConnection con, RecetaURequest entity, long idUsuario);
        Task<ResponseSQL> DeleteReceta(SqlConnection con, long idReceta, long idUsuario);
        Task<RecetaResponse> GetReceta(SqlConnection con, long id);
        Task<List<RecetaResponse>> GetAllReceta(SqlConnection con, long id);
        Task<ResponseSQL> InsertRecetaMedicamento(SqlConnection con, RecetaMedicamentoIRequest entity, long idUsuario);
        Task<ResponseSQL> UpdateRecetaMedicamento(SqlConnection con, RecetaMedicamentoURequest entity, long idUsuario);
        Task<ResponseSQL> DeleteRecetaMedicamento(SqlConnection con, long idRecetaMedicamento, long idUsuario);
        Task<RecetaMedicamentoResponse> GetRecetaMedicamento(SqlConnection con, long id);
        Task<List<RecetaMedicamentoResponse>> GetAllRecetaMedicamento(SqlConnection con, long id);
        Task<ResponseSQL> InsertMedicamentoFrecuencia(SqlConnection con, MedicamentoHorarioIRequest entidad, long idUsuario);
        Task<ResponseSQL> UpdateMedicamentoFrecuencia(SqlConnection con, MedicamentoHorarioURequest entity, long idUsuario);
        Task<ResponseSQL> DeleteMedicamentoFrecuencia(SqlConnection con, long idMedicamentoFrecuencia, long idUsuario);
        Task<MedicamentoHorario> GetMedicamentoFrecuencia(SqlConnection con, long id);
        Task<List<MedicamentoHorario>> GetAllMedicamentoFrecuencia(SqlConnection con, long id);
        Task<List<TratamientoResponse>> TratamientoActual(SqlConnection con, long idPersona);
        Task<List<TratamientoResponse>> TratamientoHistorico(SqlConnection con, long idPersona, long idMedico, long idEspecialidad, long idEstado);
        Task<List<TratamientoPorFechaResponse>> TratamientoPorFecha(SqlConnection con, string strFecha, long idUsuario, long idConsulta);
        Task<TratamientoSeguimientoResponse> TratamientoSeguimiento(SqlConnection con, long idConsulta);
        Task<List<TratamientoSeguimientoResponse>> TratamientoSeguimientoMedico(SqlConnection con, long idPersona);
        Task<List<Tratamiento>> TratamientoEnCursoByPaciente(SqlConnection con, long idPersona);
        Task<RecetaResumenResponse> RecetaResumen(SqlConnection con, long idConsulta);
        Task<TratamientoResumenResponse> TratamientoResumen(SqlConnection con, long idConsulta);
    }
}

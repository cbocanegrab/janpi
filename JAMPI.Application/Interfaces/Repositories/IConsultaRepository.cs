﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IConsultaRepository
    {
        Task<List<Consulta>> GetAll(SqlConnection con);
        Task<Consulta> Get(SqlConnection con, long id);
        Task<ResponseSQL> AddAsync(SqlConnection con, ConsultaBasicRequest entity, Session session);
        Task<ResponseSQL> UpdateTriaje(SqlConnection con, ConsultaTriajeRequest entity);
        Task<ResponseSQL> Programar(SqlConnection con, ConsultaProgramaRequest entity);
        Task<ResponseSQL> CancelarPorPaciente(SqlConnection con, ConsultaCancelaRequest entity,long idUsuario);
        Task<ResponseSQL> CancelarPorMedico(SqlConnection con, ConsultaCancelaRequest entity);
        Task<ResponseSQL> ActualizarURL(SqlConnection con, ConsultaURLRequest entity);
        Task<ResponseSQL> Delete(SqlConnection con, long id);
        Task<ConsultaResumenResponse> ProximaConsulta(SqlConnection con,long idPersonaMedico, long idPersona);
        Task<ConsultaResumenResponse> UltimaConsulta(SqlConnection con, long idPersona);
        Task<ResponseSQL> ProcesarPago(SqlConnection con, Pago obj, long idUsuario);
        Task<Consulta> LinkConsulta(SqlConnection con, long idConsulta);
        Task<ResponseSQL> ValorarConsulta(SqlConnection con, ConsultaValoracionRequest entity, long idUsuario);
        Task<List<OrdenResponse>> Ordenes(SqlConnection con, long idPersona);
        Task<ConsultaDetalleResponse> ConsultaDetalle(SqlConnection con, long idConsulta);
        Task<ResponseSQL> SeguimientoInsert(SqlConnection con, SeguimientoRequest entity, long idUsuario);
        Task<List<ConsultaSeguimiento>> SeguiminetoListar(SqlConnection con, long idConsulta);
        Task<List<Consulta>> ConsultaProgramada(SqlConnection con, long idPersonaMedico, long idPersona);
        Task<List<Consulta>> ConsultaHistorico(SqlConnection con, long idPersonaMedico, long idPersona, long idEspecialidad, long idEstadoTratamiento);
        Task<ResponseSQL> ReconsultaInsertar(SqlConnection con, ReconsultaRequest entity, long idUsuario);
        Task<ResponseSQL> ConsultaDerivadaInsertar(SqlConnection con, ConsultaDerivadaRequest entity, long idUsuario);
        Task<ConsultaDerivadaResponse> ConsultaDerivadaObtener(SqlConnection con, long idPersona);
        Task<ResponseSQL> ConsultaDerivadaEliminar(SqlConnection con, long idConsultaDerivada, long idUsuario);
        Task<ConsultaSeguimientoResponse> ConsultaSeguimiento(SqlConnection con, long idConsulta);
        Task<ConsultaProgramacionResponse> ConsultaProgramacion(SqlConnection con, long idMedico, long idPersona);
        Task<List<ConsultaProgMin>> ConsultaPorFecha(SqlConnection con, string strFecha, long idMedico, long idPersona);
    }
}

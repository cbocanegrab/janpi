﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IMedicoRepository : IGenericRepository<Medico>
    {
        Task<List<MedicoDisponibleResponse>> GetAllByEspecialidad(SqlConnection con, long idEspecialidad);
        Task<List<MedicoMinResponse>> ListarMedicosbyUsuario(SqlConnection con, long idPersona);
        Task<List<MedicoMinResponse>> ListarMedicoEmergencia(SqlConnection con, long idEspecialidad, long idUsuario);
        Task<List<MedicoDisponibleHorarioResponse>> ListarMedicoDisponible(SqlConnection con, long idEspecialidad, string strFecha);
        Task<List<Horario>> ListarMedicoDisponibleHorario(SqlConnection con, long idMedicoEspecialidad, string strFecha);
        Task<MedicoCVResponse> GetCv(SqlConnection con, long id);
        Task<List<Medico>> MedicoBusqueda(SqlConnection con, string strDescipcion, long idEspecialidad);
        Task<Medico> MedicoValoracion(SqlConnection con, long idPersona);
        Task<List<Paciente>> PacienteByMedico(SqlConnection con, long idPersona); 
        Task<ResponseSQL> MedicoPersonaGuardar(SqlConnection con, MedicoPersonaRequest entity, long idPersona, long idUsuario);
        Task<ResponseSQL> MedicoInsertar(SqlConnection con, MedicoIRequest entity, long idUsuario, string strClave);
        Task<ResponseSQL> MedicoActualizar(SqlConnection con, MedicoURequest entity, long idUsuario);
    }
}

﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IEmpresaEmpleadoRepository
    {
        Task<EmpresaEmpleadoResponse> GetEmpleado(SqlConnection con, long id);
        Task<List<EmpresaEmpleadoResponse>> GetAllEmpleado(SqlConnection con, long id);
        Task<ResponseSQL> EmpresaEmpleadoInsertar(SqlConnection con, EmpresaEmpleadoIRequest entity, string strClave, long idUsuario);
        Task<ResponseSQL> EmpresaEmpleadoActualizar(SqlConnection con, EmpresaEmpleadoURequest entity, long idUsuario);
        Task<ResponseSQL> EsmpresaEmpleadoEliminar(SqlConnection con, long idEmpresaEmpleado, long idUsuario);
    }
}

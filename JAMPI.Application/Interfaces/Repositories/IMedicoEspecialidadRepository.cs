﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IMedicoEspecialidadRepository
    {
        Task<ResponseSQL> MedicoEspecialidadInsertar(SqlConnection con, MedicoEspecialidadIRequest entity, long idUsuario);
        Task<ResponseSQL> MedicoEspecialidadActualizar(SqlConnection con, MedicoEspecialidadURequest entity, long idUsuario);
        Task<ResponseSQL> MedicoEspecialidadEliminar(SqlConnection con, long idMedicoEspecialidad, long idUsuario);
        Task<List<MedicoEspecialidadResponse>> MedicoEspecialidadListar(SqlConnection con, long idMedico, long idMedicoEspecialidad);
        Task<MedicoEspecialidad> MedicoEspecialidadDetalle(SqlConnection con, long idMedicoEspecialidad);
        Task<List<ComentarioResponse>> GetAllComentarios(SqlConnection con, long idMedicoEspecialidad);
    }
}

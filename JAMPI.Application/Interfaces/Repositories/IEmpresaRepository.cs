﻿using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IEmpresaRepository: IGenericRepository<Empresa>
    {
        Task<EmpresaResumenResponse> ResumenSemanal(SqlConnection con, long idUsuario);
        Task<EmpresaResumenResponse> ResumenMensual(SqlConnection con, long idUsuario);
        Task<List<ConsultaEmpleadoHistorialResponse>> HistorialConsultas(SqlConnection con, long idUsuario);
    }
}

﻿using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IMaestroRepository
    {
        Task<List<TipoEmergenciaSelectorResponse>> ListarTipoEmergencia(SqlConnection con);
        Task<List<ConveniosResponse>> ListarConvenio(SqlConnection con);
        Task<List<MedicamentoResponse>> ListarMedicamento(SqlConnection con);
        Task<List<Examen>> ListarExamen(SqlConnection con, long idExamen);
        Task<List<TipoEmergenciaTipResponse>> TipoEmergenciaTipListar(SqlConnection con, long idTipoEmergencia);
    }
}

﻿using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IMedicoHorarioRepository : IGenericRepository<MedicoHorario>
    {
        Task<List<MedicoHorarioResponse>> GetAllMedicoHorario(SqlConnection con, long idMedicoEspecialidad);
        Task<MedicoHorarioResponse> GetMedicoHorario(SqlConnection con, long id);
    }
}

﻿using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IEspecialidadTipRepository
    {
        Task<ResponseSQL> Add(SqlConnection con, EspecialidadTip entity);
        Task<ResponseSQL> Delete(SqlConnection con, EspecialidadTip entity);
        Task<EspecialidadTip> Get(SqlConnection con, long id);
        Task<List<EspecialidadTip>> GetAll(SqlConnection con);
        Task<List<EspecialidadTip>> GetAllByEspecialidad(SqlConnection con, long id);
        Task<ResponseSQL> Update(SqlConnection con, EspecialidadTip entity);
    }
}

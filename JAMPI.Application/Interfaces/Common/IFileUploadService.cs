﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Common
{
    public interface IFileUploadService
    {
        Task<Response<string>> InitFile();
        Task<Response<string>> ProccessFile(FileRequest fileRequest);
        bool MoverArchivo(dynamic obj);
        Task<Response<string>> SaveFile(FileUploadRequest fileUploadRequest);
        Task<string> GetFotoByMedico(long id);
        Task<string> GetFotoByPersona(long id);
        Task<string> GetFotoByUsuario(long id);
        Task<string> GetFirmaMedico(long id);
    }
}

﻿using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Common
{
    public interface IUtilService
    {
        string GenerarToken();
        string EncryptTripleDES(String sIn, string key);
        string DecryptTripleDES(string sOut, string key);
        Task<Response<bool>> EnviarCorreo(string strDestinatario, string strAsunto, string strCuerpo, bool isHTML);
    }
}

﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Services
{
    public interface IUsuarioService
    {
        Task<Response<string>> AccederAsync(LoginRequest loginRequest);
        Task<Response<long>> ActivarAsync(LoginRequest oLogin);
        Task<Response<long>> CambiarClaveAsync(PasswordRequest oPassword);
        Task<Response<int>> RecuperarClaveAsync(string strUsuario);
        Task<Response<int>> ValidarTokenAsync(string strUsuario, string strToken);
        Task<Response<string>> FacebookLoginAsync(string strAccessToken);
        Task<Response<bool>> RestableceClaveAsync(RestableceClaveRequest obj);
    }
}

﻿using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Services
{
    public interface IParametroService
    {
        Task<Response<List<Parametro>>> ListarParametros(long idTipoParametro);
    }
}

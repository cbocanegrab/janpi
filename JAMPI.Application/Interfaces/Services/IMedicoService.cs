﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Services
{
    public interface IMedicoService
    {
        Task<Response<List<Medico>>> Listar();
        Task<Response<List<MedicoDisponibleResponse>>> ListarPorEspecialidad(long idEspecialidad);
        Task<Response<MedicoResponse>> Obtener(long id);
        Task<Response<int>> Agregar(Medico medico);
        Task<Response<int>> Actualizar(Medico medico);
        Task<Response<int>> Borrar(long id);
        Task<Response<List<MedicoMinResponse>>> ListarMedicosbyUsuario();
        Task<Response<List<MedicoMinResponse>>> ListarMedicoEmergencia(long idEspecialidad);
        Task<Response<List<MedicoDisponibleHorarioResponse>>> ListarMedicoDisponible(long idEspecialidad, string strFecha);
        Task<Response<MedicoCVResponse>> GetMedicoCV(long id);
        Task<Response<List<MedicoMinResponse>>> MedicoBusqueda(string strDescripcion, long idEspecialidad);
        Task<Response<MedicoDetalleResponse>> MedicoDetalle(long idMedicoEspecialidad);
        Task<Response<MedicoValoracionResponse>> MedicoValoracion();
        Task<Response<List<MedicoContactoResponse>>> MedicoContacto(long idEspecialidad);
        Task<Response<long>> MedicoEspecialidadInsertar(MedicoEspecialidadIRequest oMedicoEspecialidad);
        Task<Response<bool>> MedicoEspecialidadActualizar(MedicoEspecialidadURequest oMedicoEspecialidad);
        Task<Response<bool>> MedicoEspecialidadEliminar(long idMedicoEspecialiadad);
        Task<Response<List<MedicoEspecialidadResponse>>> MedicoEspecialidadListar(long idMedico, long idMedicoEspecialidad);
        Task<Response<long>> MedicoExperienciaInsertar(MedicoExperienciaIRequest oMedicoExperiencia);
        Task<Response<bool>> MedicoExperienciaActualizar(MedicoExperienciaURequest oMedicoExperiencia);
        Task<Response<bool>> MedicoExperienciaEliminar(long idMedicoExperiencia);
        Task<Response<List<MedicoExperiencia>>> MedicoExperienciaListar();
        Task<Response<long>> MedicoEstudioInsertar(MedicoEstudioIRequest oMedicoExperiencia);
        Task<Response<bool>> MedicoEstudioActualizar(MedicoEstudioURequest oMedicoExperiencia);
        Task<Response<bool>> MedicoEstudioEliminar(long idMedicoEstudio);
        Task<Response<List<MedicoEstudio>>> MedicoEstudioListar();
        Task<Response<List<PacienteResponse>>> PacienteListar();
        Task<Response<bool>> MedicoPersonaGuardar(MedicoPersonaRequest obj);
        Task<Response<bool>> MedicoInsertarAsync(MedicoIRequest oMedico);
        Task<Response<bool>> MedicoActualizarAsync(MedicoURequest oMedico);
    }
}

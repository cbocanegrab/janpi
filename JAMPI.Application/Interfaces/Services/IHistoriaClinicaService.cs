﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Services
{
    public interface IHistoriaClinicaService
    {
        Task<Response<long>> HistoriaClinicaInsertar(HistoriaClinicaIRequest oHistoriaClinica);
        Task<Response<bool>> HistoriaClinicaActualizar(HistoriaClinicaURequest oHistoriaClinica);
        Task<Response<List<HistoriaClinica>>> HistoriaClinicaListar(long idPersona);

        Task<Response<long>> HistoriaClinicaDetalleInsertar(HistoriaClinicaDetalleIRequest oHistoriaClinica);
        Task<Response<bool>> HistoriaClinicaDetalleActualizar(HistoriaClinicaDetalleURequest oHistoriaClinica);
        Task<Response<List<HistoriaClinicaDetalle>>> HistoriaClinicaDetalleListar(long idHistoriaClinica);
    }
}

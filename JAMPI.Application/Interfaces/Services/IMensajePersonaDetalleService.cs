﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Services
{
    public interface IMensajePersonaDetalleService
    {
        Task<Response<List<MensajePersonaDetalleResponse>>> Listar(long idMensajePersona);
        Task<Response<long>> Insert(MensajePersonaDetalleRequest mensajePersonaDetalle);
        Task<Response<bool>> UpdateLeido(long ID);
        Task<Response<long>> MensajeMedicoInsert(MensajeMedicoDetalleRequest mensajeMedicoDetalle);
    }
}

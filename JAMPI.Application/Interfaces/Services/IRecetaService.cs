﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Services
{
    public interface IRecetaService
    {
        Task<Response<long>> InsertarRecetaAsync(RecetaIRequest oReceta);
        Task<Response<bool>> ActualizarRecetaAsync(RecetaURequest oReceta);
        Task<Response<bool>> EliminarRecetaAsync(long id);
        Task<Response<RecetaResponse>> ObtenerReceta(long id);
        Task<Response<List<RecetaResponse>>> ListarReceta();
        Task<Response<long>> InsertarRecetaMedicamentoAsync(RecetaMedicamentoIRequest oRecetaMedicamento);
        Task<Response<bool>> ActualizarRecetaMedicamentoAsync(RecetaMedicamentoURequest oRecetaMedicamento);
        Task<Response<bool>> EliminarRecetaMedicamentoAsync(long id);
        Task<Response<RecetaMedicamentoResponse>> ObtenerRecetaMedicamento(long id);
        Task<Response<List<RecetaMedicamentoResponse>>> ListarRecetaMedicamento(long idReceta);
        Task<Response<long>> InsertarMedicamentoHorarioAsync(List<MedicamentoHorarioIRequest> lMedicamentoHorario);
        Task<Response<bool>> ActualizarMedicamentoHorarioAsync(List<MedicamentoHorarioURequest> lMedicamentoHorario);
        Task<Response<List<TratamientoResponse>>> TratamientosVigentes();
        Task<Response<List<TratamientoResponse>>> TratamientosHistoricos(long idMedico, long idEspecialidad, long idEstado);
        Task<Response<List<TratamientoPorFechaResponse>>> TratamientosPorFecha(string strFecha);
        Task<Response<List<TratamientoEnCursoResponse>>> TratamientosEnCurso();
        Task<Response<TratamientoSeguimientoResponse>> TratamientosSeguimiento(long idConsulta);
        Task<Response<List<TratamientoSeguimientoResponse>>> TratamientosSeguimientoMedico();
        Task<Response<List<TratamientoEnCursoPacienteResponse>>> TratamientosEnCursoByPaciente(long idPersonaPaciente);
        Task<Response<RecetaResumenResponse>> RecetaResumen(long idConsulta);
        Task<Response<TratamientoResumenResponse>> TratamientoResumen(long idConsulta);
    }
}

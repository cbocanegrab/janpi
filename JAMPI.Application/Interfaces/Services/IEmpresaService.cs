﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Services
{
    public interface IEmpresaService
    {
        Task<Response<long>> InsertarAsync(EmpresaRequest empresa);
        Task<Response<bool>> ActualizarAsync(EmpresaRequest empresa);
        Task<Response<bool>> EliminarAsync(long id);
        Task<Response<EmpresaResponse>> Obtener(long id);
        Task<Response<List<EmpresaResponse>>> Listar();
        Task<Response<EmpresaResumenResponse>> ResumenSemanal();
        Task<Response<EmpresaResumenResponse>> ResumenMensual();
        Task<Response<List<ConsultaEmpleadoHistorialResponse>>> HistorialConsultas();
    }
}

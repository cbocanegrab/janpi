﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Services
{
    public interface IMedidaService
    {
        Task<Response<List<MedidasResponse>>> ListarMedidas(long idParamTipoMedida);
        Task<Response<bool>> InsertMedidas(MedidaRequest medidaRequest);
        Task<Response<bool>> EliminarMedidas(MedidaRequest medidaRequest);
    }
}

﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Services
{
    public interface INotificacionService
    {
        Task<Response<List<NotificacionResponse>>> Listar();
        Task<Response<bool>> MarcarLeido(long id);
    }
}

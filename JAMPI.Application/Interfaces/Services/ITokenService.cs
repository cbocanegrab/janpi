﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Services
{
    public interface ITokenService
    {
        Task<Response<CredentialResponse>> GetSessionToken();
        string GenerateJwtLogin(long idUsuario, long idPersona);
        Task<Response<CredentialResponse>> Refresh(RefreshTokenRequest refreshTokenRequest);
    }
}

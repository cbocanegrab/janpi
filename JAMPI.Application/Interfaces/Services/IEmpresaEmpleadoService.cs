﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Services
{
    public interface IEmpresaEmpleadoService
    {
        Task<Response<long>> InsertarAsync(EmpresaEmpleadoIRequest EmpresaEmpleado);
        Task<Response<bool>> ActualizarAsync(EmpresaEmpleadoURequest EmpresaEmpleado);
        Task<Response<bool>> EliminarAsync(long id);
        Task<Response<EmpresaEmpleadoResponse>> Obtener(long id);
        Task<Response<List<EmpresaEmpleadoResponse>>> Listar(); 
    }
}

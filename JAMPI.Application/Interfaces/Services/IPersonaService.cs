﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Services
{
  public  interface IPersonaService
    {
        Task<Response<long>> InsertarAsync(PersonaIRequest persona);
        Task<Response<bool>> ActualizarAsync(PersonaURequest persona);
        Task<Response<Persona>> GetInfoAsync();
        Task<Response<List<PersonaDependienteResponse>>> ListarDependientes();
        Task<Response<decimal>> CreditoAsync();
        Task<Response<bool>> InsertarContacto(string strDescripcion);
        Task<Response<PersonaMinResponse>> PersonaMin(long idPersona);
        Task<Response<long>> PersonaDepInsertar(PersonaDepRequest persona);
        Task<Response<bool>> VinculoActualizar(long idPersona, string strEstado);
        Task<Response<PersonaResponse>> PersonaObtener(long idPersona);
    }
}

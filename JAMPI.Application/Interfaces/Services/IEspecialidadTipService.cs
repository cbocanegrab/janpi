﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Services
{
    public interface IEspecialidadTipService
    {
        Task<Response<List<EspecialidadTipResponse>>> Listar();
        Task<Response<List<EspecialidadTipResponse>>> ListarPorEspecialidad(long id);
        Task<Response<EspecialidadTipResponse>> Obtener(long id);
        Task<Response<long>> Agregar(EspecialidadTipRequest especialidad);
        Task<Response<bool>> Actualizar(EspecialidadTipRequest especialidad);
        Task<Response<bool>> Borrar(long id);
    }
}

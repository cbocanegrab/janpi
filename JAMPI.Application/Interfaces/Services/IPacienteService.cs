﻿using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Services
{
    public interface IPacienteService
    {
        Task<Response<List<PersonaDependienteResponse>>>ListarDependientes();
    }
}

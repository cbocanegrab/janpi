﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Services
{
    public interface IMedicoHorarioService
    {
        Task<Response<bool>> InsertarAsync(MedicoHorarioIRequest MedicoHorario);
        Task<Response<bool>> ActualizarAsync(MedicoHorarioURequest MedicoHorario);
        Task<Response<bool>> EliminarAsync(long id);
        Task<Response<MedicoHorarioResponse>> Obtener(long id);
        Task<Response<List<MedicoHorarioResponse>>> Listar(long id); 
    }
}

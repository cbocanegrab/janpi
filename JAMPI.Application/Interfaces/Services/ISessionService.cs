﻿using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Interfaces.Services
{
    public interface ISessionService
    {
        Session GetSession();
    }
}

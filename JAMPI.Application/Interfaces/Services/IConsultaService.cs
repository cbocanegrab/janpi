﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Services
{
    public interface IConsultaService
    {
        Task<Response<List<Consulta>>> Listar();
        Task<Response<Consulta>> Obtener(long id);
        Task<Response<long>> Agregar(ConsultaBasicRequest consulta);
        Task<Response<int>> Programar(ConsultaProgramaRequest programaRequest);
        Task<Response<bool>> ActualizarTriaje(ConsultaTriajeRequest consultaTriaje);
        Task<Response<bool>> CancelarPorPaciente(ConsultaCancelaRequest cancelaRequest);
        Task<Response<bool>> CancelarPorMedico(ConsultaCancelaRequest cancelaRequest);
        Task<Response<bool>> ActualizarURL(ConsultaURLRequest urlRequest);
        Task<Response<bool>> Borrar(long id);
        Task<Response<ConsultaResumenResponse>> ProximaConsulta();
        Task<Response<ConsultaResumenResponse>> UltimaConsulta();
        Task<Response<bool>> ProcesarPago(ConsultaPagoRequest pagoRequest);
        Task<Response<string>> LinkConsulta(long idConsulta);
        Task<Response<bool>> ValorarConsulta(ConsultaValoracionRequest obj);
        Task<Response<List<OrdenResponse>>> Ordenes();
        Task<Response<ConsultaDetalleResponse>> ConsultaDetalle(long idConsulta);
        Task<Response<bool>> InsertarSeguimientoAsync(SeguimientoRequest oSeguimiento);
        Task<Response<ConsultaResumenResponse>> ProximaConsultaMedico();
        Task<Response<List<ConsultaResumenResponse>>> ConsultaProgramadaMedico();
        Task<Response<List<ConsultaResumenResponse>>> ConsultaHistoricoMedico(long idEspecialidad, long idEstadoTratamiento);
        Task<Response<bool>> ConsultaExamenGuardarAsync(List<ConsultaExamenGRequest> lConsultaExamen);
        Task<Response<List<ConsultaExamenResponse>>> ConsultaExamenListar(long idConsulta);
        Task<Response<bool>> ReconsultaInsertarAsync(ReconsultaRequest oConsulta);
        Task<Response<List<ConsultaResumenResponse>>> ConsultaHistoricoPaciente(long idPersonaPaciente);
        Task<Response<bool>> ConsultaDerivadaInsertarAsync(ConsultaDerivadaRequest oConsulta);
        Task<Response<bool>> ConsultaDerivadaEliminarAsync(long idConsultaDerivada);
        Task<Response<ConsultaDerivadaResponse>> ConsultaDerivadaObtener();
        Task<Response<ConsultaSeguimientoResponse>> ConsultaSeguimiento(long idConsulta);
        Task<Response<List<ConsultaResumenResponse>>> ConsultaHistoricoMedicoAll();
        Task<Response<ExamenResumenResponse>> ConsultaExamenResumen(long idConsulta);
        Task<Response<ConsultaProgramacionResponse>> ConsultaProgramacion();
        Task<Response<List<ConsultaProgMin>>> ConsultaPorFecha(string strFecha);
    }
}

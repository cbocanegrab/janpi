﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Services
{
    public interface IEspecialidadService
    {
        Task<Response<List<EspecialidadSelectorResponse>>> Listar();
        Task<Response<Especialidad>> Obtener(long id);
        Task<Response<long>> Agregar(EspecialidadRequest especialidad);
        Task<Response<bool>> Actualizar(EspecialidadRequest especialidad);
        Task<Response<bool>> Borrar(long id);
    }
}

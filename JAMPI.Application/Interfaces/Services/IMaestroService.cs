﻿using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Services
{
    public interface IMaestroService
    {
        Task<Response<List<TipoEmergenciaSelectorResponse>>> ListarTipoEmergencia();
        Task<Response<List<ConveniosResponse>>> ListarConvenio();
        Task<Response<List<MedicamentoResponse>>> ListarMedicamento();
        Task<Response<List<ExamenResponse>>> ListarExamen();
        Task<Response<List<TipoEmergenciaTipResponse>>> TipoEmergenciaTipListar(long idTipoEmergencia);

    }
}

﻿using AutoMapper;
using FluentValidation;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.ExternalServices.Interfaces;
using JAMPI.Application.ExternalServices.Models;
using JAMPI.Application.ExternalServices.Services;
using JAMPI.Application.Interfaces.Common;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Application.Structure.CommonStructure.Services;
using JAMPI.Application.Structure.ConsultaStructure.Services;
using JAMPI.Application.Structure.EmpresaEmpleadoEmpleadoStructure.Services;
using JAMPI.Application.Structure.EmpresaStructure.Services;
using JAMPI.Application.Structure.EspecialidadStructure.Services;
using JAMPI.Application.Structure.HistoriaClinicaStructure.Services;
using JAMPI.Application.Structure.MedicoHorarioEmpleadoStructure.Services;
using JAMPI.Application.Structure.MedicoStructure.Services;
using JAMPI.Application.Structure.MedidasStructure.Services;
using JAMPI.Application.Structure.NotificacionStructure.Services;
using JAMPI.Application.Structure.PacienteStructure.Services;
using JAMPI.Application.Structure.PersonaMedidaStructure.Mapper;
using JAMPI.Application.Structure.PersonaStructure.Services;
using JAMPI.Application.Structure.PersonaStructure.Validators;
using JAMPI.Application.Structure.RecetaStructure.Services;
using JAMPI.Application.Structure.UsuarioStructure.Services;
using JAMPI.Application.Structure.UsuarioStructure.Validators;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace JAMPI.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services,
            IConfiguration configuration)
        {

            services.AddAutoMapper(typeof(PersonaMedidaMapper));

            var facebookAuthSettings = new FacebookAuthSetting();
            configuration.Bind(nameof(FacebookAuthSetting), facebookAuthSettings);
            services.AddSingleton(facebookAuthSettings);

            services.AddScoped<IGoogle, Google>();
            services.AddScoped<IUsuarioService, UsuarioService>();
            services.AddScoped<IPersonaService, PersonaService>();
            services.AddScoped<IUtilService, UtilService>();
            services.AddScoped<IMedicoService, MedicoService>();
            services.AddScoped<IConsultaService, ConsultaService>();
            services.AddScoped<IEspecialidadService, EspecialidadService>();
            services.AddScoped<IEmpresaService, EmpresaService>();
            services.AddScoped<IEmpresaEmpleadoService, EmpresaEmpleadoService>();
            services.AddScoped<IFacebook, Facebook>();
            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<ISessionService, SessionService>();
            services.AddScoped<IParametroService, ParametroService>();
            services.AddScoped<IPacienteService, PacienteService>();
            services.AddScoped<IMaestroService, MaestroService>();
            services.AddScoped<IMedidaService, PersonaMedidaService>();
            services.AddScoped<IConfigMedidaService, PersonaConfigMedidaService>();
            services.AddScoped<INotificacionService, NotificacionService>();
            services.AddScoped<IEspecialidadTipService, EspecialidadTipService>();
            services.AddScoped<IMensajePersonaDetalleService, MensajePersonaDetalleService>();
            services.AddScoped<IMensajePersonaService, MensajePersonaService>();
            services.AddScoped<IMedicoHorarioService, MedicoHorarioService>();
            services.AddScoped<IRecetaService, RecetaService>();
            services.AddScoped<IHistoriaClinicaService, HistoriaClinicaService>();
            services.AddScoped<IFileUploadService, FileUploadService>();

            services.AddTransient<IValidator<PasswordRequest>, PasswordRequestValidator>();
            services.AddTransient<IValidator<PersonaIRequest>, PersonaIRequestValidator>();
            services.AddTransient<IValidator<PersonaURequest>, PersonaURequestValidator>();
            services.AddTransient<IValidator<RestableceClaveRequest>, RestableceClaveRequestValidator>();

            return services;
        }
    }
}

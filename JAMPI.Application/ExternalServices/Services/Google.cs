﻿using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using JAMPI.Application.ExternalServices.Interfaces;

namespace JAMPI.Application.ExternalServices.Services
{
    public class Google : IGoogle
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly IConfiguration _configuration;

        public Google(IHttpClientFactory clientFactory, IConfiguration configuration)
        {
            _clientFactory = clientFactory;
            _configuration = configuration;
        }

        public async Task<Response<TokenResponse>> Verificar(string token)
        {
            Response<TokenResponse> oResponse = new Response<TokenResponse>();
            TokenResponse tokenResponse = new TokenResponse();
            using (var client = _clientFactory.CreateClient())
            {
                string key = _configuration.GetSection("CaptchaV3").GetSection("Key").Value;
                string url = _configuration.GetSection("CaptchaV3").GetSection("URL").Value;

                var response = await client.GetStringAsync($"{url}?secret={key}&response={token}");
                tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(response);
            }
            // Recaptcha V3 Verify Api send score 0-1. If score is low such as less than 0.5, you can think that it is a bot and return false.     
            // If token is not success then return false
            oResponse.data = tokenResponse;
            if (!tokenResponse.Success || tokenResponse.Score < (decimal)0.5)
            {
                tokenResponse.Success = false;
                oResponse.success = false;
                oResponse.message = Constants._CAPTCHA_VALIDATION_ERROR;
                oResponse.developer = string.Format(Constants._CAPTCHA_VALIDATION_ERROR_DEV, tokenResponse.ErrorCodes);
            }
            else
            {
                oResponse.success = true;
            }
            oResponse.success = true;
            return oResponse;
        }
    }
}

﻿using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.ExternalServices.Interfaces;
using JAMPI.Application.ExternalServices.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.ExternalServices.Services
{
    public class Facebook : IFacebook
    {
        public const string TokenValidationUrl = "https://graph.facebook.com/debug_token?input_token={0}&access_token={1}|{2}";
        public const string UserInfoUrl = "https://graph.facebook.com/me?fields=id,name,last_name,birthday,picture,email&access_token={0}";
        private readonly FacebookAuthSetting _facebookAuthSetting;
        private readonly IHttpClientFactory _httpClientFactory;

        public Facebook(FacebookAuthSetting facebookAuthSetting, IHttpClientFactory httpClientFactory)
        {
            _facebookAuthSetting = facebookAuthSetting;
            _httpClientFactory = httpClientFactory;
        }
        
        public async Task<FacebookUserInfoResult> GetUserInfoAsync(string accessToken)
        {
            var url = string.Format(UserInfoUrl, accessToken);
            var result = await _httpClientFactory.CreateClient().GetAsync(url);
            result.EnsureSuccessStatusCode();
            var response = await result.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<FacebookUserInfoResult>(response);
        }

        public async Task<FacebookTokenValidationResult> ValidateAccessTokenTokenAsync(string accessToken)
        {
            var url = string.Format(TokenValidationUrl, accessToken, _facebookAuthSetting.AppId, _facebookAuthSetting.AppSecret);
            var result = await _httpClientFactory.CreateClient().GetAsync(url);
            result.EnsureSuccessStatusCode();
            var response = await result.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<FacebookTokenValidationResult>(response);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.ExternalServices.Models
{
    public class FacebookAuthSetting
    {
        public string AppId { get; set; }
        public string AppSecret { get; set; }
    }
}

﻿using JAMPI.Application.Contracts.Responses;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.ExternalServices.Interfaces
{
    public interface IFacebook
    {
        Task<FacebookTokenValidationResult> ValidateAccessTokenTokenAsync(string accessToken);
        Task<FacebookUserInfoResult> GetUserInfoAsync(string accessToken);
    }
}

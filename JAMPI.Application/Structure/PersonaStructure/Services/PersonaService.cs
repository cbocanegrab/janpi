using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Common;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using System.Threading.Tasks;

namespace JAMPI.Application.Structure.PersonaStructure.Services
{
    public class PersonaService : IPersonaService
    {
        private readonly IUnitOfWork _unitOfWork;
        private string _connectionString;
        private readonly IUtilService _util;
        private readonly ISessionService _sessionService;
        private IConfiguration _configuration;
        public PersonaService(IConfiguration configuration, IUnitOfWork unitOfWork, IUtilService util, ISessionService sessionService)
        {
            _unitOfWork = unitOfWork;
            _connectionString = configuration.GetConnectionString("ConnJAMPI");
            _util = util;
            _sessionService = sessionService;
            _configuration = configuration;
        }
        public async Task<Response<long>> InsertarAsync(PersonaIRequest persona)
        {
            Response<long> oResponse = new Response<long>();
            string strkey = _configuration.GetSection("KeyEncrypt").Value;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();
                    persona.strClave1 = _util.EncryptTripleDES(persona.strClave1, strkey);
                    Persona oPersona = new Persona()
                    {
                        //idParamTipoDocumento = persona.idParamTipoDocumento,
                        //strNumeroDocumento = persona.strNumeroDocumento,
                        strNombre = persona.strNombre,
                        strPrimerApellido = persona.strPrimerApellido.Split(' ')[0],
                        strSegundoApellido = persona.strPrimerApellido.Split(' ').Length > 1 ? persona.strPrimerApellido.Split(' ')[1] : " ",
                        strCelularPrincipal = persona.strCelularPrincipal,
                        strCorreoPrincipal = persona.strCorreoPrincipal,
                        usuario = new Usuario()
                        {
                            strClave = persona.strClave1,
                            strUserId = persona.strUserId,
                            idParamTipoRed = persona.idParamTipoRed
                        },
                        idPersonaSup = persona.idPersonaSup
                    };

                    oResponseSQL = await _unitOfWork.personaRepository.Add(con, oPersona);
                    oResponse.success = false;
                    oResponse.data = oResponseSQL.resultado;
                    if (oResponseSQL.resultado == 0)
                    {
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                    }
                    else if (oResponseSQL.resultado == 1)
                    {
                        if (persona.idPersonaSup == 0)
                        {
                            var msjEmail = $"Estimado(a): " + persona.strNombre + "<br><br>";
                            msjEmail += $"Le enviamos la contraseña de su cuenta JAMPI: ";
                            msjEmail += $"{persona.strClave2}<br><br>";
                            msjEmail += $"Atte.<br>";
                            msjEmail += $"JAMPI";
                            Response<bool> responseMail = await _util.EnviarCorreo(persona.strCorreoPrincipal, "Cuenta JAMPI", msjEmail, true);
                        }
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_INSERT_OK;

                    }
                    else if (oResponseSQL.resultado == 2)
                    {
                        oResponse.message = Constants._PERSONA_EMAILEXISTE;
                    }
                    else if (oResponseSQL.resultado == 3)
                    {
                        oResponse.message = Constants._PERSONA_DNIEXISTE;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                        oResponse.data = 0;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }

        public async Task<Response<bool>> ActualizarAsync(PersonaURequest persona)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();

                    ResponseSQL oResponseSQL = new ResponseSQL();
                    oResponseSQL = await _unitOfWork.personaRepository.PersonaActualizar(con, persona,session.idUsuario);
                    oResponse.success = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.data = true;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_UPDATE_OK;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_UPDATE_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                        oResponse.data = false;
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = ex.Message;
                }
            }
            return oResponse;
        }

        public async Task<Response<Persona>> GetInfoAsync()
        {
            Response<Persona> oResponse = new Response<Persona>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    Persona persona = await _unitOfWork.personaRepository.Get(con, session.idUsuario);
                    if (persona != null)
                    {
                        oResponse.success = true;
                        oResponse.data = persona;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = Constants._GENERAL_GET_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_GET_ERROR);
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                }
            }
            return oResponse;
        }

        public async Task<Response<List<PersonaDependienteResponse>>> ListarDependientes()
        {
            Response<List<PersonaDependienteResponse>> oResponse = new Response<List<PersonaDependienteResponse>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var response = await _unitOfWork.personaRepository.ListarDependientes(con, session.idPersona);
                    if (response == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = response;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<decimal>> CreditoAsync()
        {
            Response<decimal> oResponse = new Response<decimal>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    Persona persona = await _unitOfWork.personaRepository.Credito(con, session.idPersona);
                    if (persona != null)
                    {
                        oResponse.success = true;
                        oResponse.data = persona.decCredito;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = Constants._GENERAL_GET_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_GET_ERROR);
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                }
            }
            return oResponse;
        }

        public async Task<Response<bool>> InsertarContacto(string strDescripcion)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();
                    oResponseSQL = await _unitOfWork.personaRepository.InsertarContacto(con, strDescripcion, session.idPersona, session.idUsuario);
                    oResponse.success = false;
                    oResponse.data = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.data = true;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_INSERT_OK;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                        oResponse.developer = oResponseSQL.mensaje;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<PersonaMinResponse>> PersonaMin(long idPersona)
        {
            Response<PersonaMinResponse> oResponse = new Response<PersonaMinResponse>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var persona = await _unitOfWork.personaRepository.PersonaMin(con, idPersona);
                    if (persona != null)
                    {
                        oResponse.success = true;
                        oResponse.data = persona;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = Constants._GENERAL_GET_ERROR;
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = ex.Message;
                }
            }
            return oResponse;
        }
        public async Task<Response<long>> PersonaDepInsertar(PersonaDepRequest persona)
        {
            Response<long> oResponse = new Response<long>();
            string strkey = _configuration.GetSection("KeyEncrypt").Value;
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();
                    string strClave = _util.EncryptTripleDES(persona.strNumeroDocumento, strkey);
                    if(persona.idPersona==0)
                    oResponseSQL = await _unitOfWork.personaRepository.PersonaDepInsertar(con, persona, strClave,session.idUsuario);
                    else oResponseSQL = await _unitOfWork.personaRepository.PersonaDepActualizar(con, persona, strClave, session.idUsuario);

                    oResponse.success = false;
                    oResponse.data = oResponseSQL.resultado;
                    if (oResponseSQL.resultado == 0)
                    {
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                    }
                    else if (oResponseSQL.resultado == 1)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_INSERT_OK;

                    }
                    else if (oResponseSQL.resultado == 2)
                    {
                        oResponse.message = Constants._PERSONA_DNIEXISTE;
                    }
                    else if (oResponseSQL.resultado == 3)
                    {
                        oResponse.message = "SE SUPERÓ EL NÚMERO MÁXIMO DE DEPENDIENTES.";
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                        oResponse.developer = oResponseSQL.mensaje;
                        oResponse.data = 0;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = ex.Message;
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> VinculoActualizar(long idPersona, string strEstado)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();

                    oResponseSQL = await _unitOfWork.personaRepository.VinculoActualizar(con, idPersona, strEstado, session.idUsuario);
                    oResponse.success = false;

                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.data = true;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_UPDATE_OK;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_UPDATE_ERROR;
                        oResponse.developer = oResponseSQL.mensaje;
                        oResponse.data = false;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = ex.Message;
                }
            }
            return oResponse;
        }
        public async Task<Response<PersonaResponse>> PersonaObtener(long idPersona)
        {
            Response<PersonaResponse> oResponse = new Response<PersonaResponse>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var persona = await _unitOfWork.personaRepository.PersonaObtener(con, idPersona);
                    if (persona != null)
                    {
                        oResponse.success = true;
                        oResponse.data = persona;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = Constants._GENERAL_GET_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_GET_ERROR);
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                }
            }
            return oResponse;
        }
    }
}

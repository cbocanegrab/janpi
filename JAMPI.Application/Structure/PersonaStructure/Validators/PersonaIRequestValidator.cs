﻿using FluentValidation;
using JAMPI.Application.Contracts.Requests;
using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Structure.PersonaStructure.Validators
{
    public class PersonaIRequestValidator: AbstractValidator<PersonaIRequest>
    {
        public PersonaIRequestValidator()
        {
            //RuleFor(x => x.idParamTipoDocumento)
            //    .NotEmpty();
            //RuleFor(x => x.strNumeroDocumento)
            //    .NotEmpty();
            RuleFor(x => x.strNombre)
                .NotEmpty();
            RuleFor(x => x.strPrimerApellido)
                .NotEmpty();
            RuleFor(x => x.strCelularPrincipal)
                .NotEmpty();
            RuleFor(x => x.strCorreoPrincipal)
                .NotEmpty()
                .EmailAddress();
            RuleFor(x => x.strClave1)
                .NotEmpty()
                .Matches("^(([A-Z])(?=.*[a-zA-Z0-9])).{3,16}$").WithMessage("La clave no cumple las condiciones de seguridad")
                .Must((model, field) => field == model.strClave2).WithMessage("Las claves ingresadas no son iguales");
        }
    }
}

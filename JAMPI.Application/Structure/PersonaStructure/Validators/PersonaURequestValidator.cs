﻿using FluentValidation;
using JAMPI.Application.Contracts.Requests;
using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Structure.PersonaStructure.Validators
{
    public class PersonaURequestValidator: AbstractValidator<PersonaURequest>
    {
        public PersonaURequestValidator()
        {
            RuleFor(x => x.strSexo)
                .NotEmpty();
            RuleFor(x => x.strFechaNacimiento)
                .NotEmpty();
            RuleFor(x => x.strDireccion)
                .NotEmpty();
            RuleFor(x => x.strCelularPrincipal)
                .NotEmpty();
            RuleFor(x => x.idParamGradoInstr)
                .NotEmpty();
            RuleFor(x => x.strOcupacion)
                .NotEmpty();
        }
    }
}

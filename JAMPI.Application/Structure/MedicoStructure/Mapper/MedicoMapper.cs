﻿using AutoMapper;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Structure.MedicoStructure.Mapper
{
    public class MedicoMapper:Profile
    {
        public MedicoMapper()
        {
            CreateMap<MedicoEstudio, MedicoEstudioResponse>().ReverseMap();
            CreateMap<MedicoExperiencia, MedicoExperienciaResponse>().ReverseMap();
            CreateMap<MedicoEspecialidad, MedicoDetalleResponse>().ReverseMap();
            CreateMap<Medico, MedicoValoracionResponse>().ReverseMap();
            CreateMap<Medico, MedicoMinResponse>().ReverseMap();
            CreateMap<Medico, MedicoContactoResponse>().ReverseMap();
            CreateMap<Medico, MedicoResponse>().ReverseMap();
        }
    }

}

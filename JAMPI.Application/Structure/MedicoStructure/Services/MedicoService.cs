﻿using AutoMapper;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Common;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Structure.MedicoStructure.Services
{
    public class MedicoService :IMedicoService
    {
        private readonly IUnitOfWork _unitOfWork;
        private string _connectionString;
        private readonly IUtilService _util;
        private readonly ISessionService _sessionService;
        private readonly IMapper _mapper;
        private IConfiguration _configuration;
        public MedicoService(IConfiguration configuration, IUnitOfWork unitOfWork, IUtilService util, ISessionService sessionService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _connectionString = configuration.GetConnectionString("ConnJAMPI");
            _util = util;
            _sessionService = sessionService;
            _mapper = mapper;
            _configuration = configuration;
        }
        public async Task<Response<List<Medico>>> Listar()
        {
            Response<List<Medico>> oResponse = new Response<List<Medico>>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    oResponse.data = await _unitOfWork.medicoRepository.GetAll(con);
                    oResponse.success = true;
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<List<MedicoDisponibleResponse>>> ListarPorEspecialidad(long idEspecialidad)
        {
            Response<List<MedicoDisponibleResponse>> oResponse = new Response<List<MedicoDisponibleResponse>>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    oResponse.data = await _unitOfWork.medicoRepository.GetAllByEspecialidad(con, idEspecialidad);
                    oResponse.success = true;
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<MedicoResponse>> Obtener(long id)
        {
            Response<MedicoResponse> oResponse = new Response<MedicoResponse>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();

                    var data = await _unitOfWork.medicoRepository.Get(con, id);
                    oResponse.data = _mapper.Map<MedicoResponse>(data);
                    oResponse.success = true;
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = ex.Message;
                }
            }
            return oResponse;
        }
        public async Task<Response<int>> Agregar(Medico medico)
        {
            Response<int> oResponse = new Response<int>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL responseSQL = await _unitOfWork.medicoRepository.Add(con, medico);
                    if (responseSQL.resultado > 0)
                        oResponse.success = true;
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = "";
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<int>> Actualizar(Medico medico)
        {
            Response<int> oResponse = new Response<int>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL responseSQL = await _unitOfWork.medicoRepository.Update(con, medico);
                    if (responseSQL.resultado > 0)
                        oResponse.success = true;
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = "";
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<int>> Borrar(long id)
        {
            Response<int> oResponse = new Response<int>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    Medico entidad = new Medico()
                    {
                        idMedico=id,
                    };
                    ResponseSQL responseSQL = await _unitOfWork.medicoRepository.Delete(con, entidad);
                    if (responseSQL.resultado > 0) oResponse.success = true;
                    else {
                        oResponse.success = false;
                        oResponse.message = "";
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<List<MedicoMinResponse>>> ListarMedicosbyUsuario()
        {
            Response<List<MedicoMinResponse>> oResponse = new Response<List<MedicoMinResponse>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var response = await _unitOfWork.medicoRepository.ListarMedicosbyUsuario(con, session.idPersona);
                    if (response == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = response;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<List<MedicoMinResponse>>> ListarMedicoEmergencia(long idEspecialidad)
        {
            Response<List<MedicoMinResponse>> oResponse = new Response<List<MedicoMinResponse>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var response = await _unitOfWork.medicoRepository.ListarMedicoEmergencia(con, idEspecialidad, session.idUsuario);
                    if (response == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = response;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<List<MedicoDisponibleHorarioResponse>>> ListarMedicoDisponible(long idEspecialidad,string strFecha)
        {
            Response<List<MedicoDisponibleHorarioResponse>> oResponse = new Response<List<MedicoDisponibleHorarioResponse>>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.medicoRepository.ListarMedicoDisponible(con, idEspecialidad, strFecha);
                    if (lst == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        foreach (MedicoDisponibleHorarioResponse obj in lst)
                        {
                            var lstHorario = await _unitOfWork.medicoRepository.ListarMedicoDisponibleHorario(con, obj.idMedicoEspecialidad, strFecha);
                            obj.lHorario = lstHorario;
                        }
                        oResponse.data = lst;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<MedicoCVResponse>> GetMedicoCV(long id)
        {
            var response = new Response<MedicoCVResponse>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.medicoRepository.GetCv(con, id);
                    if (lst != null)response.ObtenerResponseSuccess(lst);
                    else response.ObtenerResponseError(Constants._GENERAL_ERROR_NULL, Constants._GENERAL_ERROR_DEV_NULL);
                }
                catch (Exception ex)
                {
                    response.ObtenerResponseCatchError(String.Format(Constants._GENERAL_ERROR_DEV, ex.Message));
                }
            }
            return response;
        }
        public async Task<Response<List<MedicoMinResponse>>> MedicoBusqueda(string strDescripcion, long idEspecialidad)
        {
            Response<List<MedicoMinResponse>> oResponse = new Response<List<MedicoMinResponse>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var response = await _unitOfWork.medicoRepository.MedicoBusqueda(con, strDescripcion, idEspecialidad);
                    if (response == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = _mapper.Map<List<MedicoMinResponse>>(response); ;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<MedicoDetalleResponse>> MedicoDetalle(long idMedicoEspecialidad)
        {
            var oResponse = new Response<MedicoDetalleResponse>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var entidad = await _unitOfWork.medicoEspecialidadRepository.MedicoEspecialidadDetalle(con, idMedicoEspecialidad);
                    if (entidad != null) {
                        var oMedicoDetalle = _mapper.Map<MedicoDetalleResponse>(entidad);
                        var lEstudios = await _unitOfWork.medicoEstudioRepository.GetAllMedicoEstudio(con,oMedicoDetalle.idMedico,0);
                        if (lEstudios != null)
                        {
                            oMedicoDetalle.lEstudios = new List<MedicoEstudioResponse>();
                            oMedicoDetalle.lEstudios = _mapper.Map<List<MedicoEstudioResponse>>(lEstudios);
                        }
                        var lExperiencia = await _unitOfWork.medicoExperienciaRepository.GetAllMedicoExperiencia(con, oMedicoDetalle.idMedico,0);
                        if (lExperiencia != null)
                        {
                            oMedicoDetalle.lExperiencia = new List<MedicoExperienciaResponse>();
                            oMedicoDetalle.lExperiencia = _mapper.Map<List<MedicoExperienciaResponse>>(lExperiencia);
                        }
                        var lComentarios = await _unitOfWork.medicoEspecialidadRepository.GetAllComentarios(con, idMedicoEspecialidad);
                        oMedicoDetalle.lComentario = new List<ComentarioResponse>();
                        oMedicoDetalle.lComentario = lComentarios;
                        oResponse.ObtenerResponseSuccess(oMedicoDetalle);
                    
                        //oMedicoDetalle.lEstudios =
                    }
                    else oResponse.ObtenerResponseError(Constants._GENERAL_ERROR_NULL, Constants._GENERAL_ERROR_DEV_NULL);
                }
                catch (Exception ex)
                {
                    oResponse.ObtenerResponseCatchError(String.Format(Constants._GENERAL_ERROR_DEV, ex.Message));
                }
            }
            return oResponse;
        }
        public async Task<Response<MedicoValoracionResponse>> MedicoValoracion()
        {
            Response<MedicoValoracionResponse> oResponse = new Response<MedicoValoracionResponse>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var medico = await _unitOfWork.medicoRepository.MedicoValoracion(con, session.idPersona);
                    if (medico == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        var  response = _mapper.Map<MedicoValoracionResponse>(medico);
                        oResponse.data = response;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<List<MedicoContactoResponse>>> MedicoContacto(long idEspecialidad)
        {
            Response<List<MedicoContactoResponse>> oResponse = new Response<List<MedicoContactoResponse>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var response = await _unitOfWork.medicoRepository.MedicoBusqueda(con, "", idEspecialidad);
                    if (response == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {

                        oResponse.data = _mapper.Map<List<MedicoContactoResponse>>(response); ;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<long>> MedicoEspecialidadInsertar(MedicoEspecialidadIRequest oMedicoEspecialidad)
        {
            Response<long> oResponse = new Response<long>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();

                    oResponseSQL = await _unitOfWork.medicoEspecialidadRepository.MedicoEspecialidadInsertar(con, oMedicoEspecialidad, session.idUsuario);
                    oResponse.success = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_INSERT_OK;
                        oResponse.data = oResponseSQL.resultado;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                        oResponse.data = 0;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> MedicoEspecialidadActualizar(MedicoEspecialidadURequest oMedicoEspecialidad)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();

                    ResponseSQL oResponseSQL = new ResponseSQL();
                    oResponseSQL = await _unitOfWork.medicoEspecialidadRepository.MedicoEspecialidadActualizar(con, oMedicoEspecialidad, session.idUsuario);
                    oResponse.success = false;
                    oResponse.data = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.data = true;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_UPDATE_OK;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_UPDATE_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> MedicoEspecialidadEliminar(long idMedicoEspecialiadad)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();
                    oResponseSQL = await _unitOfWork.medicoEspecialidadRepository.MedicoEspecialidadEliminar(con, idMedicoEspecialiadad, session.idUsuario);
                    oResponse.success = false;
                    oResponse.data = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.data = true;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_DELETE_OK;
                    }
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = Constants._GENERAL_UPDATE_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<List<MedicoEspecialidadResponse>>> MedicoEspecialidadListar(long idMedico, long idMedicoEspecialidad)
        {
            var oResponse = new Response<List<MedicoEspecialidadResponse>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.medicoEspecialidadRepository.MedicoEspecialidadListar(con, idMedico, idMedicoEspecialidad);
                    oResponse.success = false;
                    if (lst == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = lst;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<long>> MedicoExperienciaInsertar(MedicoExperienciaIRequest oMedicoExperiencia)
        {
            Response<long> oResponse = new Response<long>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();

                    oResponseSQL = await _unitOfWork.medicoExperienciaRepository.MedicoExperienciaInsertar(con, oMedicoExperiencia, session.idUsuario);
                    oResponse.success = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_INSERT_OK;
                        oResponse.data = oResponseSQL.resultado;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                        oResponse.data = 0;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = ex.Message;
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> MedicoExperienciaActualizar(MedicoExperienciaURequest oMedicoExperiencia)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();

                    ResponseSQL oResponseSQL = new ResponseSQL();
                    oResponseSQL = await _unitOfWork.medicoExperienciaRepository.MedicoExperienciaActualizar(con, oMedicoExperiencia, session.idUsuario);
                    oResponse.success = false;
                    oResponse.data = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.data = true;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_UPDATE_OK;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_UPDATE_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = ex.Message;
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> MedicoExperienciaEliminar(long idMedicoExperiencia)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();
                    oResponseSQL = await _unitOfWork.medicoExperienciaRepository.MedicoExperienciaEliminar(con, idMedicoExperiencia, session.idUsuario);
                    oResponse.success = false;
                    oResponse.data = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.data = true;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_DELETE_OK;
                    }
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = Constants._GENERAL_UPDATE_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<List<MedicoExperiencia>>> MedicoExperienciaListar()
        {
            Response<List<MedicoExperiencia>> oResponse = new Response<List<MedicoExperiencia>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.medicoExperienciaRepository.GetAllMedicoExperiencia(con, 0,session.idPersona);

                    oResponse.success = false;
                    if (lst == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = lst;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = ex.Message;
                }
            }
            return oResponse;
        }
        public async Task<Response<long>> MedicoEstudioInsertar(MedicoEstudioIRequest oMedicoEstudio)
        {
            Response<long> oResponse = new Response<long>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();

                    oResponseSQL = await _unitOfWork.medicoEstudioRepository.MedicoEstudioInsertar(con, oMedicoEstudio, session.idUsuario);
                    oResponse.success = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_INSERT_OK;
                        oResponse.data = oResponseSQL.resultado;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                        oResponse.data = 0;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> MedicoEstudioActualizar(MedicoEstudioURequest oMedicoEstudio)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();

                    ResponseSQL oResponseSQL = new ResponseSQL();
                    oResponseSQL = await _unitOfWork.medicoEstudioRepository.MedicoEstudioActualizar(con, oMedicoEstudio, session.idUsuario);
                    oResponse.success = false;
                    oResponse.data = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.data = true;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_UPDATE_OK;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_UPDATE_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> MedicoEstudioEliminar(long idMedicoEstudio)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();
                    oResponseSQL = await _unitOfWork.medicoEstudioRepository.MedicoEstudioEliminar(con, idMedicoEstudio, session.idUsuario);
                    oResponse.success = false;
                    oResponse.data = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.data = true;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_DELETE_OK;
                    }
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = Constants._GENERAL_UPDATE_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<List<MedicoEstudio>>> MedicoEstudioListar()
        {
            Response<List<MedicoEstudio>> oResponse = new Response<List<MedicoEstudio>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.medicoEstudioRepository.GetAllMedicoEstudio(con, 0, session.idPersona);

                    oResponse.success = false;
                    if (lst == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = lst;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<List<PacienteResponse>>> PacienteListar()
        {
            Response<List<PacienteResponse>> oResponse = new Response<List<PacienteResponse>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.medicoRepository.PacienteByMedico(con, session.idPersona);

                    oResponse.success = false;
                    if (lst == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = _mapper.Map<List<PacienteResponse>>(lst);
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> MedicoPersonaGuardar(MedicoPersonaRequest obj)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();

                    oResponseSQL = await _unitOfWork.medicoRepository.MedicoPersonaGuardar(con, obj,session.idPersona, session.idUsuario);
                    oResponse.success = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_INSERT_OK;
                        oResponse.data = true;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                        oResponse.data = false;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> MedicoInsertarAsync(MedicoIRequest oMedico)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            string strkey = _configuration.GetSection("KeyEncrypt").Value;
            string strClave =  _util.EncryptTripleDES(oMedico.strNumeroDocumento, strkey); ;
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();

                    oResponseSQL = await _unitOfWork.medicoRepository.MedicoInsertar(con, oMedico, session.idUsuario, strClave);
                    oResponse.success = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_INSERT_OK;
                        oResponse.data = true;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                        oResponse.data = false;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = ex.Message;
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> MedicoActualizarAsync(MedicoURequest oMedico)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();

                    oResponseSQL = await _unitOfWork.medicoRepository.MedicoActualizar(con, oMedico, session.idUsuario);
                    oResponse.success = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_INSERT_OK;
                        oResponse.data = true;
                    }
                    else
                    {
                        oResponse.message = oResponseSQL.resultado == 2 ? oResponse.message : Constants._GENERAL_INSERT_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                        oResponse.data = oResponseSQL.resultado == 2 ? true : false;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = ex.Message;
                }
            }
            return oResponse;
        }
    }
}

﻿using AutoMapper;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace JAMPI.Application.Structure.MedidasStructure.Services
{
    public class PersonaConfigMedidaService : IConfigMedidaService
    {
        private readonly IConfiguration _configuration;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ISessionService _sessionService;
        private readonly Session _session;
        private readonly IMapper _mapper;
        private string _connectionString;
        public PersonaConfigMedidaService(IConfiguration configuration, IUnitOfWork unitOfWork, ISessionService sessionService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _sessionService = sessionService;
            _session = _sessionService.GetSession();
            _mapper = mapper;
            _configuration = configuration;
            _connectionString = configuration.GetConnectionString("ConnJAMPI");
        }
        public async Task<Response<List<TipoMedidasResponse>>> ListarTiposMedidas()
        {
            Response<List<TipoMedidasResponse>> oResponse = new Response<List<TipoMedidasResponse>>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                Session session = _sessionService.GetSession();
                try
                {
                    await con.OpenAsync();
                    var response = await _unitOfWork.personaConfigMedidaRepository.GetAllByPersona(con, _session);
                    oResponse.data = _mapper.Map<List<TipoMedidasResponse>>(response);
                    oResponse.success = true;
                    oResponse.message = Constants.PersonaMedidas._LIST_SUCCESS;

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<long>> InsertTipoMedidas(TipoMedidaRequest medidaRequest)
        {
            Response<long> oResponse = new Response<long>();
            SqlTransaction trx = null;
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    trx = con.BeginTransaction();
                    var persona = _mapper.Map<ConfigMedida>(medidaRequest);
                    persona.idPersona = _session.idPersona;
                    persona.idUsuarioRegistro = _session.idUsuario;
                    long i = await _unitOfWork.personaConfigMedidaRepository.Add(con, persona, trx);

                    if (i > 0)
                    {
                        oResponse.success = true;
                        oResponse.data = i;
                    }
                    else
                    {
                        oResponse.developer = Constants._GENERAL_ERROR;
                        oResponse.success = false;
                        oResponse.message = Constants._GENERAL_ERROR;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
                finally
                {
                    if (trx != null)
                    {
                        if (oResponse.success) trx.Commit();
                        else trx.Rollback();
                    }
                }
            }
            return oResponse;
        }

        public async Task<Response<bool>> EliminarTipoMedidas(long id)
        {
            Response<bool> oResponse = new Response<bool>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    Session session = _sessionService.GetSession();
                    int i = await _unitOfWork.personaConfigMedidaRepository.Delete(con, id, session);
                    if (i > 0)
                        oResponse.success = true;
                    else
                    {
                        oResponse.developer = Constants._GENERAL_ERROR;
                        oResponse.success = false;
                        oResponse.message = Constants._GENERAL_ERROR;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }



    }
}

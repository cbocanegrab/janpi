﻿using AutoMapper;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Structure.PersonaMedidaStructure.Mapper
{
    public class PersonaConfigMapper : Profile
    {
        public PersonaConfigMapper()
        {
            CreateMap<ConfigMedida, TipoMedidasResponse>()
                .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.idPersonaConfigMedida)).ReverseMap();
            CreateMap<TipoMedidaRequest, ConfigMedida>()
                .ForMember(dest => dest.idPersonaConfigMedida, opt => opt.MapFrom(src => src.id)).ReverseMap();
        }
    }
}

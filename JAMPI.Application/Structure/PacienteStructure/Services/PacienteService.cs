﻿using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Common;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Structure.PacienteStructure.Services
{
    public class PacienteService:IPacienteService
    {
        private readonly IUnitOfWork _unitOfWork;
        private string _connectionString;
        private readonly IUtilService _util;
        public PacienteService(IConfiguration configuration, IUnitOfWork unitOfWork, IUtilService util)
        {
            _unitOfWork = unitOfWork;
            _connectionString = configuration.GetConnectionString("ConnJAMPI");
            _util = util;
        }
        public async Task<Response<List<PersonaDependienteResponse>>> ListarDependientes()
        {
            Response<List<PersonaDependienteResponse>> oResponse = new Response<List<PersonaDependienteResponse>>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    //var response = await _unitOfWork.consultaRepository.ProximaConsulta(con, id);
                    List<PersonaDependienteResponse> lst = new List<PersonaDependienteResponse>();
                    for (var i = 0; i < 5; ++i)
                    {
                        lst.Add(null);
                    }

                    oResponse.data = lst;
                    oResponse.success = true;

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
    }
}

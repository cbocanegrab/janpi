﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Common;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Structure.MedicoHorarioEmpleadoStructure.Services
{
    public class MedicoHorarioService : IMedicoHorarioService
    {
        private readonly IUnitOfWork _unitOfWork;
        private string _connectionString;
        private IConfiguration _configuration;
        private readonly IUtilService _util;
        private readonly ISessionService _sessionService;
        public MedicoHorarioService(IConfiguration configuration, IUnitOfWork unitOfWork, IUtilService util, ISessionService sessionService)
        {
            _unitOfWork = unitOfWork;
            _connectionString = configuration.GetConnectionString("ConnJAMPI");
            _configuration = configuration;
            _util = util;
            _sessionService = sessionService;
        }
        public async Task<Response<bool>> InsertarAsync(MedicoHorarioIRequest oMedicoHorario)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();
                    DateTime feIni = DateTime.ParseExact(oMedicoHorario.strFechaInicio, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime feFin = DateTime.ParseExact(oMedicoHorario.strFechaFin, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime fecha = new DateTime();
                    fecha = feIni;
                    while (fecha<=feFin)
                    {
                        string date = fecha.ToString("dd/MM/yyyy");
                        MedicoHorario entidad = new MedicoHorario()
                        {
                            idMedico = oMedicoHorario.idMedico,
                            idEspecialidad = oMedicoHorario.idEspecialidad,
                            strFechaInicio = date,
                            strHoraInicio = oMedicoHorario.strHoraInicio,
                            strHoraFin = oMedicoHorario.strHoraFin,
                            idUsuario = session.idUsuario,
                        };

                        oResponseSQL = await _unitOfWork.medicoHorarioRepository.Add(con, entidad);
                        if (oResponseSQL.mensaje == "")
                        {
                           fecha = fecha.AddDays(1);
                        }
                        else
                        {
                            break;
                        }
                    }
                    
                    
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_INSERT_OK;
                    }
                    else
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer =  ex.Message;
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> ActualizarAsync(MedicoHorarioURequest oMedicoHorario)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    MedicoHorario entidad = new MedicoHorario()
                    {
                        idMedicoHorario = oMedicoHorario.idMedicoHorario,
                        strHoraInicio = oMedicoHorario.strHoraInicio,
                        strHoraFin =oMedicoHorario.strHoraFin,
                        idUsuario = session.idUsuario
                    };

                    ResponseSQL oResponseSQL = new ResponseSQL();
                    oResponseSQL = await _unitOfWork.medicoHorarioRepository.Update(con, entidad);
                    oResponse.success = false;
                    if (oResponseSQL.mensaje == "") { 
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_UPDATE_OK;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_UPDATE_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }

        public async Task<Response<bool>> EliminarAsync(long id)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    MedicoHorario entidad = new MedicoHorario()
                    {
                        idMedicoHorario = id,
                        idUsuario = session.idUsuario
                    };

                    ResponseSQL oResponseSQL = new ResponseSQL();
                    oResponseSQL = await _unitOfWork.medicoHorarioRepository.Delete(con, entidad);
                    oResponse.success = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_DELETE_OK;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_UPDATE_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<MedicoHorarioResponse>> Obtener(long id)
        {
            Response<MedicoHorarioResponse> oResponse = new Response<MedicoHorarioResponse>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    oResponse.data = await _unitOfWork.medicoHorarioRepository.GetMedicoHorario(con, id);
                    oResponse.success = true;
                    if (oResponse.data == null)
                    {
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<List<MedicoHorarioResponse>>> Listar(long id)
        {
            Response<List<MedicoHorarioResponse>> oResponse = new Response<List<MedicoHorarioResponse>>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    oResponse.data = await _unitOfWork.medicoHorarioRepository.GetAllMedicoHorario(con, id);
                    oResponse.success = true;
                    if (oResponse.data == null)
                    {
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
    }
}

﻿using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Common;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Structure.CommonStructure.Services
{
    public class MaestroService: IMaestroService
    {
        private readonly IUnitOfWork _unitOfWork;
        private string _connectionString;
        private readonly IUtilService _util;
        public MaestroService(IConfiguration configuration, IUnitOfWork unitOfWork, IUtilService util)
        {
            _unitOfWork = unitOfWork;
            _connectionString = configuration.GetConnectionString("ConnJAMPI");
            _util = util;
        }
        public async Task<Response<List<TipoEmergenciaSelectorResponse>>> ListarTipoEmergencia()
        {
            Response<List<TipoEmergenciaSelectorResponse>> oResponse = new Response<List<TipoEmergenciaSelectorResponse>>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var response = await _unitOfWork.maestroRepository.ListarTipoEmergencia(con);
                    if (response == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = response;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<List<ConveniosResponse>>> ListarConvenio()
        {
            Response<List<ConveniosResponse>> oResponse = new Response<List<ConveniosResponse>>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var response = await _unitOfWork.maestroRepository.ListarConvenio(con);
                    if (response == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = response;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<List<MedicamentoResponse>>> ListarMedicamento()
        {
            Response<List<MedicamentoResponse>> oResponse = new Response<List<MedicamentoResponse>>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var response = await _unitOfWork.maestroRepository.ListarMedicamento(con);
                    if (response == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = response;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<List<ExamenResponse>>> ListarExamen()
        {
            Response<List<ExamenResponse>> oResponse = new Response<List<ExamenResponse>>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lstCab = await _unitOfWork.maestroRepository.ListarExamen(con,0);
                    if (lstCab == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        List<ExamenResponse> lst = new List<ExamenResponse>();
                        foreach(Examen item in lstCab)
                        {
                            if (item.idExamenPadre == 0)
                            {
                                ExamenResponse obj = new ExamenResponse();
                                obj.idExamen = item.idExamen;
                                obj.strExamen = item.strExamen;
                                obj.lExamenDet = new List<ExamenDetResponse>();
                                foreach (Examen item1 in lstCab)
                                {
                                    if (item1.idExamenPadre == item.idExamen)
                                    {
                                        ExamenDetResponse o = new ExamenDetResponse();
                                        o.idExamen = item1.idExamen;
                                        o.strExamen = item1.strExamen;
                                        obj.lExamenDet.Add(o);
                                    }
                                }
                                lst.Add(obj);
                            }
                        }
                        oResponse.data = lst;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<List<TipoEmergenciaTipResponse>>> TipoEmergenciaTipListar(long idTipoEmergencia)
        {
            Response<List<TipoEmergenciaTipResponse>> oResponse = new Response<List<TipoEmergenciaTipResponse>>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.maestroRepository.TipoEmergenciaTipListar(con, idTipoEmergencia);
                    if (lst == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        
                        oResponse.data = lst;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = ex.Message;
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
    }
}

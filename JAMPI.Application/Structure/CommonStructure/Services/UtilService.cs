﻿using JAMPI.Application.Interfaces.Common;
using JAMPI.Domain.Commons;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Structure.CommonStructure.Services
{
    public class UtilService:IUtilService
    {
        private readonly IConfiguration _configuration;
        public UtilService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public string GenerarToken()
        {
            string token = string.Empty;
            var random = new Random();
            for (int i = 0; i < 6; i++)
                token += random.Next(1, 9).ToString();
            return token;
        }
        public string EncryptTripleDES(String sIn, string key)
        {
            TripleDESCryptoServiceProvider DES = new TripleDESCryptoServiceProvider();
            MD5CryptoServiceProvider hashMD5 = new MD5CryptoServiceProvider();
            DES.Key = hashMD5.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(key));
            DES.Mode = CipherMode.ECB;
            ICryptoTransform DESEncrypt = DES.CreateEncryptor();
            byte[] buffer = System.Text.ASCIIEncoding.ASCII.GetBytes(sIn);
            return Convert.ToBase64String(DESEncrypt.TransformFinalBlock(buffer, 0, buffer.Length));
        }
        public string DecryptTripleDES(string sOut, string key)
        {
            TripleDESCryptoServiceProvider DES = new TripleDESCryptoServiceProvider();
            MD5CryptoServiceProvider hashMD5 = new MD5CryptoServiceProvider();
            DES.Key = hashMD5.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(key));
            DES.Mode = CipherMode.ECB;
            ICryptoTransform DESDecrypt = DES.CreateDecryptor();
            Byte[] Buffer = Convert.FromBase64String(sOut);
            return System.Text.ASCIIEncoding.ASCII.GetString(DESDecrypt.TransformFinalBlock(Buffer, 0, Buffer.Length));
        }

        public async Task<Response<bool>> EnviarCorreo(string strDestinatario, string strAsunto, string strCuerpo, bool isHTML)
        {
            string strEmisor = _configuration.GetSection("Mensajeria").GetSection("EMAIL").GetSection("Emisor").Value;
            string strClave = _configuration.GetSection("Mensajeria").GetSection("EMAIL").GetSection("Password").Value;
            string strHost = _configuration.GetSection("Mensajeria").GetSection("EMAIL").GetSection("Host").Value;
            string strPuerto = _configuration.GetSection("Mensajeria").GetSection("EMAIL").GetSection("Puerto").Value;
            Response<bool> response = new Response<bool>();
            try
            {
                using (SmtpClient client = new SmtpClient(strHost))
                {
                    client.Port = Convert.ToInt32(strPuerto);
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                    client.EnableSsl = true;
                    client.Credentials = new NetworkCredential(strEmisor, strClave);
                    using (MailMessage message = new MailMessage(strEmisor.Trim(), strDestinatario.Trim()))
                    {

                        message.Subject = strAsunto;
                        message.Body = strCuerpo;
                        message.IsBodyHtml = isHTML;

                        await client.SendMailAsync(message); 
                        response.message = Constants._GENERAL_CORREO_SEND_OK;
                        response.success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                response.success = false;
                response.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
            }
            return response;
        }
    }

}

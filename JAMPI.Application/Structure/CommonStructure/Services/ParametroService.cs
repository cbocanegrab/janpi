﻿using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Structure.CommonStructure.Services
{
    public class ParametroService : IParametroService
    {
        private readonly IConfiguration _configuration;
        private readonly IUnitOfWork _unitOfWork;
        private readonly string _connectionString;

        public ParametroService(IConfiguration configuration, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _connectionString = configuration.GetConnectionString("ConnJAMPI");
        }

        public async Task<Response<List<Parametro>>> ListarParametros(long idTipoParametro)
        {
            Response<List<Parametro>> oResponse = new Response<List<Parametro>>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    oResponse.data = await _unitOfWork.parametroRepository.List(con, idTipoParametro);
                    oResponse.success = true;
                    oResponse.message = Constants.Login.Recovery._SUCCESS_GENERATE_TOKEN;
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
    }
}

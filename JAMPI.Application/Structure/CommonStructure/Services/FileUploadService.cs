﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Interfaces.Common;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Structure.CommonStructure.Services
{
    public class FileUploadService : IFileUploadService
    {
        private readonly IWebHostEnvironment _hostingEnvironment;
        private string _connectionString;
        private readonly IConfiguration _configuration;
        private readonly IArchivoRepository _archivoRepository;
        private readonly Session _session;

        public FileUploadService(IWebHostEnvironment hostingEnvironment, IConfiguration configuration, IArchivoRepository archivoRepository, ISessionService sessionService)
        {
            _connectionString = configuration.GetConnectionString("ConnJAMPI");
            _hostingEnvironment = hostingEnvironment;
            _configuration = configuration;
            _archivoRepository = archivoRepository;
            _session = sessionService.GetSession();
        }
        public async Task<Response<string>> InitFile()
        {
            Response<string> response = new Response<string>();
            try
            {
                string token = Guid.NewGuid().ToString().Replace("-", "");//Split("-")[0];
                string path = Path.Combine(_hostingEnvironment.ContentRootPath,
                    _configuration["Directories:staticFilesRoot"],
                    string.Format(_configuration["Directories:TempFolderBlock"], token));
                await Task.Run(() =>
                {
                    if (!Directory.Exists(path)) Directory.CreateDirectory(path);
                });
                response.success = true;
                response.data = token;
            }
            catch (Exception ex)
            {
                response.message = Constants._FILE_ERROR_CREATE_FOLDER;
                response.developer = string.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
            }
            return response;
        }
        public async Task<Response<string>> ProccessFile(FileRequest fileRequest)
        {
            Response<string> response = new Response<string>();
            string path = Path.Combine(_hostingEnvironment.ContentRootPath,
                _configuration["Directories:staticFilesRoot"],
                string.Format(_configuration["Directories:TempFolderBlock"], fileRequest.strToken));
            try
            {
                if (Directory.Exists(path))
                {
                    if (fileRequest.strSection.Split(',').Length > 1)
                    {
                        string pathFinal = Path.Combine(_hostingEnvironment.ContentRootPath,
                            _configuration["Directories:staticFilesRoot"],
                            string.Format(_configuration["Directories:TempFinalFile"], fileRequest.strToken, fileRequest.strFileName));
                        var enc1252 = CodePagesEncodingProvider.Instance.GetEncoding(1252);
                        string contentFile = "";
                        byte[] fileBytes;

                        using (StreamWriter sw = new StreamWriter(pathFinal, true, enc1252))
                        {
                            fileBytes = Convert.FromBase64String(fileRequest.strSection.Split(',')[1]);
                            contentFile = enc1252.GetString(fileBytes);
                            await sw.WriteAsync(contentFile);
                        }
                        response.success = true;
                        if (fileRequest.intFin == 1) response.data = fileRequest.strToken;
                    }
                    else
                    {
                        response.message = Constants._FILE_ERROR_ADD_TEMP_FILE;
                        response.developer = Constants._FILE_ERROR_FILE_NOT_BASE64;
                    }
                }
                else
                {
                    response.success = false;
                    response.message = Constants._FILE_ERROR_ADD_TEMP_FILE;
                    response.developer = Constants._FILE_ERROR_DIRECTORY_NOT_FOUND;
                }
            }
            catch (Exception ex)
            {
                response.message = Constants._FILE_ERROR_ADD_TEMP_FILE;
                response.developer = string.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
            }
            return response;
        }

        public async Task<Response<string>> SaveFile(FileUploadRequest fileUploadRequest)
        {
            var response = new Response<string>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    long id = await _archivoRepository.NewID(con);
                    Archivo archivo = new Archivo()
                    {
                        idArchivo = id,
                        strToken = fileUploadRequest.strToken,
                        strNombre = fileUploadRequest.strNombre
                    };
                    archivo = await ObtenerArchivo(archivo);
                    await _archivoRepository.Add(con, archivo, _session);
                    int i = await _archivoRepository.UpdateArchivoTabla(con, fileUploadRequest.id, archivo.idArchivo, fileUploadRequest.intTipo, _session);
                    if (i > 0) {
                        MoverArchivo(archivo);
                        response.ObtenerResponseSuccess(archivo.strRuta);
                    }
                    
                }
                catch (Exception ex)
                {
                    response.ObtenerResponseCatchError(ex);
                }
            }

            return response;
        }


        private async Task<dynamic> ObtenerArchivo(dynamic obj)
        {
            string rutaTemp = Path.Combine(_hostingEnvironment.ContentRootPath,
                _configuration["Directories:staticFilesRoot"],
                string.Format(_configuration["Directories:TempFinalFile"], obj.strToken, obj.strNombre));

            FileInfo oInfo = new FileInfo(rutaTemp);
            obj.decPeso = oInfo.Length;
            obj.strExtension = oInfo.Extension;
            obj.strHash = await GetHash(rutaTemp);
            obj.strTipoMime = ObtenerTipoArchivo(rutaTemp);
            obj.strRuta = Path.Combine(string.Format(_configuration["Directories:ArchivosPath"], obj.idArchivo, obj.strExtension));
            return obj;
        }
        public bool MoverArchivo(dynamic obj)
        {
            string rutaTemp = Path.Combine(_hostingEnvironment.ContentRootPath,
                _configuration["Directories:staticFilesRoot"],
                string.Format(_configuration["Directories:TempFinalFile"], obj.strToken, obj.strNombre));

            string rutaFileFinal = Path.Combine(_hostingEnvironment.ContentRootPath,
                 _configuration["Directories:staticFilesRoot"], obj.strRuta);
            bool exito = MoverArchivo(rutaTemp, rutaFileFinal);

            return exito;
        }
        private async Task<string> GetHash(string strRuta)
        {
            var sBuilder = new StringBuilder();
            byte[] data = await File.ReadAllBytesAsync(strRuta);

            using (HashAlgorithm hashAlgorithm = SHA256.Create())
            {
                byte[] hash = hashAlgorithm.ComputeHash(data);
                for (int i = 0; i < hash.Length; i++)
                {
                    sBuilder.Append(hash[i].ToString("x2"));
                }
            }
            return sBuilder.ToString();
        }
        private string ObtenerTipoArchivo(string archivo)
        {
            var provider = new FileExtensionContentTypeProvider();
            string contentType;
            if (!provider.TryGetContentType(archivo, out contentType))
            {
                contentType = "application/octet-stream";
            }
            return contentType;
        }
        private bool CrearDirectorio(string ruta)
        {
            try
            {
                ruta = Path.Combine(_hostingEnvironment.ContentRootPath, _configuration["Directories:staticFilesRoot"], ruta);
                if (!Directory.Exists(ruta))
                    Directory.CreateDirectory(ruta);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        private bool MoverArchivo(string rutaOrigen, string rutaDestino)//, string nuevoNombre = "")
        {
            try
            {
                //rutaOrigen = Path.Combine(_hostingEnvironment.ContentRootPath, rutaOrigen).Replace("\\", "/");
                //string fileFinal = rutaDestino;
                //if (nuevoNombre != "") fileFinal = Path.Combine(_hostingEnvironment.ContentRootPath, rutaDestino, nuevoNombre).Replace("\\", "/");
                if (!Directory.Exists(Path.GetDirectoryName(rutaDestino))) Directory.CreateDirectory(Path.GetDirectoryName(rutaDestino));
                File.Move(rutaOrigen, rutaDestino);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<string> GetFotoByMedico(long id)
        {
            var response = "";
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    string strRuta = await _archivoRepository.GetFotoByMedico(con, id);
                    response = _configuration["Directories:staticFilesRoot"] + (strRuta != null ? strRuta : _configuration["Directories:DefaultPhotoProfile"]);
                }
                catch (Exception ex)
                {
                }
            }

            return response;
        }

        public async Task<string> GetFotoByPersona(long id)
        {
            var response = "";
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    string strRuta = await _archivoRepository.GetFotoByPersona(con, id);
                    response = _configuration["Directories:staticFilesRoot"] + (strRuta != null ? strRuta : _configuration["Directories:DefaultPhotoProfile"]);
                }
                catch (Exception ex)
                {
                }
            }

            return response;
        }

        public async Task<string> GetFotoByUsuario(long id)
        {
            var response = "";
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    string strRuta = await _archivoRepository.GetFotoByUsuario(con, id);
                    response = _configuration["Directories:staticFilesRoot"] + (strRuta != null ? strRuta : _configuration["Directories:DefaultPhotoProfile"]);
                }
                catch (Exception ex)
                {
                }
            }

            return response;
        }
        public async Task<string> GetFirmaMedico(long id)
        {
            var response = "";
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    string strRuta = await _archivoRepository.GetFirmaMedico(con, id);
                    response = _configuration["Directories:staticFilesRoot"] + (strRuta != null ? strRuta : _configuration["Directories:DefaultSignProfile"]);
                }
                catch (Exception ex)
                {
                    // response.ObtenerResponseCatchError(ex);
                }
            }

            return response;
        }
    }
}

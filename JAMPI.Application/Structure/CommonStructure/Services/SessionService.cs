﻿using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace JAMPI.Application.Structure.CommonStructure.Services
{
    public class SessionService :ISessionService
    {
        private readonly IHttpContextAccessor _http;
        public SessionService(IHttpContextAccessor http)
        {
            _http = http;
        }

        public Session GetSession() {
            Session session = new Session();
            session.idUsuario = ClaimValue<long>("UserID");
            session.idPersona = ClaimValue<long>("PersonID");
            session.jwtId = ClaimValue<string>(JwtRegisteredClaimNames.Jti);
            session.strNumeroDocumento = ClaimValue<string>("UserDocument");
            session.strNombre = ClaimValue<string>("FirstName");
            session.strPrimerApellido = ClaimValue<string>("LastName");
            session.strCorreoPrincipal = ClaimValue<string>(JwtRegisteredClaimNames.Email);
            session.idPerfil = ClaimValue<long>("ProfileID");
            session.strPerfil = ClaimValue<string>("Profile");
            session.idMedico = ClaimValue<long>("DoctorID");
            return session;
        }

        private T ClaimValue<T>(string claimKey)
        {
            string valor = (((ClaimsIdentity)_http.HttpContext.User.Identity).FindFirst(claimKey) != null) ? ((ClaimsIdentity)_http.HttpContext.User.Identity).FindFirst(claimKey).Value : "";
            return (valor != "") ? (T)Convert.ChangeType(valor, typeof(T)) : default(T);
        }
    }
}

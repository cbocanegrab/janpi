﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Structure.CommonStructure.Services
{
    public class TokenService : ITokenService
    {
        private readonly IConfiguration _configuration;
        private string _connectionString;
        private readonly TokenValidationParameters _tokenValidationParameters;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ISessionService _session;

        public TokenService(IConfiguration configuration,
            TokenValidationParameters tokenValidationParameters, IUnitOfWork unitOfWork, ISessionService session)
        {
            _configuration = configuration;
            _connectionString = configuration.GetConnectionString("ConnJAMPI");
            _tokenValidationParameters = tokenValidationParameters;
            _unitOfWork = unitOfWork;
            _session = session;
        }


        public async Task<Response<CredentialResponse>> GetSessionToken()
        {
            Response<CredentialResponse> oResponse = new Response<CredentialResponse>();
            Session session = _session.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    Session user = await _unitOfWork.usuarioRepository.obtenerSesion(con, session.idUsuario);
                    if (user != null)
                    {
                        oResponse.data = GenerateJwtSession(user);
                        //await _unitOfWork.usuarioJwtRepository.Add(con, oResponse.data);
                        oResponse.success = true;
                    }
                    else
                    {
                        oResponse.data = null;
                        oResponse.message = Constants.Token._AUTENTICACION_ERROR_GENERA_TOKEN;
                        oResponse.success = false;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                }
            }
            return oResponse;
        }

        public string GenerateJwtLogin(long idUsuario,long idPersona)
        {
            var key = Encoding.ASCII.GetBytes(_configuration["SecureToken"]);

            ClaimsIdentity claims = new ClaimsIdentity();
            claims.AddClaim(new Claim("UserID", idUsuario.ToString()));
            claims.AddClaim(new Claim("PersonID", idPersona.ToString()));

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = claims,
                Expires = DateTime.UtcNow.AddMinutes(30),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var createdToken = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(createdToken).ToString();
        }

        public async Task<Response<CredentialResponse>> Refresh(RefreshTokenRequest refreshTokenRequest)
        {

            Response<CredentialResponse> response = new Response<CredentialResponse>();
            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken validatedToken;
            var principal = tokenHandler.ValidateToken(refreshTokenRequest.strToken,
                _tokenValidationParameters, out validatedToken);
            var jwtToken = validatedToken as JwtSecurityToken;
            if (jwtToken == null ||
                jwtToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256Signature))
            {
                response.success = false;
                response.message = Constants.Jwt._NO_VALID_ALGORITHM;
                return response;
            }

            var expDateJwt = long.Parse(principal.Claims.Single(x =>
            x.Type == JwtRegisteredClaimNames.Exp).Value);

            var expDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
                .AddSeconds(expDateJwt);

            if (expDate > DateTime.UtcNow)
            {
                response.success = false;
                response.message = Constants.Jwt._TOKEN_NOT_EXPIRED;
                return response;
            }

            //using (DA_Transaccion con = new DA_Transaccion("EELEC"))
            //{
            //    try
            //    {
            //        await con.BeginTransactionAsync(false);
            //        int existe = await _unitOfWork.usuarioJwtRepository.Get(con, new CredentialResponse { strToken = refreshTokenRequest.strToken, strTokenRefresh = refreshTokenRequest.strTokenRefresh });
            //        if (existe > 0)
            //        {
            //            Session session = new Session();
            //            session.idUsuario = ClaimValue<int>(principal, "UserID");
            //            session.jwtId = ClaimValue<string>(principal, "jti");
            //            session.strUsuario = ClaimValue<string>(principal, "UserName");
            //            session.idTipoUsuario = ClaimValue<int>(principal, "UserTypeID");
            //            session.strUrlImagen = ClaimValue<string>(principal, "UserPhoto");
            //            session.strNombres = ClaimValue<string>(principal, "FirstName");
            //            session.strApellido = ClaimValue<string>(principal, "LastName");
            //            session.idOrganizacion = ClaimValue<int>(principal, "OrganizationID");


            //            response.data = GenerateJwtSession(session);
            //            await _unitOfWork.usuarioJwtRepository.Add(con, response.data);
            //            response.success = true;
            //        }
            //        else
            //        {
            //            response.data = null;
            //            response.message = Constants.Jwt._TOKEN_REFRESH_NOT_EXISTS;
            //            response.success = false;
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        response.success = false;
            //        response.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
            //    }
            //    finally
            //    {
            //        con.Ejecutar(response.success);
            //    }
            //}
            return response;
        }

        private CredentialResponse GenerateJwtSession(Session usuario)
        {
            CredentialResponse credentialResponse = new CredentialResponse();

            var key = Encoding.ASCII.GetBytes(_configuration["SecureToken"]);
            ClaimsIdentity claims = new ClaimsIdentity();
            claims.AddClaim(new Claim("UserID", usuario.idUsuario.ToString())); //Sujeto del Jwt
            claims.AddClaim(new Claim("PersonID", usuario.idPersona.ToString())); //id persona
            claims.AddClaim(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())); //Id del JWT
            claims.AddClaim(new Claim("UserDocument", usuario.strNumeroDocumento));
            claims.AddClaim(new Claim("FirstName", usuario.strNombre));
            claims.AddClaim(new Claim("LastName", usuario.strPrimerApellido));
            claims.AddClaim(new Claim("ProfileID", usuario.idPerfil.ToString()));
            claims.AddClaim(new Claim("Profile", usuario.strPerfil));
            claims.AddClaim(new Claim("DoctorID", usuario.idMedico.ToString()));
            claims.AddClaim(new Claim("UserPhoto", usuario.strRutaFoto));
            claims.AddClaim(new Claim("DoctorSign", usuario.strRutaFirma));
            claims.AddClaim(new Claim(JwtRegisteredClaimNames.Email, usuario.strCorreoPrincipal));

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = claims,
                Expires = DateTime.UtcNow.AddHours(10),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            var createdToken = tokenHandler.CreateToken(tokenDescriptor);
            credentialResponse.strToken = tokenHandler.WriteToken(createdToken).ToString();
            credentialResponse.strTokenRefresh = GenerateRefreshToken();
            return credentialResponse;
        }

        private T ClaimValue<T>(ClaimsPrincipal principal, string claimKey)
        {
            string valor = (((ClaimsIdentity)principal.Identity).FindFirst(claimKey) != null) ? ((ClaimsIdentity)principal.Identity).FindFirst(claimKey).Value : "";
            return (valor != "") ? (T)Convert.ChangeType(valor, typeof(T)) : default(T);
        }

        private string GenerateRefreshToken()
        {
            var randomNumber = new byte[32];
            using (var randomNumberGenerator = RandomNumberGenerator.Create())
            {
                randomNumberGenerator.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }
    }
}

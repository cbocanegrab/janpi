﻿using AutoMapper;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Structure.EspecialidadTipStructure.Mapper
{
    public class EspecialidadTipMapper : Profile
    {
        public EspecialidadTipMapper()
        {
            CreateMap<EspecialidadTip, EspecialidadTipResponse>();
            CreateMap<EspecialidadTipRequest, EspecialidadTip>();
        }
    }
}

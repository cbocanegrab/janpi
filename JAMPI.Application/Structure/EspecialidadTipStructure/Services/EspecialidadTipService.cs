﻿using AutoMapper;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Common;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Structure.EspecialidadStructure.Services
{
    public class EspecialidadTipService : IEspecialidadTipService
    {
        private readonly IUnitOfWork _unitOfWork;
        private string _connectionString;
        private readonly IUtilService _util;
        private readonly ISessionService _sessionService;
        private readonly Session _session;
        private readonly IMapper _mapper;

        public EspecialidadTipService(IConfiguration configuration, IUnitOfWork unitOfWork, IUtilService util, ISessionService sessionService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _connectionString = configuration.GetConnectionString("ConnJAMPI");
            _sessionService = sessionService;
            _session = _sessionService.GetSession();
            _util = util;
            _mapper = mapper;
        }
        public async Task<Response<List<EspecialidadTipResponse>>> Listar()
        {
            Response<List<EspecialidadTipResponse>> oResponse = new Response<List<EspecialidadTipResponse>>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var especialidad = await _unitOfWork.especialidadTipRepository.GetAll(con);
                    oResponse.data = _mapper.Map<List<EspecialidadTipResponse>>(especialidad);
                    oResponse.success = true;
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.message = Constants._GENERAL_ERROR;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<List<EspecialidadTipResponse>>> ListarPorEspecialidad(long id)
        {
            Response<List<EspecialidadTipResponse>> oResponse = new Response<List<EspecialidadTipResponse>>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var especialidad = await _unitOfWork.especialidadTipRepository.GetAllByEspecialidad(con, id);
                    oResponse.data = _mapper.Map<List<EspecialidadTipResponse>>(especialidad);
                    oResponse.success = true;
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.message = Constants._GENERAL_ERROR;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<EspecialidadTipResponse>> Obtener(long id)
        {
            Response<EspecialidadTipResponse> oResponse = new Response<EspecialidadTipResponse>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var especialidad = await _unitOfWork.especialidadTipRepository.Get(con, id);
                    oResponse.data = _mapper.Map<EspecialidadTipResponse>(especialidad);
                    oResponse.success = true;
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.message = Constants._GENERAL_ERROR;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<long>> Agregar(EspecialidadTipRequest especialidad)
        {
            Response<long> oResponse = new Response<long>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var _especialidad = _mapper.Map<EspecialidadTip>(especialidad);
                    _especialidad.idUsuarioRegistro = _session.idUsuario;
                    var response = await _unitOfWork.especialidadTipRepository.Add(con, _especialidad);
                    if (response.resultado > 0)
                    {
                        oResponse.success = true;
                        oResponse.data = response.resultado;
                    }
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = response.mensaje;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.message = Constants._GENERAL_ERROR;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> Actualizar(EspecialidadTipRequest especialidad)
        {
            Response<bool> oResponse = new Response<bool>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var _especialidad = _mapper.Map<EspecialidadTip>(especialidad);
                    _especialidad.idUsuarioRegistro = _session.idUsuario;
                    var response = await _unitOfWork.especialidadTipRepository.Update(con, _especialidad);
                    if (response.resultado > 0)
                        oResponse.success = true;
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = response.mensaje;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.message = Constants._GENERAL_ERROR;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> Borrar(long id)
        {
            Response<bool> oResponse = new Response<bool>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    EspecialidadTip entidad = new EspecialidadTip()
                    {
                        idEspecialidad = id,
                        idUsuarioRegistro = _session.idUsuario
                };
                    ResponseSQL responseSQL = await _unitOfWork.especialidadTipRepository.Delete(con, entidad);
                    if (responseSQL.resultado > 0) oResponse.success = true;
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = "";
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.message = Constants._GENERAL_ERROR;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                }
            }
            return oResponse;
        }
    }
}

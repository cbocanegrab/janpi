﻿using AutoMapper;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Structure.PersonaMedidaStructure.Mapper
{
    public class PersonaMedidaMapper : Profile
    {
        public PersonaMedidaMapper()
        {
            CreateMap<PersonaMedida, MedidasResponse>()
                .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.idPersonaMedida)).ReverseMap();
            CreateMap<MedidaRequest, PersonaMedida>()
                .ForMember(dest => dest.idPersonaMedida, opt => opt.MapFrom(src => src.id)).ReverseMap();
        }
    }
}

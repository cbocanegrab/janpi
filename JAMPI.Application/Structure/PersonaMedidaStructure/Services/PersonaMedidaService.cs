﻿using AutoMapper;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace JAMPI.Application.Structure.MedidasStructure.Services
{
    public class PersonaMedidaService : IMedidaService
    {
        private readonly IConfiguration _configuration;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ISessionService _sessionService;
        private readonly Session _session;
        private readonly IMapper _mapper;
        private string _connectionString;
        public PersonaMedidaService(IConfiguration configuration, IUnitOfWork unitOfWork, ISessionService sessionService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _sessionService = sessionService;
            _session = _sessionService.GetSession();
            _mapper = mapper;
            _configuration = configuration;
            _connectionString = configuration.GetConnectionString("ConnJAMPI");
        }
        public async Task<Response<List<MedidasResponse>>> ListarMedidas(long idParamTipoMedida)
        {
            Response<List<MedidasResponse>> oResponse = new Response<List<MedidasResponse>>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                Session session = _sessionService.GetSession();
                try
                {
                    await con.OpenAsync();
                    var response = await _unitOfWork.personaMedidaRepository.GetAllByPersona(con, idParamTipoMedida, _session.idPersona);
                    oResponse.data = _mapper.Map<List<MedidasResponse>>(response);
                    oResponse.success = true;
                    oResponse.message = Constants.PersonaMedidas._LIST_SUCCESS;

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> InsertMedidas(MedidaRequest medidaRequest)
        {
            Response<bool> oResponse = new Response<bool>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    MedidasResponse medidasResponse = new MedidasResponse();
                    var persona = _mapper.Map<PersonaMedida>(medidaRequest);
                    persona.idPersona = _session.idPersona;
                    persona.idUsuarioRegistro = _session.idUsuario;
                    int i= await _unitOfWork.personaMedidaRepository.Add(con, persona);

                    if (i > 0)
                        oResponse.success = true;
                    else
                    {
                        oResponse.developer = Constants._GENERAL_ERROR;
                        oResponse.success = false;
                        oResponse.message = Constants._GENERAL_ERROR;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                }
            }
            return oResponse;
        }

        public async Task<Response<bool>> EliminarMedidas(MedidaRequest medidaRequest)
        {
            Response<bool> oResponse = new Response<bool>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    MedidasResponse medidasResponse = new MedidasResponse();
                    var persona = _mapper.Map<PersonaMedida>(medidaRequest);
                    persona.idUsuarioModifica = _session.idUsuario;
                    int i = await _unitOfWork.personaMedidaRepository.Delete(con, persona);
                    if (i > 0)
                        oResponse.success = true;
                    else
                    {
                        oResponse.developer = Constants._GENERAL_ERROR;
                        oResponse.success = false;
                        oResponse.message = Constants._GENERAL_ERROR;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                }
            }
            return oResponse;
        }



    }
}

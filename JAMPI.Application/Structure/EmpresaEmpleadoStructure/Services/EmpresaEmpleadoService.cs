﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Common;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Structure.EmpresaEmpleadoEmpleadoStructure.Services
{
    public class EmpresaEmpleadoService : IEmpresaEmpleadoService
    {
        private readonly IUnitOfWork _unitOfWork;
        private string _connectionString;
        private IConfiguration _configuration;
        private readonly IUtilService _util;
        private readonly ISessionService _sessionService;
        public EmpresaEmpleadoService(IConfiguration configuration, IUnitOfWork unitOfWork, IUtilService util, ISessionService sessionService)
        {
            _unitOfWork = unitOfWork;
            _connectionString = configuration.GetConnectionString("ConnJAMPI");
            _configuration = configuration;
            _util = util;
            _sessionService = sessionService;
        }
        public async Task<Response<long>> InsertarAsync(EmpresaEmpleadoIRequest EmpresaEmpleado)
        {
            Response<long> oResponse = new Response<long>();
            string strkey = _configuration.GetSection("KeyEncrypt").Value;
            
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();
                    string strClave = _util.EncryptTripleDES(EmpresaEmpleado.strNumeroDocumento, strkey);
                    oResponseSQL = await _unitOfWork.empresaEmpleadoRepository.EmpresaEmpleadoInsertar(con, EmpresaEmpleado,strClave,session.idUsuario);
                    oResponse.success = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        var msjEmail = $"Estimado(a): " + EmpresaEmpleado.strNombre + "<br><br>";
                        msjEmail += $"Le enviamos la contraseña de su cuenta JAMPI: ";
                        msjEmail += $"{EmpresaEmpleado.strNumeroDocumento}<br><br>";
                        msjEmail += $"Atte.<br>";
                        msjEmail += $"JAMPI";
                        Response<bool> responseMail = await _util.EnviarCorreo(EmpresaEmpleado.strCorreoPrincipal, "Cuenta JAMPI", msjEmail, true);
                        oResponse.success = true;
                        oResponse.data = oResponseSQL.resultado;
                        oResponse.message = Constants._GENERAL_INSERT_OK;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                        oResponse.developer = oResponseSQL.mensaje;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer =  ex.Message;
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> ActualizarAsync(EmpresaEmpleadoURequest EmpresaEmpleado)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();

                    ResponseSQL oResponseSQL = new ResponseSQL();
                    oResponseSQL = await _unitOfWork.empresaEmpleadoRepository.EmpresaEmpleadoActualizar(con, EmpresaEmpleado, session.idUsuario);
                    oResponse.success = false;
                    
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.data = true;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_UPDATE_OK;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_UPDATE_ERROR;
                        oResponse.developer = oResponseSQL.mensaje;
                        oResponse.data = false;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }

        public async Task<Response<bool>> EliminarAsync(long id)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {

                    ResponseSQL oResponseSQL = new ResponseSQL();
                    oResponseSQL = await _unitOfWork.empresaEmpleadoRepository.EsmpresaEmpleadoEliminar(con, id,session.idUsuario);
                    oResponse.success = false;
                    
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.data = true;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_DELETE_OK;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_UPDATE_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                        oResponse.data = false;
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = ex.Message;
                }
            }
            return oResponse;
        }
        public async Task<Response<EmpresaEmpleadoResponse>> Obtener(long id)
        {
            Response<EmpresaEmpleadoResponse> oResponse = new Response<EmpresaEmpleadoResponse>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    oResponse.data = await _unitOfWork.empresaEmpleadoRepository.GetEmpleado(con, id);
                    oResponse.success = true;
                    if (oResponse.data == null)
                    {
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<List<EmpresaEmpleadoResponse>>> Listar()
        {
            Response<List<EmpresaEmpleadoResponse>> oResponse = new Response<List<EmpresaEmpleadoResponse>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    oResponse.data = await _unitOfWork.empresaEmpleadoRepository.GetAllEmpleado(con,session.idUsuario);
                    oResponse.success = true;
                    if (oResponse.data == null)
                    {
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                       oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
    }
}

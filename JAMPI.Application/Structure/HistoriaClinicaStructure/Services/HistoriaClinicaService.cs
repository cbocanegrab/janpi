﻿using AutoMapper;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Interfaces.Common;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Structure.HistoriaClinicaStructure.Services
{
    public class HistoriaClinicaService : IHistoriaClinicaService
    {
        private readonly IUnitOfWork _unitOfWork;
        private string _connectionString;
        private readonly IUtilService _util;
        private readonly ISessionService _sessionService;
        private readonly IMapper _mapper;

        public HistoriaClinicaService(IConfiguration configuration, IUnitOfWork unitOfWork, IUtilService util, ISessionService sessionService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _connectionString = configuration.GetConnectionString("ConnJAMPI");
            _util = util;
            _sessionService = sessionService;
            _mapper = mapper;
        }
        public async Task<Response<long>> HistoriaClinicaInsertar(HistoriaClinicaIRequest oHistoriaClinica)
        {
            Response<long> oResponse = new Response<long>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();

                    oResponseSQL = await _unitOfWork.historiaClinicaRepository.HistoriaClinicaInsertar(con, oHistoriaClinica, session.idUsuario);
                    oResponse.success = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_INSERT_OK;
                        oResponse.data = oResponseSQL.resultado;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                        oResponse.data = 0;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }

        public async Task<Response<bool>> HistoriaClinicaActualizar(HistoriaClinicaURequest oHistoriaClinica)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();

                    ResponseSQL oResponseSQL = new ResponseSQL();
                    oResponseSQL = await _unitOfWork.historiaClinicaRepository.HistoriaClinicaActualizar(con, oHistoriaClinica, session.idUsuario);
                    oResponse.success = false;
                    oResponse.data = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.data = true;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_UPDATE_OK;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_UPDATE_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }

        public async Task<Response<List<HistoriaClinica>>> HistoriaClinicaListar(long idPersona)
        {
            var oResponse = new Response<List<HistoriaClinica>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.historiaClinicaRepository.HistoriaClinicaListar(con, idPersona);
                    oResponse.success = false;
                    if (lst == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = lst;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }

        public async Task<Response<long>> HistoriaClinicaDetalleInsertar(HistoriaClinicaDetalleIRequest oHistoriaClinicaDetalle)
        {
            Response<long> oResponse = new Response<long>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();

                    oResponseSQL = await _unitOfWork.historiaClinicaDetalleRepository.HistoriaClinicaDetalleInsertar(con, oHistoriaClinicaDetalle, session.idUsuario);
                    oResponse.success = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_INSERT_OK;
                        oResponse.data = oResponseSQL.resultado;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                        oResponse.data = 0;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }

        public async Task<Response<bool>> HistoriaClinicaDetalleActualizar(HistoriaClinicaDetalleURequest oHistoriaClinicaDetalle)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();

                    ResponseSQL oResponseSQL = new ResponseSQL();
                    oResponseSQL = await _unitOfWork.historiaClinicaDetalleRepository.HistoriaClinicaDetalleActualizar(con, oHistoriaClinicaDetalle, session.idUsuario);
                    oResponse.success = false;
                    oResponse.data = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.data = true;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_UPDATE_OK;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_UPDATE_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }

        public async Task<Response<List<HistoriaClinicaDetalle>>> HistoriaClinicaDetalleListar(long idHistoriaClinica)
        {

            var oResponse = new Response<List<HistoriaClinicaDetalle>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.historiaClinicaDetalleRepository.HistoriaClinicaDetalleListar(con, idHistoriaClinica);
                    oResponse.success = false;
                    if (lst == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = lst;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
    }
}

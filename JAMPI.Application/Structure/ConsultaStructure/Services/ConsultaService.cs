﻿using AutoMapper;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Common;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Structure.ConsultaStructure.Services
{
    public class ConsultaService : IConsultaService
    {
        private readonly IUnitOfWork _unitOfWork;
        private string _connectionString;
        private readonly IUtilService _util;
        private readonly ISessionService _sessionService;
        private readonly IMapper _mapper;
        public ConsultaService(IConfiguration configuration, IUnitOfWork unitOfWork, IUtilService util, ISessionService sessionService,IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _connectionString = configuration.GetConnectionString("ConnJAMPI");
            _util = util;
            _sessionService = sessionService;
            _mapper = mapper;
        }
        public async Task<Response<List<Consulta>>> Listar()
        {
            Response<List<Consulta>> oResponse = new Response<List<Consulta>>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    oResponse.data = await _unitOfWork.consultaRepository.GetAll(con);
                    oResponse.success = true;
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<Consulta>> Obtener(long id)
        {
            Response<Consulta> oResponse = new Response<Consulta>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    oResponse.data = await _unitOfWork.consultaRepository.Get(con, id);
                    oResponse.success = true;
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<long>> Agregar(ConsultaBasicRequest consulta)
        {
            Response<long> oResponse = new Response<long>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var response = await _unitOfWork.consultaRepository.AddAsync(con, consulta, session);
                    if (response.resultado > 0) { 
                        oResponse.data = response.resultado;
                        oResponse.success = true;
                    }
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = response.mensaje;
                    }
                }
                catch (SqlException ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<int>> Programar(ConsultaProgramaRequest programaRequest)
        {
            Response<int> oResponse = new Response<int>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var response = await _unitOfWork.consultaRepository.Programar(con, programaRequest);
                    if (response.resultado > 0)
                        oResponse.success = true;
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = response.mensaje;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> ActualizarTriaje(ConsultaTriajeRequest triajeRequest)
        {
            Response<bool> oResponse = new Response<bool>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var response = await _unitOfWork.consultaRepository.UpdateTriaje(con, triajeRequest);
                    if (response.resultado > 0)
                        oResponse.success = true;
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = response.mensaje;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> CancelarPorPaciente(ConsultaCancelaRequest cancelaRequest)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var response = await _unitOfWork.consultaRepository.CancelarPorPaciente(con, cancelaRequest,session.idUsuario);
                    if (response.resultado > 0)
                        oResponse.success = true;
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = response.mensaje;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> CancelarPorMedico(ConsultaCancelaRequest cancelaRequest)
        {
            Response<bool> oResponse = new Response<bool>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var response = await _unitOfWork.consultaRepository.CancelarPorMedico(con, cancelaRequest);
                    if (response.resultado > 0)
                        oResponse.success = true;
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = response.mensaje;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> ActualizarURL(ConsultaURLRequest urlRequest)
        {
            Response<bool> oResponse = new Response<bool>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var response = await _unitOfWork.consultaRepository.ActualizarURL(con, urlRequest);
                    if (response.resultado > 0)
                        oResponse.success = true;
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = response.mensaje;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> Borrar(long id)
        {
            Response<bool> oResponse = new Response<bool>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var response = await _unitOfWork.consultaRepository.Delete(con, id);
                    if (response.resultado > 0) oResponse.success = true;
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = response.mensaje;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }

        public async Task<Response<ConsultaResumenResponse>> ProximaConsulta()
        {
            Response<ConsultaResumenResponse> oResponse = new Response<ConsultaResumenResponse>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var data = await _unitOfWork.consultaRepository.ProximaConsulta(con,0, session.idPersona);
                    if (data == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = data;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<ConsultaResumenResponse>> UltimaConsulta()
        {
            Response<ConsultaResumenResponse> oResponse = new Response<ConsultaResumenResponse>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var data = await _unitOfWork.consultaRepository.UltimaConsulta(con, session.idPersona);
                    if (data == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = data;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> ProcesarPago(ConsultaPagoRequest obj)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    Pago oPago = new Pago();
                    oPago.idConsulta = obj.idConsulta;
                    oPago.strTransaccion = obj.strTransaccion;
                    oPago.strFechaTransaccion = obj.strFechaTransaccion;
                    oPago.decPago = 60;
                    oPago.idParamTipoTarjeta = 50;
                    oPago.strTarjeta = "*******5621";
                    if (obj.decCreditoUsado > 0) { oPago.strUsoCredito = "1"; } else { oPago.strUsoCredito = "0"; };
                    oPago.decCreditoIni = 0;
                    oPago.decCreditoFin = 0;
                    oPago.idParamTipoTransaccion = 55;

                    var data = await _unitOfWork.consultaRepository.ProcesarPago(con, oPago, session.idUsuario);
                    if (data.mensaje == "")
                    {
                        oResponse.message = "Pago registrado con éxito";
                        oResponse.data = true;
                        oResponse.success = true;
                    }
                    else
                    {
                        oResponse.data = false;
                        oResponse.success = false;
                        oResponse.developer = data.mensaje;
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<string>> LinkConsulta(long idConsulta)
        {
            Response<string> oResponse = new Response<string>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var data = await _unitOfWork.consultaRepository.LinkConsulta(con, idConsulta);
                    if (data == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = data.strURL;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> ValorarConsulta(ConsultaValoracionRequest obj)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var data = await _unitOfWork.consultaRepository.ValorarConsulta(con, obj, session.idUsuario);
                    if (data.resultado == 1)
                    {
                        oResponse.data = true;
                        oResponse.success = true;
                    }
                    else
                    {
                        oResponse.data = false;
                        oResponse.success = true;
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<List<OrdenResponse>>> Ordenes()
        {
            Response<List<OrdenResponse>> oResponse = new Response<List<OrdenResponse>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var response = await _unitOfWork.consultaRepository.Ordenes(con, session.idPersona);
                    if (response == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = response;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<ConsultaDetalleResponse>> ConsultaDetalle(long idConsulta)
        {
            Response<ConsultaDetalleResponse> oResponse = new Response<ConsultaDetalleResponse>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var data = await _unitOfWork.consultaRepository.ConsultaDetalle(con, idConsulta);
                    if (data == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = data;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> InsertarSeguimientoAsync(SeguimientoRequest oSeguimiento)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();

                    oResponseSQL = await _unitOfWork.consultaRepository.SeguimientoInsert(con, oSeguimiento, session.idUsuario);
                    oResponse.success = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_INSERT_OK;
                        oResponse.data = true;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                        oResponse.data = false;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<ConsultaResumenResponse>> ProximaConsultaMedico()
        {
            Response<ConsultaResumenResponse> oResponse = new Response<ConsultaResumenResponse>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var data = await _unitOfWork.consultaRepository.ProximaConsulta(con,session.idPersona,0);
                    if (data == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = data;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<List<ConsultaResumenResponse>>> ConsultaProgramadaMedico()
        {
            Response<List<ConsultaResumenResponse>> oResponse = new Response<List<ConsultaResumenResponse>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.consultaRepository.ConsultaProgramada(con, session.idPersona, 0);
                    DateTime fecha = DateTime.Now;
                    if (lst == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        var lst1 = _mapper.Map<List<ConsultaResumenResponse>>(lst);
                        
                        foreach(var item in lst1)
                        {
                            int  horaConsulta = Convert.ToInt32(item.strHora.Substring(0, 2));
                            DateTime fechaConsulta = DateTime.ParseExact(item.strFechaConsulta + " " + item.strHora+":00", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                            if (fecha< fechaConsulta)
                            {
                                double horas = Math.Truncate(fechaConsulta.Subtract(fecha).TotalHours);
                                double dias = Math.Truncate(fechaConsulta.Subtract(fecha).TotalDays);
                                item.strTiempoParaConsulta = dias > 0? "faltan " + dias.ToString() +" días para tu próxima cita" : "faltan " + horas.ToString() + " horas para tu próxima cita";
                            }

                        }
                        oResponse.data = lst1;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<List<ConsultaResumenResponse>>> ConsultaHistoricoMedico(long idEspecialidad,long idEstadoTratamiento)
        {
            Response<List<ConsultaResumenResponse>> oResponse = new Response<List<ConsultaResumenResponse>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.consultaRepository.ConsultaHistorico(con, session.idPersona, 0,idEspecialidad,idEstadoTratamiento);
                    if (lst == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = _mapper.Map<List<ConsultaResumenResponse>>(lst); ;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<List<ConsultaResumenResponse>>> ConsultaHistoricoMedicoAll()
        {
            Response<List<ConsultaResumenResponse>> oResponse = new Response<List<ConsultaResumenResponse>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.consultaRepository.ConsultaHistorico(con, session.idPersona, 0, 0, 0);
                    if (lst == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = _mapper.Map<List<ConsultaResumenResponse>>(lst); ;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> ConsultaExamenGuardarAsync(List<ConsultaExamenGRequest> lConsultaExamen)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();
                    bool bResult = true;
                    string mensaje = "";
                    foreach (ConsultaExamenGRequest item in lConsultaExamen)
                    {
                        var o = await _unitOfWork.consultaExamenRepository.ConsultaExamenGuardar(con, item, session.idUsuario);
                        if (o.mensaje != "")
                        {
                            bResult = false;
                            mensaje = o.mensaje;
                            break;
                        }
                    }
                    oResponse.success = false;
                    if (bResult)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_INSERT_OK;
                        oResponse.data = true;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, 0, mensaje);
                        oResponse.data = false;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<List<ConsultaExamenResponse>>> ConsultaExamenListar(long idConsulta)
        {
            Response<List<ConsultaExamenResponse>> oResponse = new Response<List<ConsultaExamenResponse>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.consultaExamenRepository.ConsultaExamenListar(con, idConsulta);
                    oResponse.success = false;
                    if (lst == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = _mapper.Map<List<ConsultaExamenResponse>>(lst);
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> ReconsultaInsertarAsync(ReconsultaRequest oConsulta)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();

                    oResponseSQL = await _unitOfWork.consultaRepository.ReconsultaInsertar(con, oConsulta, session.idUsuario);
                    oResponse.success = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_INSERT_OK;
                        oResponse.data = true;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                        oResponse.data = false;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<List<ConsultaResumenResponse>>> ConsultaHistoricoPaciente(long idPersonaPaciente)
        {
            Response<List<ConsultaResumenResponse>> oResponse = new Response<List<ConsultaResumenResponse>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.consultaRepository.ConsultaHistorico(con, 0, idPersonaPaciente, 0, 0);
                    if (lst == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = _mapper.Map<List<ConsultaResumenResponse>>(lst); ;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> ConsultaDerivadaInsertarAsync(ConsultaDerivadaRequest oConsulta)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();

                    oResponseSQL = await _unitOfWork.consultaRepository.ConsultaDerivadaInsertar(con, oConsulta, session.idUsuario);
                    oResponse.success = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_INSERT_OK;
                        oResponse.data = true;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                        oResponse.data = false;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer =  ex.Message;
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> ConsultaDerivadaEliminarAsync(long idConsultaDerivada)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();

                    oResponseSQL = await _unitOfWork.consultaRepository.ConsultaDerivadaEliminar(con, idConsultaDerivada, session.idUsuario);
                    oResponse.success = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_DELETE_OK;
                        oResponse.data = true;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_DELETE_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                        oResponse.data = false;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = ex.Message;
                }
            }
            return oResponse;
        }
        public async Task<Response<ConsultaDerivadaResponse>> ConsultaDerivadaObtener()
        {
            Response<ConsultaDerivadaResponse> oResponse = new Response<ConsultaDerivadaResponse>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var data = await _unitOfWork.consultaRepository.ConsultaDerivadaObtener(con, session.idPersona);
                    oResponse.success = false;
                    if (data == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = data;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }

        public async Task<Response<ConsultaSeguimientoResponse>> ConsultaSeguimiento(long idConsulta)
        {
            var response = new Response<ConsultaSeguimientoResponse>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.consultaRepository.ConsultaSeguimiento(con, idConsulta);
                    if (lst != null) response.ObtenerResponseSuccess(lst);
                    else response.ObtenerResponseError(Constants._GENERAL_ERROR_NULL, Constants._GENERAL_ERROR_DEV_NULL);
                }
                catch (Exception ex)
                {
                    response.ObtenerResponseCatchError(String.Format(Constants._GENERAL_ERROR_DEV, ex.Message));
                }
            }
            return response;
        }
        public async Task<Response<ExamenResumenResponse>> ConsultaExamenResumen(long idConsulta)
        {
            var response = new Response<ExamenResumenResponse>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.consultaExamenRepository.ConsultaExamenResumen(con, idConsulta);
                    if (lst != null) response.ObtenerResponseSuccess(lst);
                    else response.ObtenerResponseError(Constants._GENERAL_ERROR_NULL, Constants._GENERAL_ERROR_DEV_NULL);
                }
                catch (Exception ex)
                {
                    response.ObtenerResponseCatchError(String.Format(Constants._GENERAL_ERROR_DEV, ex.Message));
                }
            }
            return response;
        }

        public async Task<Response<ConsultaProgramacionResponse>> ConsultaProgramacion()
        {
            var response = new Response<ConsultaProgramacionResponse>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var obj = await _unitOfWork.consultaRepository.ConsultaProgramacion(con, session.idMedico,0);
                    if (obj != null) response.ObtenerResponseSuccess(obj);
                    else response.ObtenerResponseError(Constants._GENERAL_ERROR_NULL, Constants._GENERAL_ERROR_DEV_NULL);
                }
                catch (Exception ex)
                {
                    response.ObtenerResponseCatchError(String.Format(Constants._GENERAL_ERROR_DEV, ex.Message));
                }
            }
            return response;
        }
        public async Task<Response<List<ConsultaProgMin>>> ConsultaPorFecha(string strFecha)
        {
            Response<List<ConsultaProgMin>> oResponse = new Response<List<ConsultaProgMin>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.consultaRepository.ConsultaPorFecha(con, strFecha, session.idMedico, 0);
                    if (lst == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = lst ;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer =  ex.Message;
                    oResponse.message = Constants._GENERAL_ERROR;
                }
            }
            return oResponse;
        }
    }
}

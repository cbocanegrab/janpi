﻿using AutoMapper;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Structure.ConsultaStructure.Mapper
{
    public class ConsultaMapper: Profile
    {
        public ConsultaMapper()
        {
            CreateMap<Consulta, ConsultaResumenResponse>().ReverseMap();
            CreateMap<ConsultaExamen, ConsultaExamenResponse>().ReverseMap();
            
        }
    }
}

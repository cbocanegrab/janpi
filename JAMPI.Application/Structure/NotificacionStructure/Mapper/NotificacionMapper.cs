﻿using AutoMapper;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Structure.NotificacionStructure.Mapper
{
   public  class NotificacionMapper : Profile
    {
        public NotificacionMapper()
        {
            CreateMap<Notificacion, NotificacionResponse>().ReverseMap();
        }
    }
}

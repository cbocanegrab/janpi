﻿using AutoMapper;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Structure.NotificacionStructure.Services
{
    public class NotificacionService : INotificacionService
    {
        private readonly IConfiguration _configuration;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ISessionService _sessionService;
        private readonly IMapper _mapper;
        private readonly Session _session;
        private string _connectionString;
        public NotificacionService(IConfiguration configuration, IUnitOfWork unitOfWork, ISessionService sessionService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _sessionService = sessionService;
            _mapper = mapper;
            _session = _sessionService.GetSession();
            _configuration = configuration;
            _connectionString = configuration.GetConnectionString("ConnJAMPI");
        }
        public async Task<Response<List<NotificacionResponse>>> Listar()
        {
            Response<List<NotificacionResponse>> oResponse = new Response<List<NotificacionResponse>>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var notificacion = await _unitOfWork.notificacionRepository.GetAllByPersona(con, _session.idPersona);
                    oResponse.data = _mapper.Map<List<NotificacionResponse>>(notificacion);
                    oResponse.success = true;
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                }
            }
            return oResponse;
        }

        public async Task<Response<bool>> MarcarLeido(long id)
        {
            Response<bool> oResponse = new Response<bool>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    Notificacion notificacion = new Notificacion()
                    {
                        idNotificacion = id, idUsuarioRegistro = _session.idUsuario
                    };
                    var responseSQL = await _unitOfWork.notificacionRepository.UpdateLeido(con, notificacion);
                    if (responseSQL.resultado > 0)
                    {
                        oResponse.data = true;
                        oResponse.success = true;
                    }
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = Constants._GENERAL_ERROR;
                        oResponse.developer = responseSQL.mensaje;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
    }
}

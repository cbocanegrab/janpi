﻿using AutoMapper;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Structure.RecetaStructure.Services
{
    public class RecetaService : IRecetaService
    {
        private readonly IUnitOfWork _unitOfWork;
        private string _connectionString;
        private IConfiguration _configuration;
        private ISessionService _sessionService;
        private readonly IMapper _mapper;
        public RecetaService(IConfiguration configuration, IUnitOfWork unitOfWork, ISessionService sessionService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _connectionString = configuration.GetConnectionString("ConnJAMPI");
            _configuration = configuration;
            _sessionService = sessionService;
            _mapper = mapper;
        }
        public async Task<Response<long>> InsertarRecetaAsync(RecetaIRequest oReceta)
        {
            Response<long> oResponse = new Response<long>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();

                    oResponseSQL = await _unitOfWork.recetaRepository.InsertReceta(con, oReceta,session.idUsuario);
                    oResponse.success = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_INSERT_OK;
                        oResponse.data = oResponseSQL.resultado;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                        oResponse.data = 0;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> ActualizarRecetaAsync(RecetaURequest oReceta)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();

                    ResponseSQL oResponseSQL = new ResponseSQL();
                    oResponseSQL = await _unitOfWork.recetaRepository.UpdateReceta(con, oReceta,session.idUsuario);
                    oResponse.success = false;
                    oResponse.data = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.data = true;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_UPDATE_OK;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_UPDATE_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> EliminarRecetaAsync(long id)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();
                    oResponseSQL = await _unitOfWork.recetaRepository.DeleteReceta(con, id,session.idUsuario);
                    oResponse.success = false;
                    oResponse.data = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.data = true;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_DELETE_OK;
                    }
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = Constants._GENERAL_UPDATE_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<RecetaResponse>> ObtenerReceta(long id)
        {
            Response<RecetaResponse> oResponse = new Response<RecetaResponse>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var data = await _unitOfWork.recetaRepository.GetReceta(con, id);
                    oResponse.success = false;
                    if (data == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = data;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<List<RecetaResponse>>> ListarReceta()
        {
            Response<List<RecetaResponse>> oResponse = new Response<List<RecetaResponse>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.recetaRepository.GetAllReceta(con,session.idPersona);
                    oResponse.success = false;
                    if (lst == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = lst;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<long>> InsertarRecetaMedicamentoAsync(RecetaMedicamentoIRequest oRecetaMedicamento)
        {
            Response<long> oResponse = new Response<long>();
            string strkey = _configuration.GetSection("KeyEncrypt").Value;
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();

                    oResponseSQL = await _unitOfWork.recetaRepository.InsertRecetaMedicamento(con, oRecetaMedicamento, session.idUsuario);
                    
                    oResponse.success = false;
                    oResponse.data = oResponseSQL.resultado;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_INSERT_OK;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = ex.Message;
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> ActualizarRecetaMedicamentoAsync(RecetaMedicamentoURequest oRecetaMedicamento)
        {
            Response<bool> oResponse = new Response<bool>();
            string strkey = _configuration.GetSection("KeyEncrypt").Value;
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();

                    oResponseSQL = await _unitOfWork.recetaRepository.UpdateRecetaMedicamento(con, oRecetaMedicamento, session.idUsuario);

                    oResponse.success = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.data = true;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_INSERT_OK;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                        oResponse.data = false;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = string.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> EliminarRecetaMedicamentoAsync(long id)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();
                    oResponseSQL = await _unitOfWork.recetaRepository.DeleteRecetaMedicamento(con, id, session.idUsuario);
                    oResponse.success = false;
                    oResponse.data = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.data = true;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_DELETE_OK;
                    }
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = Constants._GENERAL_UPDATE_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<RecetaMedicamentoResponse>> ObtenerRecetaMedicamento(long id)
        {
            Response<RecetaMedicamentoResponse> oResponse = new Response<RecetaMedicamentoResponse>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var data = await _unitOfWork.recetaRepository.GetRecetaMedicamento(con, id);
                    data.lMedicamentoHorario = new List<MedicamentoHorario>();
                    data.lMedicamentoHorario = await _unitOfWork.recetaRepository.GetAllMedicamentoFrecuencia(con, data.idRecetaMedicamento);
                    oResponse.success = false;
                    if (data == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = data;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<List<RecetaMedicamentoResponse>>> ListarRecetaMedicamento(long idReceta)
        {
            Response<List<RecetaMedicamentoResponse>> oResponse = new Response<List<RecetaMedicamentoResponse>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.recetaRepository.GetAllRecetaMedicamento(con, idReceta);
                    oResponse.success = false;
                    if (lst == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = lst;
                        foreach (RecetaMedicamentoResponse obj in lst)
                        {
                            obj.lMedicamentoHorario = new List<MedicamentoHorario>();
                            obj.lMedicamentoHorario = await _unitOfWork.recetaRepository.GetAllMedicamentoFrecuencia(con, obj.idRecetaMedicamento);

                        }
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<long>> InsertarMedicamentoHorarioAsync(List<MedicamentoHorarioIRequest> lMedicamentoHorario)
        {
            Response<long> oResponse = new Response<long>();
            string strkey = _configuration.GetSection("KeyEncrypt").Value;
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();

                    bool bValida = true;
                    foreach (MedicamentoHorarioIRequest obj in lMedicamentoHorario)
                    {
                        oResponseSQL = await _unitOfWork.recetaRepository.InsertMedicamentoFrecuencia(con, obj, session.idUsuario);
                        if (oResponseSQL.mensaje != "")
                        {
                            bValida = false;
                            break;
                        }
                    }
                    if (bValida)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_INSERT_OK;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> ActualizarMedicamentoHorarioAsync(List<MedicamentoHorarioURequest> lMedicamentoHorario)
        {
            Response<bool> oResponse = new Response<bool>();
            string strkey = _configuration.GetSection("KeyEncrypt").Value;
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();
                    oResponse.success = false;
                        bool bValida = true;
                        foreach (MedicamentoHorarioURequest obj in lMedicamentoHorario)
                        {
                            var data = await _unitOfWork.recetaRepository.UpdateMedicamentoFrecuencia(con, obj, session.idUsuario);
                            if (data.mensaje != "")
                            {
                                bValida = false;
                                break;
                            }
                        }
                        if (bValida)
                        {
                            oResponse.success = true;
                            oResponse.message = Constants._GENERAL_INSERT_OK;
                            oResponse.data = true;
                        }
                        else
                        {
                            oResponse.message = Constants._GENERAL_INSERT_ERROR;
                            oResponse.data = false;
                        }
                    
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<List<TratamientoResponse>>> TratamientosVigentes()
        {
            Response<List<TratamientoResponse>> oResponse = new Response<List<TratamientoResponse>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.recetaRepository.TratamientoActual(con, session.idPersona);
                    oResponse.success = false;
                    if (lst == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = lst;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<List<TratamientoResponse>>> TratamientosHistoricos(long idMedico, long idEspecialidad, long idEstado)
        {
            Response<List<TratamientoResponse>> oResponse = new Response<List<TratamientoResponse>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.recetaRepository.TratamientoHistorico(con, session.idPersona, idMedico, idEspecialidad, idEstado);
                    oResponse.success = false;
                    if (lst == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = lst;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<List<TratamientoPorFechaResponse>>> TratamientosPorFecha(string strFecha)
        {
            Response<List<TratamientoPorFechaResponse>> oResponse = new Response<List<TratamientoPorFechaResponse>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.recetaRepository.TratamientoPorFecha(con, strFecha,session.idPersona,0);
                    oResponse.success = false;
                    if (lst == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = lst;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<List<TratamientoEnCursoResponse>>> TratamientosEnCurso()
        {
            Response<List<TratamientoEnCursoResponse>> oResponse = new Response<List<TratamientoEnCursoResponse>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lTratamiento = await _unitOfWork.recetaRepository.TratamientoActual(con, session.idPersona);
                    
                    oResponse.success = false;
                    if (lTratamiento == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        var lTratamientoVigente = _mapper.Map<List<TratamientoEnCursoResponse>>(lTratamiento);
                        string now = DateTime.Now.ToString("dd/MM/yyyy");

                        foreach (TratamientoEnCursoResponse item in lTratamientoVigente)
                        {
                            var lMedicamento = await _unitOfWork.recetaRepository.TratamientoPorFecha(con, now, session.idPersona, item.idConsulta);
                            item.lMedicamento = _mapper.Map<List<MedicamentoMinResponse>>(lMedicamento);
                        }
                        oResponse.data = lTratamientoVigente;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<TratamientoSeguimientoResponse>> TratamientosSeguimiento(long idConsulta)
        {
            Response<TratamientoSeguimientoResponse> oResponse = new Response<TratamientoSeguimientoResponse>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var data = await _unitOfWork.recetaRepository.TratamientoSeguimiento(con, idConsulta);

                    oResponse.success = false;
                    if (data == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = data;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<List<TratamientoSeguimientoResponse>>> TratamientosSeguimientoMedico()
        {
            Response<List<TratamientoSeguimientoResponse>> oResponse = new Response<List<TratamientoSeguimientoResponse>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.recetaRepository.TratamientoSeguimientoMedico(con, session.idPersona);

                    oResponse.success = false;
                    if (lst == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = lst;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<List<TratamientoEnCursoPacienteResponse>>> TratamientosEnCursoByPaciente(long idUsuarioPaciente)
        {
            Response<List<TratamientoEnCursoPacienteResponse>> oResponse = new Response<List<TratamientoEnCursoPacienteResponse>>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.recetaRepository.TratamientoEnCursoByPaciente(con, idUsuarioPaciente);


                    oResponse.success = false;
                    if (lst == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data =  _mapper.Map<List<TratamientoEnCursoPacienteResponse>>(lst);
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<RecetaResumenResponse>> RecetaResumen(long idConsulta)
        {
            var response = new Response<RecetaResumenResponse>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.recetaRepository.RecetaResumen(con, idConsulta);
                    if (lst != null) response.ObtenerResponseSuccess(lst);
                    else response.ObtenerResponseError(Constants._GENERAL_ERROR_NULL, Constants._GENERAL_ERROR_DEV_NULL);
                }
                catch (Exception ex)
                {
                    response.ObtenerResponseCatchError(String.Format(Constants._GENERAL_ERROR_DEV, ex.Message));
                }
            }
            return response;
        }

        public async Task<Response<TratamientoResumenResponse>> TratamientoResumen(long idConsulta)
        {
            var response = new Response<TratamientoResumenResponse>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.recetaRepository.TratamientoResumen(con, idConsulta);
                    if (lst != null) response.ObtenerResponseSuccess(lst);
                    else response.ObtenerResponseError(Constants._GENERAL_ERROR_NULL, Constants._GENERAL_ERROR_DEV_NULL);
                }
                catch (Exception ex)
                {
                    response.ObtenerResponseCatchError(String.Format(Constants._GENERAL_ERROR_DEV, ex.Message));
                }
            }
            return response;
        }
    }

}


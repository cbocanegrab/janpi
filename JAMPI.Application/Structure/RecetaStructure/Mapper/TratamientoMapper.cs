﻿using AutoMapper;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Structure.RecetaStructure.Mapper
{
    public class TratamientoMapper: Profile
    {
        public TratamientoMapper()
        {
            CreateMap<TratamientoResponse, TratamientoEnCursoResponse>().ReverseMap();
            CreateMap<TratamientoPorFechaResponse, MedicamentoMinResponse>().ReverseMap();
            CreateMap<Tratamiento, TratamientoEnCursoPacienteResponse>().ReverseMap();
        }
    }
}

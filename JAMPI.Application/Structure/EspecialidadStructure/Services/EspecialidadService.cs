﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Common;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Structure.EspecialidadStructure.Services
{
    public class EspecialidadService : IEspecialidadService
    {
        private readonly IUnitOfWork _unitOfWork;
        private string _connectionString;
        private readonly IUtilService _util;
        public EspecialidadService(IConfiguration configuration, IUnitOfWork unitOfWork, IUtilService util)
        {
            _unitOfWork = unitOfWork;
            _connectionString = configuration.GetConnectionString("ConnJAMPI");
            _util = util;
        }
        public async Task<Response<List<EspecialidadSelectorResponse>>> Listar()
        {
            Response<List<EspecialidadSelectorResponse>> oResponse = new Response<List<EspecialidadSelectorResponse>>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    oResponse.data = await _unitOfWork.especialidadMedicaRepository.GetAllBasic(con);
                    oResponse.success = true;
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.message = Constants._GENERAL_ERROR;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<Especialidad>> Obtener(long id)
        {
            Response<Especialidad> oResponse = new Response<Especialidad>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    oResponse.data = await _unitOfWork.especialidadMedicaRepository.Get(con, id);
                    oResponse.success = true;
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.message = Constants._GENERAL_ERROR;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<long>> Agregar(EspecialidadRequest especialidad)
        {
            Response<long> oResponse = new Response<long>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var response = await _unitOfWork.especialidadMedicaRepository.AddAsync(con, especialidad);
                    if (response.resultado > 0) { 
                        oResponse.success = true;
                        oResponse.data = response.resultado;
                    }
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = response.mensaje;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.message = Constants._GENERAL_ERROR;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> Actualizar(EspecialidadRequest especialidad)
        {
            Response<bool> oResponse = new Response<bool>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var response = await _unitOfWork.especialidadMedicaRepository.Update(con, especialidad);
                    if (response.resultado > 0)
                        oResponse.success = true;
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = response.mensaje;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.message = Constants._GENERAL_ERROR;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> Borrar(long id)
        {
            Response<bool> oResponse = new Response<bool>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    Especialidad entidad = new Especialidad()
                    {
                        idEspecialidad = id,
                        idUsuarioRegistro = 1
                    };
                    ResponseSQL responseSQL = await _unitOfWork.especialidadMedicaRepository.Delete(con, entidad);
                    if (responseSQL.resultado > 0) oResponse.success = true;
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = "";
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.message = Constants._GENERAL_ERROR;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                }
            }
            return oResponse;
        }
    }
}

﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Structure.EmpresaStructure.Services
{
    public class EmpresaService : IEmpresaService
    {
        private readonly IUnitOfWork _unitOfWork;
        private string _connectionString;
        private IConfiguration _configuration;
        private ISessionService _sessionService;
        public EmpresaService(IConfiguration configuration, IUnitOfWork unitOfWork, ISessionService sessionService)
        {
            _unitOfWork = unitOfWork;
            _connectionString = configuration.GetConnectionString("ConnJAMPI");
            _configuration = configuration;
            _sessionService = sessionService;
        }
        public async Task<Response<long>> InsertarAsync(EmpresaRequest empresa)
        {
            Response<long> oResponse = new Response<long>();
            string strkey = _configuration.GetSection("KeyEncrypt").Value;
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();
                    Empresa entidad = new Empresa()
                    {
                        strRUC = empresa.strRUC,
                        strRazonSocial = empresa.strRazonSocial,
                        strDireccionComercial =empresa.strDireccionComercial,
                        strUbigeo =empresa.strUbigeo,
                        strTelefono=empresa.strTelefono,
                        idUsuarioRegistro = session.idUsuario
                    };

                    oResponseSQL = await _unitOfWork.empresaRepository.Add(con, entidad);
                    oResponse.success = false;
                    oResponse.data = oResponseSQL.resultado;
                    if (oResponseSQL.resultado == 0)
                    {
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                    }
                    else if (oResponseSQL.resultado == 1)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_INSERT_OK;
                    }
                    else if (oResponseSQL.resultado == 2)
                    {
                        oResponse.message = Constants._EMPRESA_RUCEXISTE;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                        oResponse.data = 0;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<bool>> ActualizarAsync(EmpresaRequest empresa)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    Empresa entidad = new Empresa()
                    {
                        idEmpresa =empresa.idEmpresa,
                        strRUC = empresa.strRUC,
                        strRazonSocial = empresa.strRazonSocial,
                        strDireccionComercial = empresa.strDireccionComercial,
                        strUbigeo = empresa.strUbigeo,
                        strTelefono = empresa.strTelefono,
                        idUsuarioRegistro = session.idUsuario
                    };

                    ResponseSQL oResponseSQL = new ResponseSQL();
                    oResponseSQL = await _unitOfWork.empresaRepository.Update(con, entidad);
                    oResponse.success = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.data = true;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_UPDATE_OK;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_UPDATE_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                        oResponse.data = false;
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }

        public async Task<Response<bool>> EliminarAsync(long id)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    Empresa entidad = new Empresa()
                    {
                        idEmpresa = id,
                        idUsuarioRegistro = session.idUsuario
                    };

                    ResponseSQL oResponseSQL = new ResponseSQL();
                    oResponseSQL = await _unitOfWork.empresaRepository.Delete(con, entidad);
                    oResponse.success = false;
                     if (oResponseSQL.mensaje == "")
                    {
                        oResponse.data = true;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_DELETE_OK;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_UPDATE_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                        oResponse.data = false;
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<EmpresaResponse>> Obtener(long id)
        {
            Response<EmpresaResponse> oResponse = new Response<EmpresaResponse>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var data = await _unitOfWork.empresaRepository.Get(con, id);
                    oResponse.success = false;
                    if (data== null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = new EmpresaResponse()
                        {
                            idEmpresa = data.idEmpresa,
                            strRUC = data.strRUC,
                            strRazonSocial = data.strRazonSocial,
                            strDireccionComercial = data.strDireccionComercial,
                            strTelefono = data.strTelefono,
                            strUbigeo = data.strUbigeo
                        };
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<List<EmpresaResponse>>> Listar()
        {
            Response<List<EmpresaResponse>> oResponse = new Response<List<EmpresaResponse>>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.empresaRepository.GetAll(con);
                    oResponse.success = false;
                    if (lst == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = lst.ConvertAll(new Converter<Empresa, EmpresaResponse>(x => new EmpresaResponse
                        {
                            idEmpresa = x.idEmpresa,
                            strRUC = x.strRUC,
                            strRazonSocial = x.strRazonSocial,
                            strDireccionComercial = x.strDireccionComercial,
                            strTelefono = x.strTelefono,
                            strUbigeo = x.strUbigeo

                        }));
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<EmpresaResumenResponse>> ResumenSemanal()
        {
            Response<EmpresaResumenResponse> oResponse = new Response<EmpresaResumenResponse>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var data = await _unitOfWork.empresaRepository.ResumenSemanal(con, session.idUsuario);
                    
                    oResponse.success = false;
                    if (data == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = data;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = ex.Message;
                }
            }
            return oResponse;
        }
        public async Task<Response<EmpresaResumenResponse>> ResumenMensual()
        {
            Response<EmpresaResumenResponse> oResponse = new Response<EmpresaResumenResponse>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var data = await _unitOfWork.empresaRepository.ResumenMensual(con, session.idUsuario);

                    oResponse.success = false;
                    if (data == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = data;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = ex.Message;
                }
            }
            return oResponse;
        }
        public async Task<Response<List<ConsultaEmpleadoHistorialResponse>>> HistorialConsultas()
        {
            Response<List<ConsultaEmpleadoHistorialResponse>> oResponse = new Response<List<ConsultaEmpleadoHistorialResponse>>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var data = await _unitOfWork.empresaRepository.HistorialConsultas(con, session.idUsuario);
                    
                    oResponse.success = false;
                    if (data == null)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_NULL;
                    }
                    else
                    {
                        oResponse.data = data;
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_GET_OK;
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = ex.Message;
                }
            }
            return oResponse;
        }
    }
}

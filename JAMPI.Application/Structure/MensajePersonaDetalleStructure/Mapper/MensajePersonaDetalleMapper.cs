﻿using AutoMapper;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Structure.NotificacionStructure.Mapper
{
   public  class MensajePersonaDetalleMapper : Profile
    {
        public MensajePersonaDetalleMapper()
        {
            CreateMap<MensajePersonaDetalle, MensajePersonaDetalleResponse>()
                .ForMember(dest=>dest.id , opt=>opt.MapFrom(src=>src.idMensajePersonaDetalle))
                .ForMember(dest => dest.strFechaEnvio, opt => opt.MapFrom(src => src.strFechaEnvia))
                .ForMember(dest => dest.strFechaLeido, opt => opt.MapFrom(src => src.strFechaLee))
                .ReverseMap();
            CreateMap<MensajePersonaDetalle, MensajePersonaDetalleRequest>().ReverseMap();
            CreateMap<MensajePersonaDetalle, MensajeMedicoDetalleRequest>().ReverseMap();
        }
    }
}

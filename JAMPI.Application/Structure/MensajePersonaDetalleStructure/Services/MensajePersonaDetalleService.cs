﻿using AutoMapper;
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace JAMPI.Application.Structure.NotificacionStructure.Services
{
    public class MensajePersonaDetalleService : IMensajePersonaDetalleService
    {
        private readonly IConfiguration _configuration;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ISessionService _sessionService;
        private readonly IMapper _mapper;
        private readonly Session _session;
        private string _connectionString;
        public MensajePersonaDetalleService(IConfiguration configuration, IUnitOfWork unitOfWork, ISessionService sessionService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _sessionService = sessionService;
            _mapper = mapper;
            _session = _sessionService.GetSession();
            _configuration = configuration;
            _connectionString = configuration.GetConnectionString("ConnJAMPI");
        }
        public async Task<Response<List<MensajePersonaDetalleResponse>>> Listar(long idMensajePersona)
        {
            var response = new Response<List<MensajePersonaDetalleResponse>>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var notificacion = await _unitOfWork.mensajePersonaDetalleRepository.GetAllByPersona(con, idMensajePersona);
                    response.data = _mapper.Map<List<MensajePersonaDetalleResponse>>(notificacion);
                    response.success = true;
                }
                catch (Exception ex)
                {
                    response.success = false;
                    response.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                }
            }
            return response;
        }
        public async Task<Response<long>> Insert(MensajePersonaDetalleRequest mensajePersonaDetalle)
        {
            Response<long> oResponse = new Response<long>();
            SqlTransaction trx = null;
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    trx = con.BeginTransaction();
                    var data = _mapper.Map<MensajePersonaDetalle>(mensajePersonaDetalle);
                    data.idPersonaEnvia = _session.idPersona;
                    data.idUsuarioRegistro = _session.idUsuario;
                    if (data.idMensajePersona == 0)
                    {
                        MensajePersona mensajePersona = new MensajePersona()
                        {
                            idMedico = data.idMedico,
                            idPersona = _session.idPersona,
                            idUsuarioRegistro = _session.idUsuario
                        };

                        var id = await _unitOfWork.mensajePersonaRepository.Add(con, mensajePersona, trx);
                        data.idMensajePersona = id;
                    }
                    var notificacion = await _unitOfWork.mensajePersonaDetalleRepository.Add(con, data, trx);
                    oResponse.success = true;
                    oResponse.data = data.idMensajePersona;
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                }
                finally
                {
                    if (trx != null)
                        if (oResponse.success) trx.Commit();
                        else trx.Rollback();
                }
            }
            return oResponse;
        }

        public async Task<Response<bool>> UpdateLeido(long ID)
        {
            Response<bool> oResponse = new Response<bool>();
            Session session = _sessionService.GetSession();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();

                    oResponseSQL = await _unitOfWork.mensajePersonaDetalleRepository.UpdateLeido(con, ID, session.idUsuario);
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_INSERT_OK;
                        oResponse.data = true;
                    }
                    else
                    {
                        oResponse.message = Constants._GENERAL_UPDATE_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje);
                        oResponse.data = oResponseSQL.resultado == 2 ? true : false;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = ex.Message;
                }
            }
            return oResponse;
        }

        public async Task<Response<long>> MensajeMedicoInsert(MensajeMedicoDetalleRequest mensajeMedicoDetalle)
        {
            Response<long> oResponse = new Response<long>();
            SqlTransaction trx = null;
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    trx = con.BeginTransaction();
                    var data = _mapper.Map<MensajePersonaDetalle>(mensajeMedicoDetalle);
                    data.idPersonaEnvia = _session.idPersona;
                    data.idUsuarioRegistro = _session.idUsuario;
                    data.idMedico = _session.idMedico;
                    if (data.idMensajePersona == 0)
                    {
                        MensajePersona mensajePersona = new MensajePersona()
                        {
                            idMedico = _session.idMedico,
                            idPersona = mensajeMedicoDetalle.idPersona,
                            idUsuarioRegistro = _session.idUsuario
                        };

                        var id = await _unitOfWork.mensajePersonaRepository.Add(con, mensajePersona, trx);
                        data.idMensajePersona = id;
                    }
                    var notificacion = await _unitOfWork.mensajePersonaDetalleRepository.Add(con, data, trx);
                    oResponse.success = true;
                    oResponse.data = data.idMensajePersona;
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = ex.Message;
                }
                finally
                {
                    if (trx != null)
                        if (oResponse.success) trx.Commit();
                        else trx.Rollback();
                }
            }
            return oResponse;
        }
    }
}

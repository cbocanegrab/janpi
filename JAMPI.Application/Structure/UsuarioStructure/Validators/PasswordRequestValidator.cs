﻿using FluentValidation;
using JAMPI.Application.Contracts.Requests;

namespace JAMPI.Application.Structure.UsuarioStructure.Validators
{
    public class PasswordRequestValidator : AbstractValidator<PasswordRequest>
    {
        public PasswordRequestValidator()
        {
            RuleFor(x => x.strClave1)
                .NotEmpty()
                .Matches("^(([A-Z])(?=.*[a-zA-Z0-9])).{3,16}$").WithMessage("La clave no cumple las condiciones de seguridad")
                .Must((model, field) => field == model.strClave2).WithMessage("Las claves ingresadas no son iguales");
        }
    }
}

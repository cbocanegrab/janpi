﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using JAMPI.Application.ExternalServices.Interfaces;
using JAMPI.Application.Interfaces.Common;
using Newtonsoft.Json;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;

namespace JAMPI.Application.Structure.UsuarioStructure.Services
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUnitOfWork _unitOfWork;
        private string _connectionString;
        private IGoogle _google;
        private readonly IUtilService _util;
        private readonly ITokenService _tokenService;
        private readonly IFacebook _facebook;
        private IConfiguration _configuration;

        public UsuarioService(IConfiguration configuration, IUnitOfWork unitOfWork, IGoogle google, IUtilService util, ITokenService tokenService, IFacebook facebook)
        {
            _unitOfWork = unitOfWork;
            _google = google;
            _connectionString = configuration.GetConnectionString("ConnJAMPI");
            _util = util;
            _tokenService = tokenService;
            _facebook = facebook;
            _configuration = configuration;
        }
        public async Task<Response<string>> AccederAsync(LoginRequest loginRequest)
        {
            Response<string> oResponse = new Response<string>();

            //Consulta con Google
            var result = await _google.Verificar(loginRequest.strToken);
            string strkey = _configuration.GetSection("KeyEncrypt").Value;
            if (result.success)
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    try
                    {
                        await con.OpenAsync();
                        loginRequest.strClave1 = _util.EncryptTripleDES(loginRequest.strClave1, strkey);
                        var usuario = await _unitOfWork.usuarioRepository.validarLogin(con, loginRequest);

                        if (usuario != null)
                        {
                            oResponse.data = _tokenService.GenerateJwtLogin(usuario.idUsuario,usuario.idPersona);
                            oResponse.success = true;
                        }
                        else
                        {
                            oResponse.success = false;
                            oResponse.message = Constants.Login._NO_EXISTE;
                            oResponse.developer = Constants.Login._NO_EXISTE_DEV;
                        }
                    }
                    catch (Exception ex)
                    {
                        oResponse.message = Constants._GENERAL_ERROR;
                        oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    }
                }
            }
            else
            {
                oResponse.message = result.message;
                oResponse.developer = result.developer;
            }
            return oResponse;
        }
        public async Task<Response<long>> ActivarAsync(LoginRequest oLogin)
        {
            Response<long> oResponse = new Response<long>();
            string strkey = _configuration.GetSection("KeyEncrypt").Value;
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();
                    oLogin.strClave1 = _util.EncryptTripleDES(oLogin.strClave1, strkey);
                    oResponseSQL = await _unitOfWork.usuarioRepository.ActivarUsuario(con, oLogin);
                    oResponse.success = false;
                    oResponse.data = oResponseSQL.resultado;
                    if (oResponseSQL.resultado == 1)
                    {
                        oResponse.success = true;
                        oResponse.message = "Se registró la contraseña con éxito";
                    }
                    else
                    {
                        oResponse.message = "El correo o token no son correctos";
                        oResponse.developer = null;
                        oResponse.data = 0;
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<long>> CambiarClaveAsync(PasswordRequest oPassword)
        {
            Response<long> oResponse = new Response<long>();
            string strkey = _configuration.GetSection("KeyEncrypt").Value;
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();
                    oPassword.strClave1 = _util.EncryptTripleDES(oPassword.strClave1, strkey);
                    oPassword.strClaveActual = _util.EncryptTripleDES(oPassword.strClaveActual, strkey);
                    oResponseSQL = await _unitOfWork.usuarioRepository.CambiarClave(con, oPassword);
                    oResponse.success = false;
                    oResponse.data = oResponseSQL.resultado;
                    if (oResponseSQL.resultado == 1)
                    {
                        var msjEmail = $"Estimado(a): <br><br>";
                        msjEmail += $"Su nueva contraseña es: ";
                        msjEmail += $"{oPassword.strClave1}<br><br>";
                        msjEmail += $"Atte.<br>";
                        msjEmail += $"JAMPI";
                        Response<bool> responseMail = await _util.EnviarCorreo(oPassword.strUsuario, "Cambio de contraseña cuenta JAMPI", msjEmail, true);

                        oResponse.success = true;
                        oResponse.message = "Se cambió la contraseña con éxito";
                    }
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = "La contraseña actual no es correcta";
                        oResponse.developer = null;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<int>> RecuperarClaveAsync(string strUsuario)
        {
            Response<int> oResponse = new Response<int>();
            string strkey = _configuration.GetSection("KeyEncrypt").Value;
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();
                    var token = _util.GenerarToken();
                    var saveToken = await _unitOfWork.usuarioRepository.GuardarToken(con, new Usuario { strNombreUsuario = strUsuario, strToken = token });
                    if (saveToken != null && saveToken.resultado > 0)
                    {
                        var data = await _unitOfWork.usuarioRepository.RecuperarClave(con, strUsuario);
                        //string Clave = _util.DecryptTripleDES(data.strClave, strkey);
                        var msjEmail = $"Estimado(a):" + data.strNombre + "<br><br>";
                        msjEmail += $"El token generado para cambiar de clave es: ";
                        msjEmail += $"{token}<br><br>";
                        msjEmail += $"El código estará disponible durante los próximos 15 minutos.<br><br><br><br>";
                        msjEmail += $"Si usted no ha solicitado una nueva clave, por favor omita este correo.<br>";
                        msjEmail += $"Atte.<br>";
                        msjEmail += $"JAMPI";
                        Response<bool> responseMail = await _util.EnviarCorreo(strUsuario, "Recuperar contraseña cuenta JAMPI", msjEmail, true);

                        oResponse.success = true;
                        oResponse.message = Constants.Login.Recovery._SUCCESS_GENERATE_TOKEN;
                    }
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = Constants.Login.Recovery._NOT_USER_EXISTS;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<int>> ValidarTokenAsync(string strUsuario, string strToken)
        {
            Response<int> oResponse = new Response<int>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();
                    var saveToken = await _unitOfWork.usuarioRepository.VerificaToken(con, new Usuario { strNombreUsuario = strUsuario, strToken = strToken });
                    if (saveToken != null && saveToken.resultado > 0)
                    {  
                        oResponse.success = true;
                        oResponse.message = Constants.Login.Recovery._TOKEN_VALID_SUCCESS;
                    }
                    else if(saveToken.resultado == -1)
                    {
                        oResponse.success = false;
                        oResponse.message = Constants.Login.Recovery._NOT_USER_EXISTS;
                    }
                    else if (saveToken.resultado == -2)
                    {
                        oResponse.success = false;
                        oResponse.message = Constants.Login.Recovery._TOKEN_NOT_EXISTS;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
        public async Task<Response<string>> FacebookLoginAsync(string strAccessToken)
        {
            Response<string> response = new Response<string>();
            var validateToken = await _facebook.ValidateAccessTokenTokenAsync(strAccessToken);
            if (validateToken.Data.IsValid)
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    await con.OpenAsync();
                    var session = await _unitOfWork.usuarioRepository.GetLoginExternal(con, validateToken.Data.UserId, Constants.Login.External._FACEBOOK_ID);
                    if (session != null && session.idUsuario > 0)
                    {
                        response.data = _tokenService.GenerateJwtLogin(session.idUsuario,session.idPersona);
                        response.success = true;
                    }
                    else
                    {
                        response.data = "-1";
                        response.success = false;
                        response.message = Constants.Login.External.Facebook._NO_USER_EXISTS;
                    }
                }
                //var userInfo = await _facebook.GetUserInfoAsync(strAccessToken);
                //if (userInfo != null)
                //{

                //}
            }
            else
            {
                response.success = false;
                response.message = Constants.Login.External.Facebook._NO_TOKEN_VALID;
            }

            return response;
        }
        public async Task<Response<bool>> RestableceClaveAsync(RestableceClaveRequest obj)
        {
            Response<bool> oResponse = new Response<bool>();
            string strkey = _configuration.GetSection("KeyEncrypt").Value;
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();
                    obj.strClave1 = _util.EncryptTripleDES(obj.strClave1, strkey);
                    oResponseSQL = await _unitOfWork.usuarioRepository.RestableceClave(con, obj);
                    oResponse.success = false;
                    oResponse.data = false;
                    if (oResponseSQL.mensaje == "")
                    {
                        oResponse.data = true;
                        oResponse.success = true;
                        oResponse.message = "Se restableció la contraseña con éxito";
                    }
                    else
                    {
                        oResponse.success = false;
                        oResponse.message = "No se pudo restablecer la contraseña";
                        oResponse.developer = null;
                    }
                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = ex.Message;
                }
            }
            return oResponse;
        }

    }
}

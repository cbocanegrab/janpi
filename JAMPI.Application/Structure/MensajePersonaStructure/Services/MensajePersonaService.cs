﻿using AutoMapper;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace JAMPI.Application.Structure.NotificacionStructure.Services
{
    public class MensajePersonaService : IMensajePersonaService
    {
        private readonly IConfiguration _configuration;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ISessionService _sessionService;
        private readonly IMapper _mapper;
        private readonly Session _session;
        private string _connectionString;
        public MensajePersonaService(IConfiguration configuration, IUnitOfWork unitOfWork, ISessionService sessionService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _sessionService = sessionService;
            _mapper = mapper;
            _session = _sessionService.GetSession();
            _configuration = configuration;
            _connectionString = configuration.GetConnectionString("ConnJAMPI");
        }
        public async Task<Response<List<MensajePersonaResponse>>> Listar()
        {
            var response = new Response<List<MensajePersonaResponse>>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var notificacion = await _unitOfWork.mensajePersonaRepository.GetAllByPersona(con, _session.idPersona);
                    response.data = _mapper.Map<List<MensajePersonaResponse>>(notificacion);
                    response.success = true;
                }
                catch (Exception ex)
                {
                    response.success = false;
                    response.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                }
                return response;
            }

        }
        public async Task<Response<List<MensajePersonaResponse>>> ListarMensajesMedico()
        {
            var response = new Response<List<MensajePersonaResponse>>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    var lst = await _unitOfWork.mensajePersonaRepository.MensajesMedico(con, _session.idPersona);
                    if (lst != null)
                    {
                        response.data = _mapper.Map<List<MensajePersonaResponse>>(lst);
                        response.success = true;
                    }
                    else
                    {
                        response.success = true;
                        response.message = Constants._GENERAL_GET_NULL;
                    }
                }
                catch (Exception ex)
                {
                    response.success = false;
                    response.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                }
                return response;
            }

        }

    }
}

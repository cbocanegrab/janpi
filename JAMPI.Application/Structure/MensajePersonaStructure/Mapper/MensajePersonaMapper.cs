﻿using AutoMapper;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Structure.NotificacionStructure.Mapper
{
   public  class MensajePersonaMapper : Profile
    {
        public MensajePersonaMapper()
        {
            CreateMap<MensajePersona, MensajePersonaResponse>()
                .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.idMensajePersona)).ReverseMap();
        }
    }
}

﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using JANPI.Infrasturcture.ORM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
namespace JAMPI.Infrastructure.Repositories
{
    public class ArchivoRepository: Base, IArchivoRepository
    {
        public async Task<long> NewID(SqlConnection con)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            return await ExecuteScalarAsync<long>("USP_ARCHIVO_NID", lParametros, con);
        }
        public async Task<int> Add(SqlConnection con, Archivo archivo, Session session)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDARCHIVO", SqlDbType.BigInt, archivo.idArchivo, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXNOMBRE", SqlDbType.VarChar, archivo.strNombre, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXRUTA", SqlDbType.VarChar, archivo.strRuta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXTIPOMIME", SqlDbType.VarChar, archivo.strTipoMime, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXHASH", SqlDbType.VarChar, archivo.strHash, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_DEPESO", SqlDbType.Decimal, archivo.decPeso, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXEXTENSION", SqlDbType.VarChar, archivo.strExtension, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, session.idUsuario, ParameterDirection.Input));
            return await ExecuteNonQueryIntAsync("USP_ARCHIVO_I", lParametros, con);
        }

        public async Task<int> UpdateArchivoTabla(SqlConnection con, long id, long idArchivo, int tipo, Session session)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_ID", SqlDbType.BigInt, id, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDARCHIVO", SqlDbType.BigInt, idArchivo, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_NUTIPO", SqlDbType.Int, tipo, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, session.idUsuario, ParameterDirection.Input));
            return await ExecuteNonQueryIntAsync("USP_ARCHIVO_TABLA_U", lParametros, con);
        }

        public async Task<string> GetFotoByPersona(SqlConnection con, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_ID", SqlDbType.BigInt, id, ParameterDirection.Input));
            return await ExecuteScalarAsync<string>("USP_GETPHOTO_BY_PERSONA", lParametros, con);
        }

        public async Task<string> GetFotoByMedico(SqlConnection con, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_ID", SqlDbType.BigInt, id, ParameterDirection.Input));
            return await ExecuteScalarAsync<string>("USP_GETPHOTO_BY_MEDICO", lParametros, con);
        }
        public async Task<string> GetFotoByUsuario(SqlConnection con, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_ID", SqlDbType.BigInt, id, ParameterDirection.Input));
            return await ExecuteScalarAsync<string>("USP_GETPHOTO_BY_USUARIO", lParametros, con);
        }
        public async Task<string> GetFirmaMedico(SqlConnection con, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_ID", SqlDbType.BigInt, id, ParameterDirection.Input));
            return await ExecuteScalarAsync<string>("USP_GETSIGN_BY_MEDICO", lParametros, con);
        }
    }
}

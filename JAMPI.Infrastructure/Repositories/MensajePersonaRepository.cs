﻿using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using JANPI.Infrasturcture.ORM;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Infrastructure.Repositories
{
    public class MensajePersonaRepository : Base, IMensajePersonaRepository
    {
        public async Task<long> Add(SqlConnection con, MensajePersona entity, SqlTransaction trx)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, entity.idPersona, ParameterDirection.Input, 0));
            lParametros.Add(new ParametroSQL("@PI_IDMEDICO", SqlDbType.BigInt, entity.idMedico, ParameterDirection.Input, 100));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, entity.idUsuarioRegistro, ParameterDirection.Input));
            return await ExecuteScalarAsync<long>("USP_MENSAJES_I", lParametros, con, trx);
        }

        public async Task<List<MensajePersona>> GetAllByPersona(SqlConnection con, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, id, ParameterDirection.Input, 0));
            var drd =await ExecuteReaderAsync("USP_MENSAJES_L", lParametros, con);
            return ListarObjeto<MensajePersona>(drd);
        }
        public async Task<List<MensajePersona>> MensajesMedico(SqlConnection con, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, id, ParameterDirection.Input, 0));
            var drd = await ExecuteReaderAsync("USP_MENSAJES_MEDICO_L", lParametros, con);
            return ListarObjeto<MensajePersona>(drd);
        }
    }
}

﻿
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using JANPI.Infrasturcture.ORM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Infrastructure.Repositories
{
    public class ConsultaRepository : Base, IConsultaRepository
    {
        //private readonly long _userID;
        public ConsultaRepository()//long userID)
        {
            //_userID = userID;
        }
        public async Task<ResponseSQL> AddAsync(SqlConnection con, ConsultaBasicRequest entity, Session session)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, entity.idPersona, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDMEDICOESPECIALIDAD", SqlDbType.BigInt, entity.idMedicoEspecialidad, ParameterDirection.Input, 100));
            lParametros.Add(new ParametroSQL("@PI_FECONSULTA", SqlDbType.VarChar, entity.strFechaConsulta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXHORA", SqlDbType.VarChar, entity.strHora, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMTIPOCONSULTA", SqlDbType.BigInt, entity.idParamTipoConsulta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTAPADRE", SqlDbType.BigInt, entity.idConsultaPadre, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_DECDESCUENTO", SqlDbType.Decimal, entity.deDescuento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXMOTIVO", SqlDbType.VarChar, entity.strMotivo, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTADERIVADA", SqlDbType.BigInt, entity.idConsultaDerivada, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, session.idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            return await ExecuteNonQueryAsync("USP_CONSULTA_INSERT", lParametros, con);
        }
        public async Task<ResponseSQL> Delete(SqlConnection con, long id)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, id, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, 1, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            return await ExecuteNonQueryAsync("USP_CONSULTA_DELETE", lParametros, con);

        }
        public Task<Consulta> Get(SqlConnection con, long id)
        {
            throw new NotImplementedException();
        }
        public Task<List<Consulta>> GetAll(SqlConnection con)
        {
            throw new NotImplementedException();
        }
        public Task<ResponseSQL> Update(SqlConnection con, Consulta entity)
        {
            throw new NotImplementedException();
        }
        public async Task<ResponseSQL> UpdateTriaje(SqlConnection con, ConsultaTriajeRequest entity)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, entity.idConsulta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_DEPESO", SqlDbType.Decimal, entity.decPeso, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_DEOXIGENACION", SqlDbType.Decimal, entity.decOxigenacion, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_DEPRESION", SqlDbType.Decimal, entity.decPresion, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_DETALLA", SqlDbType.Decimal, entity.decTalla, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_DETEMPERATURA", SqlDbType.Decimal, entity.decTemperatura, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, 1, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            return await ExecuteNonQueryAsync("USP_CONSULTA_UPDATE_TRIAJE", lParametros, con);
        }
        public async Task<ResponseSQL> Programar(SqlConnection con, ConsultaProgramaRequest entity)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, entity.idConsulta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXTRANSACCION", SqlDbType.Decimal, entity.strTransaccion, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, 1, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            return await ExecuteNonQueryAsync("USP_CONSULTA_UPDATE_TRIAJE", lParametros, con);
        }
        public async Task<ResponseSQL> CancelarPorPaciente(SqlConnection con, ConsultaCancelaRequest entity, long idUsuario)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, entity.idConsulta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMMOTIVOCANCELACION", SqlDbType.BigInt, entity.idParamMotivoCancelacion, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            return await ExecuteNonQueryAsync("USP_CONSULTA_CANCEL_CITA_P", lParametros, con);
        }
        public async Task<ResponseSQL> CancelarPorMedico(SqlConnection con, ConsultaCancelaRequest entity)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, entity.idConsulta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMMOTIVOCANCELACION", SqlDbType.BigInt, entity.idParamMotivoCancelacion, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            return await ExecuteNonQueryAsync("USP_CONSULTA_CANCEL_CITA_M", lParametros, con);
        }
        public async Task<ResponseSQL> ActualizarURL(SqlConnection con, ConsultaURLRequest entity)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, entity.idConsulta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMPLATAFORMA", SqlDbType.BigInt, entity.idParamPlataforma, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXURL", SqlDbType.VarChar, entity.strURL, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, 1, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            return await ExecuteNonQueryAsync("USP_CONSULTA_UPDATE_URL", lParametros, con);
        }
        public async Task<ConsultaResumenResponse> ProximaConsulta(SqlConnection con,long idPersonaMedico, long idPersona)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, idPersona, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA_MEDICO", SqlDbType.BigInt, idPersonaMedico, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_CONSULTA_PROXIMA", lParametros, con);
            ConsultaResumenResponse obj = ObtenerObjeto<ConsultaResumenResponse>(drd);
            drd.Close();
            return obj;
        }
        public async Task<ConsultaResumenResponse> UltimaConsulta(SqlConnection con, long idPersona)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, idPersona, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_CONSULTA_ULTIMA", lParametros, con);
            ConsultaResumenResponse obj = ObtenerObjeto<ConsultaResumenResponse>(drd);
            drd.Close();
            return obj;
        }
        public async Task<ResponseSQL> ProcesarPago(SqlConnection con, Pago entity, long idUsuario)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, entity.idConsulta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXTRANSACCION", SqlDbType.VarChar, entity.strTransaccion, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FETRANSACCION", SqlDbType.VarChar, entity.strFechaTransaccion, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_DEPAGO", SqlDbType.Decimal, entity.decPago, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMTIPOTARJETA", SqlDbType.BigInt, entity.idParamTipoTarjeta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXTARJETA", SqlDbType.VarChar, entity.strTarjeta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FGUSOCREDITO", SqlDbType.VarChar, entity.strUsoCredito, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_DECREDITOINI", SqlDbType.Decimal, entity.decCreditoIni, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_DECREDITOFIN", SqlDbType.Decimal, entity.decCreditoFin, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPAGOPADRE", SqlDbType.BigInt, entity.idPagoPadre, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMTIPOTRANSACCION", SqlDbType.VarChar, entity.idParamTipoTransaccion, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            return await ExecuteNonQueryAsync("USP_PAGO_I", lParametros, con);
        }
        public async Task<Consulta> LinkConsulta(SqlConnection con, long idConsulta)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, idConsulta, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_CONSULTA_URL", lParametros, con);
            Consulta obj = ObtenerObjeto<Consulta>(drd);
            drd.Close();
            return obj;
        }
        public async Task<ResponseSQL> ValorarConsulta(SqlConnection con, ConsultaValoracionRequest entity, long idUsuario)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, entity.idConsulta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_DEVALOR", SqlDbType.Decimal, entity.deValor, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMTIPOVALOR", SqlDbType.BigInt, entity.idParamTipoValor, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCOMENTARIO", SqlDbType.VarChar, entity.strComentario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            return await ExecuteNonQueryAsync("USP_CONSULTA_VALORACION", lParametros, con);
        }
        public async Task<List<OrdenResponse>> Ordenes(SqlConnection con, long idPersona)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, idPersona, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_ORDENES_LIST", lParametros, con);
            return ListarObjeto<OrdenResponse>(drd);
        }
        public async Task<ConsultaDetalleResponse> ConsultaDetalle(SqlConnection con, long idConsulta)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, idConsulta, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_CONSULTA_DETALLE_L", lParametros, con);
            ConsultaDetalleResponse obj = ObtenerObjeto<ConsultaDetalleResponse>(drd);
            drd.Close();
            return obj;
        }
        public async Task<ResponseSQL> SeguimientoInsert(SqlConnection con, SeguimientoRequest entity,long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, entity.idConsulta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMSEGUIMIENTO", SqlDbType.BigInt, entity.idParamSeguimiento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXDESCRIPCION", SqlDbType.VarChar, entity.strDescripcion, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_NUVALORACION", SqlDbType.Int, entity.intValoracion, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_SEGUIMIENTO_I", lParametros, con);

            return rpta;
        }
        public async Task<List<ConsultaSeguimiento>> SeguiminetoListar(SqlConnection con, long idConsulta)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, idConsulta, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_SEGUIMIENTO_L", lParametros, con);
            return ListarObjeto<ConsultaSeguimiento>(drd);
        }
        public async Task<List<Consulta>> ConsultaProgramada(SqlConnection con, long idPersonaMedico, long idPersona)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, idPersona, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA_MEDICO", SqlDbType.BigInt, idPersonaMedico, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_CONSULTA_PROGRAMADA_L", lParametros, con);
            return ListarObjeto<Consulta>(drd);
        }
        public async Task<List<Consulta>> ConsultaHistorico(SqlConnection con, long idPersonaMedico,long idPersona,long idEspecialidad, long idEstadoTratamiento)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, idPersona, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPERSONAMEDICO", SqlDbType.BigInt, idPersonaMedico, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDESTADOTRATAMIENTO", SqlDbType.BigInt, idEstadoTratamiento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDESPECIALIDAD", SqlDbType.BigInt, idEspecialidad, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_CONSULTA_HIST", lParametros, con);
            return ListarObjeto<Consulta>(drd);
        }
        public async Task<ResponseSQL> ReconsultaInsertar(SqlConnection con, ReconsultaRequest entity, long idUsuario)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, entity.idConsulta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FECONSULTA", SqlDbType.VarChar, entity.strFechaConsulta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXHORA", SqlDbType.VarChar, entity.strHora, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXMOTIVO", SqlDbType.VarChar, entity.strMotivo, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_DEDESCUENTO", SqlDbType.Decimal, entity.decDescuento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            return await ExecuteNonQueryAsync("USP_RECOSULTA_I", lParametros, con);
        }
        public async Task<ResponseSQL> ConsultaDerivadaInsertar(SqlConnection con, ConsultaDerivadaRequest entity, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, entity.idConsulta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDESPECIALIDAD", SqlDbType.BigInt, entity.idEspecialidad, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXMOTIVO", SqlDbType.VarChar, entity.strMotivo, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_CONSULTA_DERIVADA_I", lParametros, con);

            return rpta;
        }
        public async Task<ResponseSQL> ConsultaDerivadaEliminar(SqlConnection con, long idConsultaDerivada, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTADERIVADA", SqlDbType.BigInt, idConsultaDerivada, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_CONSULTA_DERIVADA_D", lParametros, con);

            return rpta;
        }
        public async Task<ConsultaDerivadaResponse> ConsultaDerivadaObtener(SqlConnection con, long idPersona)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, idPersona, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_CONSULTA_DERIVADA_L", lParametros, con);
            ConsultaDerivadaResponse obj = ObtenerObjeto<ConsultaDerivadaResponse>(drd);
            drd.Close();
            return obj;
        }
        public async Task<ConsultaSeguimientoResponse> ConsultaSeguimiento(SqlConnection con, long idConsulta)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, idConsulta, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_CONSULTA_SEGUIMIENTO_L", lParametros, con);
            ConsultaSeguimientoResponse ConsultaSeguimiento = new ConsultaSeguimientoResponse();
            ConsultaSeguimiento.oTratamientoConsulta = ObtenerObjeto<TratamientoConsulta>(drd);
            await drd.NextResultAsync();
            ConsultaSeguimiento.lSeguimiento = ListarObjeto<ConsultaSeguimiento>(drd);
            await drd.NextResultAsync();
            drd.Close();
            return ConsultaSeguimiento;
        }
        public async Task<ConsultaProgramacionResponse> ConsultaProgramacion(SqlConnection con, long idMedico,long idPersona)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICO", SqlDbType.BigInt, idMedico, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, idPersona, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_CONSULTA_RESUMEN", lParametros, con);
            ConsultaProgramacionResponse obj = new ConsultaProgramacionResponse();
            obj.oConsultaCercana = ObtenerObjeto<ConsultaProgMin>(drd);
            await drd.NextResultAsync();
            obj.lConsultaHoy = ListarObjeto<ConsultaProgMin>(drd);
            await drd.NextResultAsync();
            obj.oConsultaProxima = ObtenerObjeto<ConsultaProgMin>(drd);
            await drd.NextResultAsync();
            obj.oConsultaUltima = ObtenerObjeto<ConsultaProgMin>(drd);
            await drd.NextResultAsync();
            drd.Close();
            return obj;
        }
        public async Task<List<ConsultaProgMin>> ConsultaPorFecha(SqlConnection con, string strFecha, long idMedico, long idPersona)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICO", SqlDbType.BigInt, idMedico, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, idPersona, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FECONSULTA", SqlDbType.VarChar, strFecha, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_CONSULTA_X_FECHA", lParametros, con);
            return ListarObjeto<ConsultaProgMin>(drd);
       
        }
    }
}

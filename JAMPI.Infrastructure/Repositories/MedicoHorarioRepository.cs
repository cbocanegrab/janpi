﻿using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using JANPI.Infrasturcture.ORM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Infrastructure.Repositories
{
    public class MedicoHorarioRepository: Base, IMedicoHorarioRepository
    {
        public async Task<ResponseSQL> Add(SqlConnection con, MedicoHorario entity)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICO", SqlDbType.BigInt, entity.idMedico, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDESPECIALIDAD", SqlDbType.BigInt, entity.idEspecialidad, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FEINICIO", SqlDbType.VarChar, entity.strFechaInicio, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXHORAINICIO", SqlDbType.VarChar, entity.strHoraInicio, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXHORAFIN", SqlDbType.VarChar, entity.strHoraFin, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, entity.idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_MEDICOHORARIO_I", lParametros, con);

            return rpta;
        }

        public async Task<ResponseSQL> Update(SqlConnection con, MedicoHorario entity)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICOHORARIO", SqlDbType.BigInt, entity.idMedicoHorario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXHORAINICIO", SqlDbType.VarChar, entity.strHoraInicio, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXHORAFIN", SqlDbType.VarChar, entity.strHoraFin, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, entity.idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            rpta = await ExecuteNonQueryAsync("USP_MEDICOHORARIO_U", lParametros, con);

            return rpta;
        }

        public async Task<ResponseSQL> Delete(SqlConnection con, MedicoHorario entity)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICOHORARIO", SqlDbType.BigInt, entity.idMedicoHorario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, entity.idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            rpta = await ExecuteNonQueryAsync("USP_MEDICOHORARIO_D", lParametros, con);

            return rpta;
        }

        public async Task<MedicoHorarioResponse> GetMedicoHorario(SqlConnection con, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICOHORARIO", SqlDbType.BigInt, id, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDMEDICOESPECIALIDAD", SqlDbType.BigInt, 0, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_MEDICOHORARIO_L", lParametros, con);
            MedicoHorarioResponse oEntidad = ObtenerObjeto<MedicoHorarioResponse>(drd);
            drd.Close();
            return oEntidad;
        }

        public async Task<List<MedicoHorarioResponse>> GetAllMedicoHorario(SqlConnection con,long idMedicoEspecialidad)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICOHORARIO", SqlDbType.BigInt, 0, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDMEDICOESPECIALIDAD", SqlDbType.BigInt, idMedicoEspecialidad, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_MEDICOHORARIO_L", lParametros, con);
            return ListarObjeto<MedicoHorarioResponse>(drd);
        }
        public Task<List<MedicoHorario>> GetAll(SqlConnection con)
        {
            throw new NotImplementedException();
        }
        public Task<MedicoHorario> Get(SqlConnection con,long id)
        {
            throw new NotImplementedException();
        }
    }
}

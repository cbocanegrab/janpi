﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using JANPI.Infrasturcture.ORM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Infrastructure.Repositories
{
    public class HistoriaClinicaRepository : Base, IHistoriaClinicaRepository
    {
        public async Task<ResponseSQL> HistoriaClinicaInsertar(SqlConnection con, HistoriaClinicaIRequest entity, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, entity.idPersona, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXANTECEDENTEFAMILIAR", SqlDbType.VarChar, entity.strAntecedenteFamiliar, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXANTECEDENTEMEDICO", SqlDbType.VarChar, entity.strAntecedenteMedico, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXANTECEDENTEQUIRURGICO", SqlDbType.VarChar, entity.strAntecedenteQuirurgico, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXANTECEDENTETRAUMATICO", SqlDbType.VarChar, entity.strAntecedenteTraumatico, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXANTECEDENTEALERGICO", SqlDbType.VarChar, entity.strAntecedenteAlergico, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXANTECEDENTEVICIOMANIA", SqlDbType.VarChar, entity.strAntecedenteViciomania, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_HISTORIA_CLINICA_I", lParametros, con);

            return rpta;
        }
        public async Task<ResponseSQL> HistoriaClinicaActualizar(SqlConnection con, HistoriaClinicaURequest entity, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDHISTORIACLINICA", SqlDbType.BigInt, entity.idHistoriaClinica, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXANTECEDENTEFAMILIAR", SqlDbType.VarChar, entity.strAntecedenteFamiliar, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXANTECEDENTEMEDICO", SqlDbType.VarChar, entity.strAntecedenteMedico, ParameterDirection.Input)); 
            lParametros.Add(new ParametroSQL("@PI_TXANTECEDENTEQUIRURGICO", SqlDbType.VarChar, entity.strAntecedenteQuirurgico, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXANTECEDENTETRAUMATICO", SqlDbType.VarChar, entity.strAntecedenteTraumatico, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXANTECEDENTEALERGICO", SqlDbType.VarChar, entity.strAntecedenteAlergico, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXANTECEDENTEVICIOMANIA", SqlDbType.VarChar, entity.strAntecedenteViciomania, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_HISTORIA_CLINICA_U", lParametros, con);

            return rpta;
        }

        public async Task<List<HistoriaClinica>> HistoriaClinicaListar(SqlConnection con, long idPersona)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, idPersona, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_HISTORIA_CLINICA_L", lParametros, con);
            List<HistoriaClinica> lst = ListarObjeto<HistoriaClinica>(drd);
            drd.Close();
            return lst;
        }
    }
}

﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using JANPI.Infrasturcture.ORM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Infrastructure.Repositories
{
    public class RecetaRepository:Base,IRecetaRepository
    {
        public async Task<ResponseSQL> InsertReceta(SqlConnection con, RecetaIRequest entity,long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, entity.idConsulta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FERECETA", SqlDbType.VarChar, entity.strFechaReceta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FECADUCIDAD", SqlDbType.VarChar, entity.strFechaCaducidad, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCOMENTARIO", SqlDbType.VarChar, entity.strComentario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_RECETA_I", lParametros, con);

            return rpta;
        }

        public async Task<ResponseSQL> UpdateReceta(SqlConnection con, RecetaURequest entity, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDRECETA", SqlDbType.BigInt, entity.idReceta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FERECETA", SqlDbType.VarChar, entity.strFechaReceta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FECADUCIDAD", SqlDbType.VarChar, entity.strFechaCaducidad, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCOMENTARIO", SqlDbType.VarChar, entity.strComentario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_RECETA_U", lParametros, con);

            return rpta;
        }

        public async Task<ResponseSQL> DeleteReceta(SqlConnection con, long idReceta, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDRECETA", SqlDbType.BigInt, idReceta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_RECETA_D", lParametros, con);

            return rpta;
        }
        public async Task<RecetaResponse> GetReceta(SqlConnection con, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDRECETA", SqlDbType.BigInt, id, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, 0, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_RECETA_L", lParametros, con);
            RecetaResponse oEntidad = ObtenerObjeto<RecetaResponse>(drd);
            drd.Close();
            return oEntidad;
        }

        public async Task<List<RecetaResponse>> GetAllReceta(SqlConnection con, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDRECETA", SqlDbType.BigInt, 0, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, id, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_RECETA_L", lParametros, con);
            return ListarObjeto<RecetaResponse>(drd);
        }
        public async Task<ResponseSQL> InsertRecetaMedicamento(SqlConnection con, RecetaMedicamentoIRequest entity, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDRECETA", SqlDbType.BigInt, entity.idReceta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXDOSIS", SqlDbType.VarChar, entity.strDosis, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDMEDICAMENTO", SqlDbType.BigInt, entity.idMedicamento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMUNIDADMEDIDA", SqlDbType.BigInt, entity.idParamUnidadMedida, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FEINICIO", SqlDbType.VarChar, entity.strFechaInicio, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_NUDIAS", SqlDbType.Int, entity.intDias, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCOMENTARIOMEDICAMENTO", SqlDbType.VarChar, entity.strComentarioMedicamento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMVIAADMINISTRACION", SqlDbType.BigInt, entity.idParamViaAdministracion, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXPRESENTACION", SqlDbType.VarChar, entity.strPresentacion, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_RECETA_MEDICAMENTO_I", lParametros, con);

            return rpta;
        }

        public async Task<ResponseSQL> UpdateRecetaMedicamento(SqlConnection con, RecetaMedicamentoURequest entity, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDRECETAMEDICAMENTO", SqlDbType.BigInt, entity.idRecetaMedicamento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXDOSIS", SqlDbType.VarChar, entity.strDosis, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDMEDICAMENTO", SqlDbType.BigInt, entity.idMedicamento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMUNIDADMEDIDA", SqlDbType.BigInt, entity.idParamUnidadMedida, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FEINICIO", SqlDbType.VarChar, entity.strFechaInicio, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_NUDIAS", SqlDbType.Int, entity.intDias, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCOMENTARIOMEDICAMENTO", SqlDbType.VarChar, entity.strComentarioMedicamento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMVIAADMINISTRACION", SqlDbType.BigInt, entity.idParamViaAdministracion, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXPRESENTACION", SqlDbType.VarChar, entity.strPresentacion, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_RECETA_MEDICAMENTO_U", lParametros, con);

            return rpta;
        }

        public async Task<ResponseSQL> DeleteRecetaMedicamento(SqlConnection con, long idRecetaMedicamento, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDRECETAMEDICAMENTO", SqlDbType.BigInt, idRecetaMedicamento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_RECETA_MEDICAMENTO_D", lParametros, con);

            return rpta;
        }
        public async Task<RecetaMedicamentoResponse> GetRecetaMedicamento(SqlConnection con, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDRECETA", SqlDbType.BigInt, id, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDRECETAMEDICAMENTO", SqlDbType.BigInt, 0, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_RECETA_MEDICAMENTO_L", lParametros, con);
            RecetaMedicamentoResponse oEntidad = ObtenerObjeto<RecetaMedicamentoResponse>(drd);
            drd.Close();
            return oEntidad;
        }

        public async Task<List<RecetaMedicamentoResponse>> GetAllRecetaMedicamento(SqlConnection con, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDRECETA", SqlDbType.BigInt, id, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDRECETAMEDICAMENTO", SqlDbType.BigInt, 0, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_RECETA_MEDICAMENTO_L", lParametros, con);
            var lst= ListarObjeto<RecetaMedicamentoResponse>(drd);
            drd.Close();
            return lst;
        }
        public async Task<ResponseSQL> InsertMedicamentoFrecuencia(SqlConnection con,MedicamentoHorarioIRequest entidad, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDRECETAMEDICAMENTO", SqlDbType.BigInt, entidad.idRecetaMedicamento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXHORA", SqlDbType.VarChar, entidad.strHora, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_MEDICAMENTO_FRECUENCIA_I", lParametros, con);

            return rpta;
        }

        public async Task<ResponseSQL> UpdateMedicamentoFrecuencia(SqlConnection con, MedicamentoHorarioURequest entity, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICAMENTOFRECUENCIA", SqlDbType.BigInt, entity.idMedicamentoFrecuencia, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXHORA", SqlDbType.VarChar, entity.strHora, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FGESTADO", SqlDbType.Char, entity.strEstado, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_MEDICAMENTO_FRECUENCIA_U", lParametros, con);

            return rpta;
        }

        public async Task<ResponseSQL> DeleteMedicamentoFrecuencia(SqlConnection con, long idMedicamentoFrecuencia, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICAMENTOFRECUENCIA", SqlDbType.BigInt, idMedicamentoFrecuencia, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_MEDICAMENTO_FRECUENCIA_D", lParametros, con);

            return rpta;
        }
        public async Task<MedicamentoHorario> GetMedicamentoFrecuencia(SqlConnection con, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICAMENTOFRECUENCIA", SqlDbType.BigInt, id, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDRECETAMEDICAMENTO", SqlDbType.BigInt, 0, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_MEDICAMENTO_FRECUENCIA_L", lParametros, con);
            MedicamentoHorario oEntidad = ObtenerObjeto<MedicamentoHorario>(drd);
            drd.Close();
            return oEntidad;
        }

        public async Task<List<MedicamentoHorario>> GetAllMedicamentoFrecuencia(SqlConnection con, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICAMENTOFRECUENCIA", SqlDbType.BigInt, 0, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDRECETAMEDICAMENTO", SqlDbType.BigInt, id, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_MEDICAMENTO_FRECUENCIA_L", lParametros, con);
            return ListarObjeto<MedicamentoHorario>(drd);
        }
        public async Task<List<TratamientoResponse>> TratamientoActual(SqlConnection con, long idPersona)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, idPersona, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_TRATAMIENTO_ACTUAL", lParametros, con);
            List <TratamientoResponse> lst = ListarObjeto<TratamientoResponse>(drd);
            drd.Close();
            return lst;
        }
        public async Task<List<TratamientoResponse>> TratamientoHistorico(SqlConnection con, long idPersona, long idMedico, long idEspecialidad, long idEstado)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, idPersona, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDMEDICO", SqlDbType.BigInt, idMedico, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDESPECIALIDAD", SqlDbType.BigInt, idEspecialidad, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDESTADOTRATAMIENTO", SqlDbType.BigInt, idEstado, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_TRATAMIENTO_HIST", lParametros, con);
            return ListarObjeto<TratamientoResponse>(drd);
        }
        public async Task<List<TratamientoPorFechaResponse>> TratamientoPorFecha(SqlConnection con, string strFecha,long idUsuario, long idConsulta)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, idConsulta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FERECETA", SqlDbType.VarChar, strFecha, ParameterDirection.Input));
            
            SqlDataReader drd = await ExecuteReaderAsync("[USP_TRATAMIENTO_BYFECHA]", lParametros, con);
            List<TratamientoPorFechaResponse> lst = ListarObjeto<TratamientoPorFechaResponse>(drd);
            drd.Close();
            return lst;
        }

        public async Task<TratamientoSeguimientoResponse>TratamientoSeguimiento(SqlConnection con, long idConsulta)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, idConsulta, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_TRATAMIENTO_SEGUIMIENTO", lParametros, con);
            TratamientoSeguimientoResponse oEntidad = ObtenerObjeto<TratamientoSeguimientoResponse>(drd);
            drd.Close();
            return oEntidad;
        }

        public async Task<List<TratamientoSeguimientoResponse>> TratamientoSeguimientoMedico(SqlConnection con, long idPersona)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, idPersona, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_TRATAMIENTO_SEGUIMIENTO_MED", lParametros, con);
            List<TratamientoSeguimientoResponse> lst = ListarObjeto<TratamientoSeguimientoResponse>(drd);
            drd.Close();
            return lst;
        }
        public async Task<List<Tratamiento>> TratamientoEnCursoByPaciente(SqlConnection con, long idPersona)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, idPersona, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_TRATAMIENTO_ENCURSO_BYPACIENTE", lParametros, con);
            List<Tratamiento> lst = ListarObjeto<Tratamiento>(drd);
            drd.Close();
            return lst;
        }
        public async Task<RecetaResumenResponse> RecetaResumen(SqlConnection con, long idConsulta)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, idConsulta, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_RECETA_RESUMEN", lParametros, con);
            RecetaResumenResponse oRecetaResumenResponse = new RecetaResumenResponse();
            oRecetaResumenResponse.oPersona = ObtenerObjeto<PersonaMin>(drd);
            await drd.NextResultAsync();
            oRecetaResumenResponse.lRecetaMedicamento = ListarObjeto<RecetaMedicamentoMin>(drd);
            await drd.NextResultAsync();
            drd.Close();
            return oRecetaResumenResponse;
        }
        public async Task<TratamientoResumenResponse> TratamientoResumen(SqlConnection con, long idConsulta)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, idConsulta, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_TRATAMIENTO_RESUMEN", lParametros, con);
            TratamientoResumenResponse oTratamientoResumenResponse = new TratamientoResumenResponse();
            oTratamientoResumenResponse.oPersona = ObtenerObjeto<PersonaMin>(drd);
            await drd.NextResultAsync();
            oTratamientoResumenResponse.lTratamiento = ListarObjeto<TratamientoMin>(drd);
            await drd.NextResultAsync();
            drd.Close();
            return oTratamientoResumenResponse;
        }

    }
}

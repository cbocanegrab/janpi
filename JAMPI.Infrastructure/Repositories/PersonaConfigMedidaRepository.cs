﻿using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using JANPI.Infrasturcture.ORM;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace JAMPI.Infrastructure.Repositories
{
    public class PersonaConfigMedidaRepository : Base, IPersonaConfigMedidaRepository
    {
        public async Task<long> Add(SqlConnection con, ConfigMedida entity, SqlTransaction trx)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, entity.idPersona, ParameterDirection.Input, 0));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMTIPOMEDIDA", SqlDbType.BigInt, entity.idParamTipoMedida, ParameterDirection.Input, 18));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, entity.idUsuarioRegistro, ParameterDirection.Input));
            return await ExecuteScalarAsync<long>("USP_PERSONA_CONFIG_I", lParametros, con, trx);
        }

        public async Task<int> Delete(SqlConnection con,long id, Session session)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONACONFIGMEDIDA", SqlDbType.BigInt, id, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, session.idUsuario, ParameterDirection.Input));
            return await ExecuteNonQueryIntAsync("USP_PERSONA_CONFIG_D", lParametros, con);
        }
        public async Task<List<ConfigMedida>> GetAllByPersona(SqlConnection con, Session session)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, session.idPersona, ParameterDirection.Input));
            var drd = await ExecuteReaderAsync("USP_PERSONA_CONFIG_L", lParametros, con);
            return ListarObjeto<ConfigMedida>(drd);
        }

    }
}

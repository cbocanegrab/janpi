﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using JANPI.Infrasturcture.ORM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Infrastructure.Repositories
{
    public class MedicoEstudioRepository : Base, IMedicoEstudioRepository
    {
        public async Task<ResponseSQL> MedicoEstudioInsertar(SqlConnection con, MedicoEstudioIRequest entity, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_TXCENTROESTUDIO", SqlDbType.VarChar, entity.strCentroEstudio, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCARRERA", SqlDbType.VarChar, entity.strCarrera, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_MEDICO_ESTUDIO_I", lParametros, con);

            return rpta;
        }
        public async Task<ResponseSQL> MedicoEstudioActualizar(SqlConnection con, MedicoEstudioURequest entity, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICOESTUDIO", SqlDbType.BigInt, entity.idMedicoEstudio, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCENTROESTUDIO", SqlDbType.VarChar, entity.strCentroEstudio, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCARRERA", SqlDbType.VarChar, entity.strCarrera, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_MEDICO_ESTUDIO_U", lParametros, con);

            return rpta;
        }

        public async Task<ResponseSQL> MedicoEstudioEliminar(SqlConnection con, long idMedicoEstudio, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICOESTUDIO", SqlDbType.BigInt, idMedicoEstudio, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_MEDICO_Estudio_D", lParametros, con);

            return rpta;
        }
        public async Task<List<MedicoEstudio>> GetAllMedicoEstudio(SqlConnection con, long idMedico,long idPersona)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICOESTUDIO", SqlDbType.BigInt, 0, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDMEDICO", SqlDbType.BigInt, idMedico, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, idPersona, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_MEDICO_ESTUDIO_L", lParametros, con);
            List<MedicoEstudio> lst = ListarObjeto<MedicoEstudio>(drd);
            drd.Close();
            return lst;
        }
    }
}

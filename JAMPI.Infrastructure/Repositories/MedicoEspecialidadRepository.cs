﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using JANPI.Infrasturcture.ORM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
namespace JAMPI.Infrastructure.Repositories
{
    public class MedicoEspecialidadRepository : Base, IMedicoEspecialidadRepository
    {

        public async Task<MedicoEspecialidad> MedicoEspecialidadDetalle(SqlConnection con, long idMedicoEspecialidad)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICOESPECIALIDAD", SqlDbType.BigInt, idMedicoEspecialidad, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_MEDICO_ESPECIALIDAD_DETALLE", lParametros, con);
            MedicoEspecialidad oEntidad = ObtenerObjeto<MedicoEspecialidad>(drd);
            drd.Close();
            return oEntidad;
        }
        public async Task<List<ComentarioResponse>> GetAllComentarios(SqlConnection con, long idMedicoEspecialidad)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICOESPECIALIDAD", SqlDbType.BigInt, idMedicoEspecialidad, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_MEDICO_COMENTARIOS", lParametros, con);
            List<ComentarioResponse> lst = ListarObjeto<ComentarioResponse>(drd);
            drd.Close();
            return lst;
        }

        public async Task<ResponseSQL> MedicoEspecialidadInsertar(SqlConnection con, MedicoEspecialidadIRequest entity, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICO", SqlDbType.BigInt, entity.idMedico, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDESPECIALIDAD", SqlDbType.BigInt, entity.idEspecialidad, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_MEDICO_ESPECIALIDAD_I", lParametros, con);

            return rpta;
        }

        public async Task<ResponseSQL> MedicoEspecialidadActualizar(SqlConnection con, MedicoEspecialidadURequest entity, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICOESPECIALIDAD", SqlDbType.BigInt, entity.idMedicoEspecialidad, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDESPECIALIDAD", SqlDbType.BigInt, entity.idEspecialidad, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_MEDICO_ESPECIALIDAD_U", lParametros, con);

            return rpta;
        }

        public async Task<ResponseSQL> MedicoEspecialidadEliminar(SqlConnection con, long idMedicoEspecialidad, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICOESPECIALIDAD", SqlDbType.BigInt, idMedicoEspecialidad, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_MEDICO_ESPECIALIDAD_D", lParametros, con);

            return rpta;
        }

        public async Task<List<MedicoEspecialidadResponse>> MedicoEspecialidadListar(SqlConnection con, long idMedico, long idMedicoEspecialidad)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICO", SqlDbType.BigInt, idMedico, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDMEDICOESPECIALIDAD", SqlDbType.BigInt, idMedicoEspecialidad, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_MEDICO_ESPECIALIDAD_L", lParametros, con);
            List<MedicoEspecialidadResponse> lst = ListarObjeto<MedicoEspecialidadResponse>(drd);
            drd.Close();
            return lst;
        }
    }
}

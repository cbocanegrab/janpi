﻿using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using JANPI.Infrasturcture.ORM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Infrastructure.Repositories
{
    public class PersonaMedidaRepository : Base, IPersonaMedidaRepository
    {
        public async Task<int> Add(SqlConnection con, PersonaMedida entity)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, entity.idPersona, ParameterDirection.Input, 0));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMTIPOMEDIDA", SqlDbType.BigInt, entity.idParamTipoMedida, ParameterDirection.Input, 0));
            lParametros.Add(new ParametroSQL("@PI_TXVALOR", SqlDbType.VarChar, entity.strValor, ParameterDirection.Input, 18));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, entity.idUsuarioRegistro, ParameterDirection.Input));
            return await ExecuteNonQueryIntAsync("USP_PERSONA_MEDIDA_I", lParametros, con);
        }

        public async Task<int> Delete(SqlConnection con, PersonaMedida entity)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONAMEDIDA", SqlDbType.BigInt, entity.idPersonaMedida, ParameterDirection.Input, 0));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, entity.idUsuarioModifica, ParameterDirection.Input));
            return await ExecuteNonQueryIntAsync("USP_PERSONA_MEDIDA_D", lParametros, con);
        }
        public async Task<List<PersonaMedida>> GetAllByPersona(SqlConnection con, long idParamTipoMedida, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPARAMTIPOMEDIDA", SqlDbType.BigInt, idParamTipoMedida, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, id, ParameterDirection.Input));
            var drd = await ExecuteReaderAsync("USP_PERSONA_MEDIDA_L", lParametros, con);
            return ListarObjeto<PersonaMedida>(drd);
        }

    }
}

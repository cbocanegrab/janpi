﻿using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using JANPI.Infrasturcture.ORM;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Infrastructure.Repositories
{
    public class NotificacionRepository : Base, INotificacionRepository
    {
        public async Task<ResponseSQL> Add(SqlConnection con, Notificacion entity)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, entity.idPersona, ParameterDirection.Input, 0));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMTIPONOTIFICACION", SqlDbType.BigInt, entity.idParamTipoNotificacion, ParameterDirection.Input, 100));
            lParametros.Add(new ParametroSQL("@PI_TXNOTIFICACION", SqlDbType.VarChar, entity.strNotificacion, ParameterDirection.Input, 100));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, entity.idUsuarioRegistro, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FENOTIFICACION", SqlDbType.DateTime, entity.strFechaNotificacion, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            return await ExecuteNonQueryAsync("USP_NOTIFICACION_I", lParametros, con);
        }

        public async Task<List<Notificacion>> GetAllByPersona(SqlConnection con, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, id, ParameterDirection.Input, 0));
            var drd =await ExecuteReaderAsync("USP_NOTIFICACION_L", lParametros, con);
            return ListarObjeto<Notificacion>(drd);
        }

        public async Task<ResponseSQL> UpdateLeido(SqlConnection con, Notificacion entity)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDNOTIFICACION", SqlDbType.BigInt, entity.idNotificacion, ParameterDirection.Input, 0));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, entity.idUsuarioRegistro, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            return await ExecuteNonQueryAsync("USP_NOTIFICACION_LEIDO", lParametros, con);
        }
    }
}

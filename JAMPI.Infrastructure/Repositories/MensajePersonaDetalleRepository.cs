﻿using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using JANPI.Infrasturcture.ORM;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Infrastructure.Repositories
{
    public class MensajePersonaDetalleRepository : Base, IMensajePersonaDetalleRepository
    {
        public async Task<int> Add(SqlConnection con, MensajePersonaDetalle entity, SqlTransaction trx)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMENSAJEPERSONA", SqlDbType.BigInt, entity.idMensajePersona, ParameterDirection.Input, 0));
            lParametros.Add(new ParametroSQL("@PI_IDPERSONAENVIA", SqlDbType.BigInt, entity.idPersonaEnvia, ParameterDirection.Input, 100));
            lParametros.Add(new ParametroSQL("@PI_TXMENSAJE", SqlDbType.VarChar, entity.strMensaje, ParameterDirection.Input, 100));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, entity.idUsuarioRegistro, ParameterDirection.Input));
            return await ExecuteNonQueryIntAsync("USP_MENSAJES_DETALLE_I", lParametros, con, trx);
        }

        public async Task<List<MensajePersonaDetalle>> GetAllByPersona(SqlConnection con, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMENSAJEPERSONA", SqlDbType.BigInt, id, ParameterDirection.Input, 0));
            var drd =await ExecuteReaderAsync("USP_MENSAJES_DETALLE_L", lParametros, con);
            var result = ListarObjeto<MensajePersonaDetalle>(drd);
            drd.Close();
            return result;
        }

        public async Task<ResponseSQL> UpdateLeido(SqlConnection con, long id, long idUsuario)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMENSAJEPERSONA", SqlDbType.BigInt, id, ParameterDirection.Input, 0));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            return await ExecuteNonQueryAsync("USP_MENSAJES_DETALLE_LEER", lParametros, con);
        }
    }
}

﻿using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Entities;
using JANPI.Infrasturcture.ORM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Infrastructure.Repositories
{
    public class MaestroRepository: Base, IMaestroRepository 
    {
        public async Task<List<TipoEmergenciaSelectorResponse>> ListarTipoEmergencia(SqlConnection con)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            SqlDataReader drd = await ExecuteReaderAsync("USP_TIPOEMERGENCIA_L", lParametros, con);
            return ListarObjeto<TipoEmergenciaSelectorResponse>(drd);
        }
        public async Task<List<ConveniosResponse>> ListarConvenio(SqlConnection con)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPARAMTIPOCONVENIO", SqlDbType.BigInt, 0, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_CONVENIOS_L", lParametros, con);
            return ListarObjeto<ConveniosResponse>(drd);
        }
        public async Task<List<MedicamentoResponse>> ListarMedicamento(SqlConnection con)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            SqlDataReader drd = await ExecuteReaderAsync("USP_MEDICAMENTO_L", lParametros, con);
            return ListarObjeto<MedicamentoResponse>(drd);
        }
        public async Task<List<Examen>> ListarExamen(SqlConnection con,long idExamen)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            SqlDataReader drd = await ExecuteReaderAsync("USP_EXAMEN_L", lParametros, con);
            return ListarObjeto<Examen>(drd);
        }
        public async Task<List<TipoEmergenciaTipResponse>> TipoEmergenciaTipListar(SqlConnection con,long idTipoEmergencia)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDTIPOEMERGENCIA", SqlDbType.BigInt, idTipoEmergencia, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_EMERGENCIA_TIP_L", lParametros, con);
            return ListarObjeto<TipoEmergenciaTipResponse>(drd);
        }
    }
}

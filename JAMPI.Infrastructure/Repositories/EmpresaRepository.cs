﻿using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using JANPI.Infrasturcture.ORM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Infrastructure.Repositories
{
    public class EmpresaRepository :Base, IEmpresaRepository
    {
        public async Task<ResponseSQL> Add(SqlConnection con, Empresa entity)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_TXRUC", SqlDbType.VarChar, entity.strRUC, ParameterDirection.Input,20));
            lParametros.Add(new ParametroSQL("@PI_TXRAZONSOCIAL", SqlDbType.VarChar, entity.strRazonSocial, ParameterDirection.Input, 100));
            lParametros.Add(new ParametroSQL("@PI_TXDIRECCIONCOMERCIAL", SqlDbType.VarChar, entity.strDireccionComercial, ParameterDirection.Input, 500));
            lParametros.Add(new ParametroSQL("@PI_TXTELEFONO", SqlDbType.VarChar, entity.strTelefono, ParameterDirection.Input, 15));
            lParametros.Add(new ParametroSQL("@PI_TXUBIGEO", SqlDbType.VarChar, entity.strUbigeo, ParameterDirection.Input, 6));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, entity.idUsuarioRegistro, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_EMPRESA_I", lParametros, con);

            return rpta;
        }

        public async Task<ResponseSQL> Update(SqlConnection con, Empresa entity)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDEMPRESA", SqlDbType.BigInt, entity.idEmpresa, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXRUC", SqlDbType.VarChar, entity.strRUC, ParameterDirection.Input, 20));
            lParametros.Add(new ParametroSQL("@PI_TXRAZONSOCIAL", SqlDbType.VarChar, entity.strRazonSocial, ParameterDirection.Input, 100));
            lParametros.Add(new ParametroSQL("@PI_TXDIRECCIONCOMERCIAL", SqlDbType.VarChar, entity.strDireccionComercial, ParameterDirection.Input, 500));
            lParametros.Add(new ParametroSQL("@PI_TXTELEFONO", SqlDbType.VarChar, entity.strTelefono, ParameterDirection.Input, 15));
            lParametros.Add(new ParametroSQL("@PI_TXUBIGEO", SqlDbType.VarChar, entity.strUbigeo, ParameterDirection.Input, 6));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, entity.idUsuarioRegistro, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            rpta = await ExecuteNonQueryAsync("USP_EMPRESA_U", lParametros, con);

            return rpta;
        }

        public async Task<ResponseSQL> Delete(SqlConnection con, Empresa entity)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDEMPRESA", SqlDbType.BigInt, entity.idEmpresa, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, entity.idUsuarioRegistro, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            rpta = await ExecuteNonQueryAsync("USP_EMPRESA_D", lParametros, con);

            return rpta;
        }

        public async Task<Empresa> Get(SqlConnection con, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDEMPRESA", SqlDbType.BigInt, id, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_EMPRESA_L", lParametros, con);
            Empresa oEntidad = ObtenerObjeto<Empresa>(drd);
            drd.Close();
            return oEntidad;
        }

        public async Task<List<Empresa>> GetAll(SqlConnection con)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDEMPRESA", SqlDbType.BigInt, 0, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_EMPRESA_L", lParametros, con);
            return ListarObjeto<Empresa>(drd);
        }
        public async Task<EmpresaResumenResponse> ResumenSemanal(SqlConnection con, long idUsuario)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_EMPRESA_RESUMEN_SEMANAL", lParametros, con);
            EmpresaResumenResponse oEntidad = ObtenerObjeto<EmpresaResumenResponse>(drd);
            drd.Close();
            return oEntidad;
        }
        public async Task<EmpresaResumenResponse> ResumenMensual(SqlConnection con, long idUsuario)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_EMPRESA_RESUMEN_MENSUAL", lParametros, con);
            EmpresaResumenResponse oEntidad = ObtenerObjeto<EmpresaResumenResponse>(drd);
            drd.Close();
            return oEntidad;
        }
        public async Task<List<ConsultaEmpleadoHistorialResponse>> HistorialConsultas(SqlConnection con, long idUsuario)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_EMPLEADO_HISTORIAL_L", lParametros, con);
            return ListarObjeto<ConsultaEmpleadoHistorialResponse>(drd);
        }
    }
}

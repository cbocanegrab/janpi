﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using JANPI.Infrasturcture.ORM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Infrastructure.Repositories
{
    public class EmpresaEmpleadoRepository :Base, IEmpresaEmpleadoRepository
    {
        public async Task<ResponseSQL> EmpresaEmpleadoInsertar(SqlConnection con, EmpresaEmpleadoIRequest entity,string strClave, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPARAMTIPODOCUMENTO", SqlDbType.BigInt, entity.idParamTipoDocumento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXNUMERODOCUMENTO", SqlDbType.VarChar, entity.strNumeroDocumento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXNOMBRE", SqlDbType.VarChar, entity.strNombre, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXPRIMERAPELLIDO", SqlDbType.VarChar, entity.strPrimerApellido, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXSEGUNDOAPELLIDO", SqlDbType.VarChar, entity.strSegundoApellido, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FGSEXO", SqlDbType.VarChar, entity.strSexo, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FENACIMIENTO", SqlDbType.VarChar, entity.strFechaNacimiento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMESTADOCIVIL", SqlDbType.BigInt, entity.idParamEstadoCivil, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCELULARPRINCIPAL", SqlDbType.VarChar, entity.strCelularPrincipal, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCELULARSECUNDARIO", SqlDbType.VarChar, entity.strCelularSecundario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCORREOPRINCIPAL", SqlDbType.VarChar, entity.strCorreoPrincipal, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCORREOSECUNDARIO", SqlDbType.VarChar, entity.strCorreoSecundario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXDIRECCION", SqlDbType.VarChar, entity.strDireccion, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMGRADOINSTR", SqlDbType.BigInt, entity.idParamGradoInstruccion, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXOCUPACION", SqlDbType.VarChar, entity.strOcupacion, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FEINGRESO", SqlDbType.VarChar, entity.strFechaIngreso, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDEMPLEADOPADRE", SqlDbType.BigInt, entity.idEmpleadoPadre, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCLAVE", SqlDbType.VarChar, strClave, ParameterDirection.Input, 50, false));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_EMPRESAEMPLEADO_I", lParametros, con);

            return rpta;
        }
        public async Task<ResponseSQL> EmpresaEmpleadoActualizar(SqlConnection con, EmpresaEmpleadoURequest entity, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDEMPRESAEMPLEADO", SqlDbType.BigInt, entity.idEmpresaEmpleado, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMTIPODOCUMENTO", SqlDbType.BigInt, entity.idParamTipoDocumento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXNUMERODOCUMENTO", SqlDbType.VarChar, entity.strNumeroDocumento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXNOMBRE", SqlDbType.VarChar, entity.strNombre, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXPRIMERAPELLIDO", SqlDbType.VarChar, entity.strPrimerApellido, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXSEGUNDOAPELLIDO", SqlDbType.VarChar, entity.strSegundoApellido, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FGSEXO", SqlDbType.VarChar, entity.strSexo, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FENACIMIENTO", SqlDbType.VarChar, entity.strFechaNacimiento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMESTADOCIVIL", SqlDbType.BigInt, entity.idParamEstadoCivil, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCELULARPRINCIPAL", SqlDbType.VarChar, entity.strCelularPrincipal, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCELULARSECUNDARIO", SqlDbType.VarChar, entity.strCelularSecundario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCORREOSECUNDARIO", SqlDbType.VarChar, entity.strCorreoSecundario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXDIRECCION", SqlDbType.VarChar, entity.strDireccion, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMGRADOINSTR", SqlDbType.BigInt, entity.idParamGradoInstruccion, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXOCUPACION", SqlDbType.VarChar, entity.strOcupacion, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FEINGRESO", SqlDbType.VarChar, entity.strFechaIngreso, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FESALIDA", SqlDbType.VarChar, entity.strFechaSalida, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDEMPLEADOPADRE", SqlDbType.BigInt, entity.idEmpleadoPadre, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_EMPRESAEMPLEADO_U", lParametros, con);

            return rpta;
        }
        
        public async Task<ResponseSQL> EsmpresaEmpleadoEliminar(SqlConnection con, long idEmpresaEmpleado, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDEMPRESAEMPLEADO", SqlDbType.BigInt, idEmpresaEmpleado, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            rpta = await ExecuteNonQueryAsync("USP_EMPRESAEMPLEADO_D", lParametros, con);

            return rpta;
        }

        public async Task<EmpresaEmpleadoResponse> GetEmpleado(SqlConnection con, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDEMPRESAEMPLEADO", SqlDbType.BigInt, id, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, 0, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_EMPRESAEMPLEADO_L", lParametros, con);
            EmpresaEmpleadoResponse oEntidad = ObtenerObjeto<EmpresaEmpleadoResponse>(drd);
            drd.Close();
            return oEntidad;
        }

        public async Task<List<EmpresaEmpleadoResponse>> GetAllEmpleado(SqlConnection con, long idUsuario)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDEMPRESAEMPLEADO", SqlDbType.BigInt, 0, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_EMPRESAEMPLEADO_L", lParametros, con);
            return ListarObjeto<EmpresaEmpleadoResponse>(drd);
        }
        public async Task<EmpresaEmpleado> Get(SqlConnection con, long id)
        {
            throw new NotImplementedException();
        }

        public Task<List<EmpresaEmpleado>> GetAll(SqlConnection con)
        {
            throw new NotImplementedException();
        }
    }
}

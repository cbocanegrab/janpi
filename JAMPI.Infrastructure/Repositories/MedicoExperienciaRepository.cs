﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using JANPI.Infrasturcture.ORM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Infrastructure.Repositories
{
    public class MedicoExperienciaRepository : Base, IMedicoExperienciaRepository
    {
        public async Task<ResponseSQL> MedicoExperienciaInsertar (SqlConnection con, MedicoExperienciaIRequest entity, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_TXPUESTO", SqlDbType.VarChar, entity.strPuesto, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXINSTITUCIONEXPERIENCIA", SqlDbType.VarChar, entity.strInstitucionExperiencia, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FGCARGOACTUAL", SqlDbType.VarChar, entity.strCargoActual, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXANIOINICIO", SqlDbType.VarChar, entity.strAnioInicio, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXANIOFIN", SqlDbType.VarChar, entity.strAnioFin, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            lParametros.Add(new ParametroSQL("@PI_TXMESINICIO", SqlDbType.VarChar, entity.strMesInicio, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXMESFIN", SqlDbType.VarChar, entity.strMesFin, ParameterDirection.Input));

            rpta = await ExecuteNonQueryAsync("USP_MEDICO_EXPERIENCIA_I", lParametros, con);

            return rpta;
        }
        public async Task<ResponseSQL> MedicoExperienciaActualizar (SqlConnection con, MedicoExperienciaURequest entity, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICOEXPERIENCIA", SqlDbType.BigInt, entity.idMedicoExperiencia, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXPUESTO", SqlDbType.VarChar, entity.strPuesto, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXINSTITUCIONEXPERIENCIA", SqlDbType.VarChar, entity.strInstitucionExperiencia, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXANIOINICIO", SqlDbType.VarChar, entity.strAnioInicio, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXANIOFIN", SqlDbType.VarChar, entity.strAnioFin, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXMESINICIO", SqlDbType.VarChar, entity.strMesInicio, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXMESFIN", SqlDbType.VarChar, entity.strMesFin, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_MEDICO_EXPERIENCIA_U", lParametros, con);

            return rpta;
        }

        public async Task<ResponseSQL> MedicoExperienciaEliminar(SqlConnection con, long idMedicoExperiencia, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICOEXPERIENCIA", SqlDbType.BigInt, idMedicoExperiencia, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_MEDICO_EXPERIENCIA_D", lParametros, con);

            return rpta;
        }

        public async Task<List<MedicoExperiencia>> GetAllMedicoExperiencia(SqlConnection con, long idMedico, long idPersona)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICOEXPERIENCIA", SqlDbType.BigInt, 0, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDMEDICO", SqlDbType.BigInt, idMedico, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, idPersona, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_MEDICO_EXPERIENCIA_L", lParametros, con);
            List<MedicoExperiencia> lst = ListarObjeto<MedicoExperiencia>(drd);
            drd.Close();
            return lst;
        }
    }
}

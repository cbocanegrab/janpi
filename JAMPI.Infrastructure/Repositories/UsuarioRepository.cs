﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using JANPI.Infrasturcture.ORM;
using JAMPI.Domain.Commons;

namespace JAMPI.Infrastructure.Repositories
{
    public class UsuarioRepository : Base, IUsuarioRepository
    {
        public Task<ResponseSQL> Add(SqlConnection con, Usuario entity)
        {
            throw new NotImplementedException();
        }

        public Task<ResponseSQL> Delete(SqlConnection con, Usuario entidad)
        {
            throw new NotImplementedException();
        }

        public Task<Usuario> Get(SqlConnection con, long id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<Usuario>> GetAll(SqlConnection con)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            SqlDataReader drd = await ExecuteReaderAsync("USP_USUARIO_LISTAR", lParametros, con);
            List<Usuario> oListaUsuario = ListarObjeto<Usuario>(drd);
            drd.Close();
            return oListaUsuario;
        }

        public Task<ResponseSQL> Update(SqlConnection con, Usuario entity)
        {
            throw new NotImplementedException();
        }

        public async Task<Usuario> validarLogin(SqlConnection con, LoginRequest loginRequest)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_TXUSUARIO", SqlDbType.VarChar, loginRequest.strUsuario, ParameterDirection.Input, 100));
            lParametros.Add(new ParametroSQL("@PI_TXCLAVE", SqlDbType.VarChar, loginRequest.strClave1, ParameterDirection.Input, 100));
            SqlDataReader drd = await ExecuteReaderAsync("USP_USUARIO_VALIDAR_LOGIN", lParametros, con);
            Usuario oUsuario = ObtenerObjeto<Usuario>(drd);
            drd.Close();
            return oUsuario;
        }
        public async Task<Session> obtenerSesion(SqlConnection con, long idUsuario)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.VarChar, idUsuario, ParameterDirection.Input, 100));
            SqlDataReader drd = await ExecuteReaderAsync("USP_USUARIO_G", lParametros, con);
            Session oUsuario = ObtenerObjeto<Session>(drd);
            drd.Close();
            return oUsuario;
        }
        public async Task<ResponseSQL> ActivarUsuario(SqlConnection con, LoginRequest loginRequest)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_TXNOMBREUSUARIO", SqlDbType.VarChar, loginRequest.strUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXTOKEN", SqlDbType.VarChar, loginRequest.strToken, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCLAVE", SqlDbType.VarChar, loginRequest.strClave1, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            rpta = await ExecuteNonQueryAsync("USP_USUARIO_ACTIVAR", lParametros, con);

            return rpta;
        }
        public async Task<ResponseSQL> CambiarClave(SqlConnection con, PasswordRequest PasswordRequest)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_TXNOMBREUSUARIO", SqlDbType.VarChar, PasswordRequest.strUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCLAVEACTUAL", SqlDbType.VarChar, PasswordRequest.strClaveActual, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCLAVE", SqlDbType.VarChar, PasswordRequest.strClave1, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            rpta = await ExecuteNonQueryAsync("USP_USUARIO_CAMBIARCLAVE", lParametros, con);

            return rpta;
        }
        public async Task<ResponseSQL> GuardarToken(SqlConnection con, Usuario usuario)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_TXNOMBREUSUARIO", SqlDbType.VarChar, usuario.strNombreUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXTOKEN", SqlDbType.VarChar, usuario.strToken, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            return await ExecuteNonQueryAsync("USP_USUARIO_GUARDARTOKEN", lParametros, con);
        }

        public async Task<ResponseSQL> VerificaToken(SqlConnection con, Usuario usuario)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_TXNOMBREUSUARIO", SqlDbType.VarChar, usuario.strNombreUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXTOKEN", SqlDbType.VarChar, usuario.strToken, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            return await ExecuteNonQueryAsync("USP_USUARIO_VALIDATOKEN", lParametros, con);
        }

        public async Task<Usuario> RecuperarClave(SqlConnection con, string strUsuario)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_TXNOMBREUSUARIO", SqlDbType.VarChar, strUsuario, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_USUARIO_RECUPERARCLAVE", lParametros, con);
            Usuario oEntidad = ObtenerObjeto<Usuario>(drd);
            drd.Close();
            return oEntidad;
        }

        public async Task<Session> GetLoginExternal(SqlConnection con, string strAccesoToken, long idParamTipoRed)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_TXUSER", SqlDbType.VarChar, strAccesoToken, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMTIPORED", SqlDbType.BigInt, idParamTipoRed, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_USUARIO_SOCIAL_G", lParametros, con);
            Session session = ObtenerObjeto<Session>(drd);
            drd.Close();

            return session;
        }
        public async Task<ResponseSQL> RestableceClave(SqlConnection con, RestableceClaveRequest obj)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_TXNOMBREUSUARIO", SqlDbType.VarChar, obj.strUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXTOKEN", SqlDbType.VarChar, obj.strToken, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCLAVE", SqlDbType.VarChar, obj.strClave1, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            rpta = await ExecuteNonQueryAsync("USP_USUARIO_RESTABLECECLAVE", lParametros, con);

            return rpta;
        }
    }
}

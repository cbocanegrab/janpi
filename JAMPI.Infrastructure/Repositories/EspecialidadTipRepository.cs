﻿using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using JANPI.Infrasturcture.ORM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Infrastructure.Repositories
{
    public class EspecialidadTipRepository : Base, IEspecialidadTipRepository
    {
        public async Task<ResponseSQL> Add(SqlConnection con, EspecialidadTip entity)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDESPECIALIDAD", SqlDbType.BigInt, entity.idEspecialidad, ParameterDirection.Input, 0));
            lParametros.Add(new ParametroSQL("@PI_TXESPECIALIDADTIP", SqlDbType.VarChar, entity.strTip, ParameterDirection.Input, 100));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, entity.idUsuarioRegistro, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            return await ExecuteNonQueryAsync("USP_ESPECIALIDAD_TIP_I", lParametros, con);
        }

        public async Task<ResponseSQL> Delete(SqlConnection con, EspecialidadTip entity)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDESPECIALIDADTIP", SqlDbType.BigInt, entity.idEspecialidadTip, ParameterDirection.Input, 0));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, entity.idUsuarioRegistro, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            return await ExecuteNonQueryAsync("USP_ESPECIALIDAD_TIP_D", lParametros, con);
        }

        public async Task<EspecialidadTip> Get(SqlConnection con, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDESPECIALIDADTIP", SqlDbType.BigInt, id, ParameterDirection.Input, 0));
            var drd = await ExecuteReaderAsync("USP_ESPECIALIDAD_TIP_G", lParametros, con);
            return ObtenerObjeto<EspecialidadTip>(drd);
        }

        public async Task<List<EspecialidadTip>> GetAll(SqlConnection con)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            var drd = await ExecuteReaderAsync("USP_ESPECIALIDAD_TIP_L", lParametros, con);
            return ListarObjeto<EspecialidadTip>(drd);
        }
        public async Task<List<EspecialidadTip>> GetAllByEspecialidad(SqlConnection con, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDESPECIALIDAD", SqlDbType.BigInt, id, ParameterDirection.Input, 0));
            var drd = await ExecuteReaderAsync("USP_ESPECIALIDAD_TIP_L", lParametros, con);
            return ListarObjeto<EspecialidadTip>(drd);
        }

        public async Task<ResponseSQL> Update(SqlConnection con, EspecialidadTip entity)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDESPECIALIDADTIP", SqlDbType.BigInt, entity.idEspecialidadTip, ParameterDirection.Input, 0));
            lParametros.Add(new ParametroSQL("@PI_IDESPECIALIDAD", SqlDbType.BigInt, entity.idEspecialidad, ParameterDirection.Input, 0));
            lParametros.Add(new ParametroSQL("@PI_TXESPECIALIDADTIP", SqlDbType.VarChar, entity.strTip, ParameterDirection.Input, 100));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, entity.idUsuarioRegistro, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            return await ExecuteNonQueryAsync("USP_ESPECIALIDAD_TIP_U", lParametros, con);
        }
    }
}

﻿using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using JANPI.Infrasturcture.ORM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Infrastructure.Repositories
{
    public class ParametroRepository :Base, IParametroRepository
    {
        public async Task<List<Parametro>> List(SqlConnection con, long idTipoParametro)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDTIPOPARAMETRO", SqlDbType.VarChar, idTipoParametro, ParameterDirection.Input,20));
            SqlDataReader drd =  await ExecuteReaderAsync("USP_PARAMETRO_L", lParametros, con);
            return ListarObjeto<Parametro>(drd);
        }
    }
}

﻿
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using JANPI.Infrasturcture.ORM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Infrastructure.Repositories
{
    public class EspecialidadMedicaRepository : Base, IEspecialidadMedicaRepository
    {
        //private readonly long _userID;
        public EspecialidadMedicaRepository()//long userID)
        {
            //_userID = userID;
        }
        public async Task<ResponseSQL> AddAsync(SqlConnection con, EspecialidadRequest entity)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_TXESPECIALIDAD", SqlDbType.VarChar, entity.strEspecialidad, ParameterDirection.Input, 100));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, 1, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            return await ExecuteNonQueryAsync("USP_ESPECIALIDAD_INSERT", lParametros, con);
        }
        public Task<ResponseSQL> Update(SqlConnection con, Especialidad entity)
        {
            throw new NotImplementedException();
        }
        public async Task<ResponseSQL> Update(SqlConnection con, EspecialidadRequest entity)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDESPECIALIDAD", SqlDbType.BigInt, entity.idEspecialidad, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXESPECIALIDAD", SqlDbType.VarChar, entity.strEspecialidad, ParameterDirection.Input, 100));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, 1, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            return await ExecuteNonQueryAsync("USP_ESPECIALIDAD_UPDATE", lParametros, con);
        }

        public async Task<ResponseSQL> Delete(SqlConnection con, Especialidad entidad)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDESPECIALIDAD", SqlDbType.BigInt, entidad.idEspecialidad, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, 1, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            return await ExecuteNonQueryAsync("USP_ESPECIALIDAD_DELETE", lParametros, con);
        }

        public async Task<Especialidad> Get(SqlConnection con, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDESPECIALIDAD", SqlDbType.BigInt, id, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            SqlDataReader drd = await ExecuteReaderAsync("USP_ESPECIALIDAD_GET", lParametros, con);
            return ObtenerObjeto<Especialidad>(drd);
        }
        public async Task<List<Especialidad>> GetAll(SqlConnection con)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            SqlDataReader drd = await ExecuteReaderAsync("USP_ESPECIALIDAD_LIST", lParametros, con);
            return ListarObjeto<Especialidad>(drd);
        }
        public async Task<List<EspecialidadSelectorResponse>> GetAllBasic(SqlConnection con)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            SqlDataReader drd = await ExecuteReaderAsync("USP_ESPECIALIDAD_LIST", lParametros, con);
            return ListarObjeto<EspecialidadSelectorResponse>(drd);
        }
        public async Task<ResponseSQL> Add(SqlConnection con, Especialidad entity)
        {
            throw new NotImplementedException();
        }
    }
}

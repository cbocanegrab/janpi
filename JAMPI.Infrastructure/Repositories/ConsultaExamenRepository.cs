﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using JANPI.Infrasturcture.ORM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
namespace JAMPI.Infrastructure.Repositories
{
    public class ConsultaExamenRepository: Base, IConsultaExamenRepository
    {
        public async Task<ResponseSQL> ConsultaExamenGuardar(SqlConnection con, ConsultaExamenGRequest entity, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, entity.idConsulta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDEXAMEN", SqlDbType.BigInt, entity.idExamen, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FGESTADO", SqlDbType.VarChar, entity.strEstado, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_CONSULTA_EXAMEN_G", lParametros, con);

            return rpta;
        }

        public async Task<List<ConsultaExamen>> ConsultaExamenListar(SqlConnection con, long idConsulta)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, idConsulta, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_CONSULTA_EXAMEN_L", lParametros, con);
            return ListarObjeto<ConsultaExamen>(drd);
        }
        public async Task<ExamenResumenResponse> ConsultaExamenResumen(SqlConnection con, long idConsulta)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, idConsulta, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_EXAMEN_RESUMEN", lParametros, con);
            ExamenResumenResponse oConsultaExamen = new ExamenResumenResponse();
            oConsultaExamen.oPersona = ObtenerObjeto<PersonaMin>(drd);
            await drd.NextResultAsync();
            oConsultaExamen.lExamen = ListarObjeto<ExamenMin>(drd);
            await drd.NextResultAsync();
            drd.Close();
            return oConsultaExamen;
        }
    }
}

﻿
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using JANPI.Infrasturcture.ORM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Infrastructure.Repositories
{
    public class MedicoRepository : Base, IMedicoRepository
    {
        private readonly long _userID;

        public Task<ResponseSQL> Add(SqlConnection con, Medico entity)
        {
            throw new NotImplementedException();
        }
        public async Task<ResponseSQL> MedicoInsertar(SqlConnection con, MedicoIRequest entity, long idUsuario,string strClave)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPARAMTIPODOCUMENTO", SqlDbType.BigInt, entity.idParamTipoDocumento, ParameterDirection.Input, 100));
            lParametros.Add(new ParametroSQL("@PI_TXNUMERODOCUMENTO", SqlDbType.VarChar, entity.strNumeroDocumento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXNOMBRE", SqlDbType.VarChar, entity.strNombre, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXPRIMERAPELLIDO", SqlDbType.VarChar, entity.strPrimerApellido, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXSEGUNDOAPELLIDO", SqlDbType.VarChar, entity.strSegundoApellido, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMTIPOMEDICO", SqlDbType.BigInt, entity.idParamTipoMedico, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCELULARPRINCIPAL", SqlDbType.VarChar, entity.strCelularPrincipal, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCORREOPRINCIPAL", SqlDbType.VarChar, entity.strCorreoPrincipal, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FEINICIO", SqlDbType.VarChar, entity.strFechaInicio, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FEFIN", SqlDbType.VarChar, entity.strFechaFin, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDESPECIALIDAD1", SqlDbType.BigInt, entity.idEspecialidad1, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDESPECIALIDAD2", SqlDbType.BigInt, entity.idEspecialidad2, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCLAVE", SqlDbType.VarChar,strClave, ParameterDirection.Input, 50));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.VarChar, idUsuario, ParameterDirection.Input, 50));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            rpta = await ExecuteNonQueryAsync("USP_MEDICO_I", lParametros, con);
            return rpta;
        }

        public async Task<ResponseSQL> MedicoActualizar(SqlConnection con, MedicoURequest entity, long idUsuario )
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICO", SqlDbType.BigInt, entity.idMedico, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMTIPODOCUMENTO", SqlDbType.BigInt, entity.idParamTipoDocumento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXNUMERODOCUMENTO", SqlDbType.VarChar, entity.strNumeroDocumento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXNOMBRE", SqlDbType.VarChar, entity.strNombre, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXPRIMERAPELLIDO", SqlDbType.VarChar, entity.strPrimerApellido, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXSEGUNDOAPELLIDO", SqlDbType.VarChar, entity.strSegundoApellido, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FGSEXO", SqlDbType.VarChar, entity.strSexo, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMESTADOCIVIL", SqlDbType.BigInt, entity.idParamEstadoCivil, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMTIPOMEDICO", SqlDbType.BigInt, entity.idParamTipoMedico, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCELULARPRINCIPAL", SqlDbType.VarChar, entity.strCelularPrincipal, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCELULARSECUNDARIO", SqlDbType.VarChar, entity.strCelularSecundario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXDIRECCION", SqlDbType.VarChar, entity.strDireccion, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCMP", SqlDbType.VarChar, entity.strCMP, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDESPECIALIDAD1", SqlDbType.BigInt, entity.idEspecialidad1, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDESPECIALIDAD2", SqlDbType.BigInt, entity.idEspecialidad2, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.VarChar, idUsuario, ParameterDirection.Input, 50));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            rpta = await ExecuteNonQueryAsync("USP_MEDICO_U", lParametros, con);
            return rpta;
        }

        public async Task<ResponseSQL> Delete(SqlConnection con, Medico entidad)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICO", SqlDbType.BigInt, entidad.idMedico, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, 1, ParameterDirection.Input));
            rpta = await ExecuteNonQueryAsync("USP_ESPECIALIDAD_DELETE", lParametros, con);
            return rpta;
        }

        public async Task<Medico> Get(SqlConnection con, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICO", SqlDbType.BigInt, id, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_MEDICO_L", lParametros, con);
            var result= ObtenerObjeto<Medico>(drd);
            drd.Close();
            return result;
        }

        public async Task<MedicoCVResponse> GetCv(SqlConnection con, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICO", SqlDbType.BigInt, id, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_MEDICO_CV_G", lParametros, con);
            MedicoCVResponse medicoCV = new MedicoCVResponse();
            medicoCV.MedicoInfo = ObtenerObjeto<MedicoCVInfo>(drd);
            await drd.NextResultAsync();
            medicoCV.lMedicoExpediencia = ListarObjeto<MedicoCVExperiencia>(drd); 
            await drd.NextResultAsync();
            medicoCV.lMedicoEstudios = ListarObjeto<MedicoCVEstudios>(drd);
            await drd.NextResultAsync();
            medicoCV.lMedicoComentarios = ListarObjeto<MedicoCVComentarios>(drd);
            drd.Close();
            return medicoCV;
        }

        public async Task<List<Medico>> GetAll(SqlConnection con)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            SqlDataReader drd = await ExecuteReaderAsync("USP_MEDICO_LIST", lParametros, con);
            var result = ListarObjeto<Medico>(drd);
            drd.Close();
            return result;
        }

        public async Task<List<MedicoDisponibleResponse>> GetAllByEspecialidad(SqlConnection con, long idEspecialidad)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDESPECIALIDAD", SqlDbType.BigInt, idEspecialidad, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_MEDICO_LIST_ESPECIALIDAD", lParametros, con);
            var result = ListarObjeto<MedicoDisponibleResponse>(drd);
            drd.Close();
            return result;
        }

        public async Task<ResponseSQL> Update(SqlConnection con, Medico entity)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICO", SqlDbType.BigInt, entity.idMedico, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.VarChar, entity.idPersona, ParameterDirection.Input, 100));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMTIPOMEDICO", SqlDbType.BigInt, entity.idParamTipoMedico, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FEINICIO", SqlDbType.BigInt, entity.strFechaInicio, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FEFIN", SqlDbType.BigInt, entity.strFechaFin, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCMP", SqlDbType.BigInt, entity.strCMP, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXUSUARIO", SqlDbType.VarChar, "ADMIN", ParameterDirection.Input, 50));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            rpta = await ExecuteNonQueryAsync("USP_MEDICO_UPDATE", lParametros, con);
            return rpta;
        }

        public async Task<List<MedicoMinResponse>> ListarMedicosbyUsuario(SqlConnection con,long idPersona)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, idPersona, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_MEDICO_BYUSUARIO_LIST", lParametros, con);
            return ListarObjeto<MedicoMinResponse>(drd);
        }
        public async Task<List<MedicoMinResponse>> ListarMedicoEmergencia(SqlConnection con,long idEspecialidad, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDESPECIALIDAD", SqlDbType.BigInt, idEspecialidad, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_MEDICO_EMERGENCIA_LIST", lParametros, con);
            return ListarObjeto<MedicoMinResponse>(drd);
        }
        public async Task<List<MedicoDisponibleHorarioResponse>> ListarMedicoDisponible(SqlConnection con, long idEspecialidad, string strFecha)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDESPECIALIDAD", SqlDbType.BigInt, idEspecialidad, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FEINICIO", SqlDbType.VarChar, strFecha, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_MEDICO_DISPONIBLE_LIST", lParametros, con);
            List<MedicoDisponibleHorarioResponse> lst = new List<MedicoDisponibleHorarioResponse>();
            lst= ListarObjeto<MedicoDisponibleHorarioResponse>(drd);
            drd.Close();
            return lst;
        }
        public async Task<List<Horario>> ListarMedicoDisponibleHorario(SqlConnection con, long idMedicoEspecialidad, string strFecha)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDMEDICOESPECIALIDAD", SqlDbType.BigInt, idMedicoEspecialidad, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FEINICIO", SqlDbType.VarChar, strFecha, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_MEDICO_DISPON_HORARIO_LIST", lParametros, con);
            List<Horario> lst = new List<Horario>();
            lst = ListarObjeto<Horario>(drd);
            drd.Close();
            return lst;
        }
        public async Task<List<Medico>> MedicoBusqueda(SqlConnection con, string strDescipcion, long idEspecialidad)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_TXDESCRIPCION", SqlDbType.VarChar, strDescipcion, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDESPECIALIDAD", SqlDbType.BigInt, idEspecialidad, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_MEDICO_BUSQUEDA", lParametros, con);
            return ListarObjeto<Medico>(drd);
        }
        public async Task<Medico> MedicoValoracion(SqlConnection con, long idPersona)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, idPersona, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_MEDICO_VALORACION", lParametros, con);
            Medico obj = ObtenerObjeto<Medico>(drd);
            drd.Close();
            return obj;
        }

        public async Task<List<Paciente>> PacienteByMedico(SqlConnection con, long idPersona)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, idPersona, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_PACIENTE_BY_MEDICO", lParametros, con);
            List<Paciente> lst = new List<Paciente>();
            lst = ListarObjeto<Paciente>(drd);
            drd.Close();
            return lst;
        }
        public async Task<ResponseSQL> MedicoPersonaGuardar(SqlConnection con, MedicoPersonaRequest entity, long idPersona,long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONAMEDICO", SqlDbType.BigInt, idPersona, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPERSONAPACIENTE", SqlDbType.BigInt, entity.idPersona, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FGDESTACADO", SqlDbType.VarChar, entity.strDestacado, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_MEDICO_PERSONA_G", lParametros, con);

            return rpta;
        }

    }
}

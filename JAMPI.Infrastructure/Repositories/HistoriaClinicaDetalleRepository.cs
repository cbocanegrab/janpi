﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using JANPI.Infrasturcture.ORM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Infrastructure.Repositories
{
    public class HistoriaClinicaDetalleRepository : Base, IHistoriaClinicaDetalleRepository
    {
        public async Task<ResponseSQL> HistoriaClinicaDetalleActualizar(SqlConnection con, HistoriaClinicaDetalleURequest entity, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDHISTORIACLINICADET", SqlDbType.BigInt, entity.idHistoriaClinicaDet, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, entity.idConsulta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXMOTIVOCONSULTA ", SqlDbType.VarChar, entity.strMotivoConsulta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXDETALLEENEFERMEDAD  ", SqlDbType.VarChar, entity.strDetalleEnfermedad, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_HISTORIA_CLINICA_DETALLE_U", lParametros, con);

            return rpta;
        }

        public async Task<ResponseSQL> HistoriaClinicaDetalleInsertar(SqlConnection con, HistoriaClinicaDetalleIRequest entity, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDHISTORIACLINICA", SqlDbType.BigInt, entity.idHistoriaClinica, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDCONSULTA", SqlDbType.BigInt, entity.idConsulta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXMOTIVOCONSULTA ", SqlDbType.VarChar, entity.strMotivoConsulta, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXDETALLEENEFERMEDAD  ", SqlDbType.VarChar, entity.strDetalleEnfermedad, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_HISTORIA_CLINICA_DETALLE_I", lParametros, con);

            return rpta;
        }

        public async Task<List<HistoriaClinicaDetalle>> HistoriaClinicaDetalleListar(SqlConnection con, long idHistoriaClinica)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDHISTORIACLINICA", SqlDbType.BigInt, idHistoriaClinica, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_HISTORIA_CLINICA_DETALLE_L", lParametros, con);
            List<HistoriaClinicaDetalle> lst = ListarObjeto<HistoriaClinicaDetalle>(drd);
            drd.Close();
            return lst;
        }
    }
}

﻿
using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Contracts.Responses;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using JANPI.Infrasturcture.ORM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Infrastructure.Repositories
{
    public class PersonaRepository : Base, IPersonaRepository
    {
        public async Task<ResponseSQL> Add(SqlConnection con, Persona entity)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            //lParametros.Add(new ParametroSQL("@PI_IDPARAMTIPODOCUMENTO", SqlDbType.BigInt, entity.idParamTipoDocumento, ParameterDirection.Input));
            //lParametros.Add(new ParametroSQL("@PI_TXNUMERODOCUMENTO", SqlDbType.VarChar, entity.strNumeroDocumento, ParameterDirection.Input, 20));
            lParametros.Add(new ParametroSQL("@PI_TXNOMBRE", SqlDbType.VarChar, entity.strNombre, ParameterDirection.Input, 50));
            lParametros.Add(new ParametroSQL("@PI_TXPRIMERAPELLIDO", SqlDbType.VarChar, entity.strPrimerApellido, ParameterDirection.Input, 50));
            lParametros.Add(new ParametroSQL("@PI_TXSEGUNDOAPELLIDO", SqlDbType.VarChar, entity.strSegundoApellido, ParameterDirection.Input, 50));
            lParametros.Add(new ParametroSQL("@PI_TXCELULARPRINCIPAL", SqlDbType.VarChar, entity.strCelularPrincipal, ParameterDirection.Input, 20));
            lParametros.Add(new ParametroSQL("@PI_TXCORREOPRINCIPAL", SqlDbType.VarChar, entity.strCorreoPrincipal, ParameterDirection.Input, 50));
            lParametros.Add(new ParametroSQL("@PI_TXCLAVE", SqlDbType.VarChar, entity.usuario.strClave, ParameterDirection.Input, 50, false));
            lParametros.Add(new ParametroSQL("@PI_TXUSERID", SqlDbType.VarChar, entity.usuario.strUserId, ParameterDirection.Input, 100, false));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMTIPORED", SqlDbType.VarChar, entity.usuario.idParamTipoRed, ParameterDirection.Input, 0, false));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIOPADRE", SqlDbType.BigInt, entity.usuario.idUsuarioPadre, ParameterDirection.Input, 0));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_PERSONA_INSERT", lParametros, con);

            return rpta;
        }

        public async Task<ResponseSQL> PersonaActualizar(SqlConnection con, PersonaURequest entity,long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_TXNOMBRE", SqlDbType.VarChar, entity.strNombre, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXPRIMERAPELLIDO", SqlDbType.VarChar, entity.strPrimerApellido, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXSEGUNDOAPELLIDO", SqlDbType.VarChar, entity.strSegundoApellido, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FGSEXO", SqlDbType.Char, entity.strSexo, ParameterDirection.Input, 1));
            lParametros.Add(new ParametroSQL("@PI_FENACIMIENTO", SqlDbType.VarChar, entity.strFechaNacimiento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMTIPODOCUMENTO", SqlDbType.BigInt, entity.idParamTipoDocumento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXNUMERODOCUMENTO", SqlDbType.VarChar, entity.strNumeroDocumento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMESTADOCIVIL", SqlDbType.BigInt, entity.idParamEstadoCivil, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCELULARPRINCIPAL", SqlDbType.VarChar, entity.strCelularPrincipal, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCELULARSECUNDARIO", SqlDbType.VarChar, entity.strCelularSecundario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCORREOSECUNDARIO", SqlDbType.VarChar, entity.strCorreoSecundario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXDIRECCION", SqlDbType.VarChar, entity.strDireccion, ParameterDirection.Input, 500));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMGRADOINSTR", SqlDbType.BigInt, entity.idParamGradoInstr, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXOCUPACION", SqlDbType.VarChar, entity.strOcupacion, ParameterDirection.Input, 100));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            rpta = await ExecuteNonQueryAsync("USP_PERSONA_U", lParametros, con);

            return rpta;
        }
        public Task<ResponseSQL> Update(SqlConnection con, Persona entity)
        {
            throw new NotImplementedException();
        }
        public Task<ResponseSQL> Delete(SqlConnection con, Persona entity)
        {
            throw new NotImplementedException();
        }

        public async Task<Persona> Get(SqlConnection con, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, id, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, 0, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_PERSONA_G", lParametros, con);
            Persona oPersona = ObtenerObjeto<Persona>(drd);
            drd.Close();
            return oPersona;
        }

        public Task<List<Persona>> GetAll(SqlConnection con)
        {
            throw new NotImplementedException();
        }
        public async Task<List<PersonaDependienteResponse>> ListarDependientes(SqlConnection con, long idPersona)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, idPersona, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_PERSONA_DEPENDIENTES_LIST", lParametros, con);
            return ListarObjeto<PersonaDependienteResponse>(drd);
        }
        public async Task<Persona> Credito(SqlConnection con, long idPersona)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, idPersona, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_PERSONA_CREDITO_L", lParametros, con);
            Persona oPersona = ObtenerObjeto<Persona>(drd);
            return oPersona;
        }
        public async Task<ResponseSQL> InsertarContacto(SqlConnection con, string strDescripcion, long idPersona, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_TXDESCRIPCION", SqlDbType.VarChar, strDescripcion, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, idPersona, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));

            rpta = await ExecuteNonQueryAsync("USP_CONTACTO_I", lParametros, con);

            return rpta;
        }
        public async Task<PersonaMinResponse> PersonaMin(SqlConnection con, long idPersona)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, idPersona, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_PERSONA_MIN", lParametros, con);
            PersonaMinResponse oPersona = ObtenerObjeto<PersonaMinResponse>(drd);
            return oPersona;
        }
        public async Task<ResponseSQL> PersonaDepInsertar(SqlConnection con, PersonaDepRequest entity, string strClave,long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_TXNOMBRE", SqlDbType.VarChar, entity.strNombre, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXPRIMERAPELLIDO", SqlDbType.VarChar, entity.strPrimerApellido, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXSEGUNDOAPELLIDO", SqlDbType.VarChar, entity.strSegundoApellido, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FGSEXO", SqlDbType.Char, entity.strSexo, ParameterDirection.Input, 1));
            lParametros.Add(new ParametroSQL("@PI_FENACIMIENTO", SqlDbType.VarChar, entity.strFechaNacimiento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMTIPODOCUMENTO", SqlDbType.BigInt, entity.idParamTipoDocumento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXNUMERODOCUMENTO", SqlDbType.VarChar, entity.strNumeroDocumento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMESTADOCIVIL", SqlDbType.BigInt, entity.idParamEstadoCivil, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCELULARPRINCIPAL", SqlDbType.VarChar, entity.strCelularPrincipal, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCELULARSECUNDARIO", SqlDbType.VarChar, entity.strCelularSecundario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCORREOPRINCIPAL", SqlDbType.VarChar, entity.strCorreoPrincipal, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCORREOSECUNDARIO", SqlDbType.VarChar, entity.strCorreoSecundario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXDIRECCION", SqlDbType.VarChar, entity.strDireccion, ParameterDirection.Input, 500));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMGRADOINSTR", SqlDbType.BigInt, entity.idParamGradoInstr, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXOCUPACION", SqlDbType.VarChar, entity.strOcupacion, ParameterDirection.Input, 100));
            lParametros.Add(new ParametroSQL("@PI_TXPARENTESCO", SqlDbType.VarChar, entity.strParentesco, ParameterDirection.Input, 50, false));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            rpta = await ExecuteNonQueryAsync("USP_PERSONADEP_I", lParametros, con);

            return rpta;
        }
        public async Task<ResponseSQL> PersonaDepActualizar(SqlConnection con, PersonaDepRequest entity, string strClave, long idUsuario)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, entity.idPersona, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXNOMBRE", SqlDbType.VarChar, entity.strNombre, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXPRIMERAPELLIDO", SqlDbType.VarChar, entity.strPrimerApellido, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXSEGUNDOAPELLIDO", SqlDbType.VarChar, entity.strSegundoApellido, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FGSEXO", SqlDbType.Char, entity.strSexo, ParameterDirection.Input, 1));
            lParametros.Add(new ParametroSQL("@PI_FENACIMIENTO", SqlDbType.VarChar, entity.strFechaNacimiento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMTIPODOCUMENTO", SqlDbType.BigInt, entity.idParamTipoDocumento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXNUMERODOCUMENTO", SqlDbType.VarChar, entity.strNumeroDocumento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMESTADOCIVIL", SqlDbType.BigInt, entity.idParamEstadoCivil, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCELULARPRINCIPAL", SqlDbType.VarChar, entity.strCelularPrincipal, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCELULARSECUNDARIO", SqlDbType.VarChar, entity.strCelularSecundario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCORREOPRINCIPAL", SqlDbType.VarChar, entity.strCorreoPrincipal, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXCORREOSECUNDARIO", SqlDbType.VarChar, entity.strCorreoSecundario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXDIRECCION", SqlDbType.VarChar, entity.strDireccion, ParameterDirection.Input, 500));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMGRADOINSTR", SqlDbType.BigInt, entity.idParamGradoInstr, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXOCUPACION", SqlDbType.VarChar, entity.strOcupacion, ParameterDirection.Input, 100));
            lParametros.Add(new ParametroSQL("@PI_TXPARENTESCO", SqlDbType.VarChar, entity.strParentesco, ParameterDirection.Input, 50, false));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            rpta = await ExecuteNonQueryAsync("USP_PERSONADEP_U", lParametros, con);

            return rpta;
        }
        public async Task<ResponseSQL> VinculoActualizar(SqlConnection con, long idPersona, string strEstado, long idUsuario)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, idPersona, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_FGVINCULOESTADO", SqlDbType.Char, strEstado, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, idUsuario, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output, 500));
            var rpta = await ExecuteNonQueryAsync("USP_VINCULO_U", lParametros, con);
            return rpta;
        }
        public async Task<PersonaResponse> PersonaObtener(SqlConnection con, long id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, 0, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_IDPERSONA", SqlDbType.BigInt, id, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_PERSONA_G", lParametros, con);
            PersonaResponse oPersona = ObtenerObjeto<PersonaResponse>(drd);
            drd.Close();
            return oPersona;
        }
    }
}

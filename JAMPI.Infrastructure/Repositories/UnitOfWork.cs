﻿using JAMPI.Application.Interfaces.Repositories;

namespace JAMPI.Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork()
        {
        }

        public IUsuarioRepository usuarioRepository { get => new UsuarioRepository(); }
        public IPersonaRepository personaRepository { get => new PersonaRepository(); }
        public IMedicoRepository medicoRepository { get => new MedicoRepository(); }
        public IEspecialidadMedicaRepository especialidadMedicaRepository { get => new EspecialidadMedicaRepository(); }
        public IConsultaRepository consultaRepository { get => new ConsultaRepository(); }
        public IEmpresaRepository empresaRepository { get => new EmpresaRepository(); }
        public IEmpresaEmpleadoRepository empresaEmpleadoRepository { get => new EmpresaEmpleadoRepository(); }
        public IParametroRepository parametroRepository { get => new ParametroRepository(); }
        public IPersonaMedidaRepository personaMedidaRepository { get => new PersonaMedidaRepository(); }
        public IPersonaConfigMedidaRepository personaConfigMedidaRepository { get => new PersonaConfigMedidaRepository(); }
        public INotificacionRepository notificacionRepository { get => new NotificacionRepository(); }
        public IEspecialidadTipRepository especialidadTipRepository { get => new EspecialidadTipRepository(); }
        public IMensajePersonaRepository mensajePersonaRepository { get => new MensajePersonaRepository(); }
        public IMensajePersonaDetalleRepository mensajePersonaDetalleRepository { get => new MensajePersonaDetalleRepository(); }
        public IMaestroRepository maestroRepository { get => new MaestroRepository(); }
        public IMedicoHorarioRepository medicoHorarioRepository { get => new MedicoHorarioRepository(); }
        public IRecetaRepository recetaRepository { get => new RecetaRepository(); }
        public IMedicoEspecialidadRepository medicoEspecialidadRepository { get => new MedicoEspecialidadRepository(); }
        public IMedicoEstudioRepository medicoEstudioRepository { get => new MedicoEstudioRepository(); }
        public IMedicoExperienciaRepository medicoExperienciaRepository { get => new MedicoExperienciaRepository(); }
        public IConsultaExamenRepository consultaExamenRepository { get => new ConsultaExamenRepository(); }
        public IHistoriaClinicaRepository historiaClinicaRepository { get => new HistoriaClinicaRepository(); }
        public IHistoriaClinicaDetalleRepository historiaClinicaDetalleRepository { get => new HistoriaClinicaDetalleRepository(); }
        public IArchivoRepository archivoRepository { get => new ArchivoRepository(); }

    }
}

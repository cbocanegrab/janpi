﻿using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Infrastructure.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace JAMPI.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IUsuarioRepository, UsuarioRepository>();
            services.AddTransient<IPersonaRepository, PersonaRepository>();
            services.AddTransient<IMedicoRepository, MedicoRepository>();
            services.AddTransient<IEspecialidadMedicaRepository, EspecialidadMedicaRepository>();
            services.AddTransient<IConsultaRepository, ConsultaRepository>();
            services.AddTransient<IEmpresaRepository, EmpresaRepository>();
            services.AddTransient<IEmpresaEmpleadoRepository, EmpresaEmpleadoRepository>();
            services.AddTransient<IParametroRepository, ParametroRepository>();
            services.AddTransient<IPersonaMedidaRepository, PersonaMedidaRepository>();
            services.AddTransient<IPersonaConfigMedidaRepository, PersonaConfigMedidaRepository>();
            services.AddTransient<INotificacionRepository, NotificacionRepository>();
            services.AddTransient<IEspecialidadTipRepository, EspecialidadTipRepository>();
            services.AddTransient<IMensajePersonaRepository, MensajePersonaRepository>();
            services.AddTransient<IMensajePersonaDetalleRepository, MensajePersonaDetalleRepository>();
            services.AddTransient<IMedicoHorarioRepository, MedicoHorarioRepository>();
            services.AddTransient<IRecetaRepository, RecetaRepository>();
            services.AddTransient<IHistoriaClinicaRepository, HistoriaClinicaRepository>();
            services.AddTransient<IHistoriaClinicaDetalleRepository, HistoriaClinicaDetalleRepository>();
            services.AddTransient<IArchivoRepository, ArchivoRepository>();
            return services;
        }
    }
}
